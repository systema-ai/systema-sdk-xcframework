# Systema SDK for iOS
This repository contains the `xcframework` artifact of [Systema SDK](https://gitlab.com/systema-labs/systema-sdk) for iOS/OSX.

## Installation
Below are few options to use this in your Podfile:

- Directly from the repository: master or develop branch

  `pod 'SystemaSDK', :branch => 'master', :git => "https://gitlab.com/systema-labs/systema-sdk-xcframework"`

- Directly from the repository: specific tag 

  `pod 'SystemaSDK', :tag=> '0.1.0rc1', :git => "https://gitlab.com/systema-labs/systema-sdk-xcframework"`

- From public Cocoapod spec: 

  `pod 'SystemaSDK', '~> 0.1.0rc1'`
