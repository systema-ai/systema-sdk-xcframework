Pod::Spec.new do |spec|
  spec.name         = "SystemaSDK"
  spec.version      = "0.1.3"
  spec.summary      = "Systema AI Mobile SDK"
  spec.description  = <<-DESC
  Systema AI Mobile SDK lets you easily integrate the Systema AI REST APIs with your mobile apps.
  Developer documentation = https://systema-labs.gitlab.io/systema-sdk/
    DESC
  spec.homepage     = "https://gitlab.com/systema-labs/systema-sdk-xcframework"
  spec.license      = { :type => "MIT", :file => "LICENSE" }
  spec.author       = { "Systema AI" => "development@systema.ai" }
  spec.ios.deployment_target = "12.0"
  spec.source       = { :git => "https://bitbucket.org/systema-ai/systema-sdk-xcframework.git", :tag => "#{spec.version}" }
  spec.exclude_files = "Classes/Exclude"
  spec.vendored_frameworks      = "SystemaSDK.xcframework"
  spec.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'i386' }
  spec.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'i386' }
end
