#import <Foundation/NSArray.h>
#import <Foundation/NSDictionary.h>
#import <Foundation/NSError.h>
#import <Foundation/NSObject.h>
#import <Foundation/NSSet.h>
#import <Foundation/NSString.h>
#import <Foundation/NSValue.h>

@class SSDKCartRecommendationRequest, SSDKRequestOptions, SSDKRecommendationResponse, SSDKSystemaResult<__covariant T>, SSDKRecommendationRequest, SSDKSmartSearchRequest, SSDKSmartSearchResponse, SSDKSmartSuggestRequest, SSDKSmartSuggestResponse, SSDKKotlinUnit, SSDKEndpointType, SSDKKtor_httpUrl, SSDKKtor_client_coreHttpClient, SSDKSystemaLogLevel, SSDKSystemaAPIKey, SSDKSystemaClientID, SSDKEnvironmentType, SSDKPurchaseOrder, SSDKKtor_client_coreHttpResponse, SSDKItemContainer, SSDKCartItem, SSDKWishlistItem, SSDKSystemaUser, SSDKKotlinEnumCompanion, SSDKKotlinEnum<E>, SSDKCurrency, SSDKKotlinArray<T>, SSDKLanguages, SSDKQueryItemType, SSDKSystemaConstants, SSDKSystemaTags, SSDKCallType, SSDKScoreType, SSDKTrackerEventType, SSDKCartItemAcquiredEvent, SSDKCartItemAcquisitionCompleteEvent, SSDKCartItemRelinquishedEvent, SSDKContainerShownEvent, SSDKItemClickEvent, SSDKPageViewEvent, SSDKWishlistItemAcquiredEvent, SSDKWishlistItemRelinquishedEvent, SSDKSystemaRoutes, SSDKSystemaDevice, SSDKKotlinx_datetimeInstant, SSDKKeyValueCompanion, SSDKKeyValue, SSDKSystemaAPIKeyCompanion, SSDKSystemaDeviceCompanion, SSDKSystemaResultCompanion, SSDKKotlinException, SSDKSystemaUserCompanion, SSDKKotlinThrowable, SSDKSystemaErrorEvent, SSDKSystemaLogEventCompanion, SSDKSystemaLogEvent, SSDKTrackEventDateCompanion, SSDKTrackEventDate, SSDKWishlistItemCompanion, SSDKWishlistItemAcquiredEventCompanion, SSDKWishlistItemRelinquishedEventCompanion, SSDKCartItemCompanion, SSDKCartItemAcquiredEventCompanion, SSDKCartItemAcquisitionCompleteEventCompanion, SSDKCartItemRelinquishedEventCompanion, SSDKOrderItemCompanion, SSDKOrderItem, SSDKShippingAddress, SSDKPurchaseOrderCompanion, SSDKShippingAddressCompanion, SSDKContainerShownEventCompanion, SSDKItemClickEventCompanion, SSDKItemContainerCompanion, SSDKPageViewEventCompanion, SSDKCategoryCompanion, SSDKCategory, SSDKKotlinx_serialization_jsonJsonElement, SSDKProductCompanion, SSDKProduct, SSDKRequestUser, SSDKFilter, SSDKCartRecommendationRequestCompanion, SSDKFilterCompanion, SSDKQueryItemCompanion, SSDKQueryItem, SSDKRecommendationRequestCompanion, SSDKRequestUserCompanion, SSDKSmartSearchRequestCompanion, SSDKSmartSuggestRequestCompanion, SSDKRecommendationResponseCompanion, SSDKResponseDefaultCompanion, SSDKResponseDefault<T>, SSDKSmartSearchResponseCompanion, SSDKSmartSuggestResult, SSDKSmartSuggestResponseCompanion, SSDKSmartSuggestResultCompanion, SSDKApiMaintenanceCompanion, SSDKApiMaintenance, SSDKDynamicSettings, SSDKDynamicConfigCompanion, SSDKDynamicConfig, SSDKPayloadTemplate, SSDKDynamicSettingsCompanion, SSDKPayloadTemplateCompanion, SSDKSystemaCacheCompanion, SSDKSystemaCache, SSDKSystemaIosStorageCompanion, SSDKKotlinRuntimeException, SSDKSystemaRuntimeException, SSDKKotlinIllegalStateException, SSDKKtor_client_coreHttpRequestData, SSDKKtor_client_coreHttpResponseData, SSDKKtor_client_coreHttpClientEngineConfig, SSDKKotlinx_coroutines_coreCoroutineDispatcher, SSDKKtor_httpUrlCompanion, SSDKKtor_httpURLProtocol, SSDKKtor_client_coreHttpClientConfig<T>, SSDKKtor_eventsEvents, SSDKKtor_client_coreHttpReceivePipeline, SSDKKtor_client_coreHttpRequestPipeline, SSDKKtor_client_coreHttpResponsePipeline, SSDKKtor_client_coreHttpSendPipeline, SSDKKtor_client_coreHttpClientCall, SSDKKtor_utilsGMTDate, SSDKKtor_httpHttpStatusCode, SSDKKtor_httpHttpProtocolVersion, SSDKKotlinx_datetimeInstantCompanion, SSDKKotlinx_serialization_coreSerializersModule, SSDKKotlinx_serialization_coreSerialKind, SSDKKotlinNothing, SSDKKotlinx_serialization_jsonJsonElementCompanion, SSDKKtor_httpHttpMethod, SSDKKtor_httpOutgoingContent, SSDKKtor_client_coreProxyConfig, SSDKKotlinAbstractCoroutineContextElement, SSDKKotlinx_coroutines_coreCoroutineDispatcherKey, SSDKKtor_httpURLProtocolCompanion, SSDKKtor_utilsAttributeKey<T>, SSDKKtor_eventsEventDefinition<T>, SSDKKtor_utilsPipelinePhase, SSDKKtor_utilsPipeline<TSubject, TContext>, SSDKKtor_client_coreHttpReceivePipelinePhases, SSDKKtor_client_coreHttpRequestPipelinePhases, SSDKKtor_client_coreHttpRequestBuilder, SSDKKtor_client_coreHttpResponsePipelinePhases, SSDKKtor_client_coreHttpResponseContainer, SSDKKtor_client_coreHttpSendPipelinePhases, SSDKKtor_client_coreHttpClientCallCompanion, SSDKKtor_utilsTypeInfo, SSDKKtor_ioMemory, SSDKKtor_ioChunkBuffer, SSDKKotlinByteArray, SSDKKtor_ioBuffer, SSDKKtor_ioByteReadPacket, SSDKKtor_utilsGMTDateCompanion, SSDKKtor_utilsWeekDay, SSDKKtor_utilsMonth, SSDKKtor_httpHttpStatusCodeCompanion, SSDKKtor_httpHttpProtocolVersionCompanion, SSDKKtor_httpHttpMethodCompanion, SSDKKtor_httpContentType, SSDKKotlinCancellationException, SSDKKotlinAbstractCoroutineContextKey<B, E>, SSDKKtor_httpHeadersBuilder, SSDKKtor_client_coreHttpRequestBuilderCompanion, SSDKKtor_httpURLBuilder, SSDKKtor_ioMemoryCompanion, SSDKKtor_ioBufferCompanion, SSDKKtor_ioChunkBufferCompanion, SSDKKotlinByteIterator, SSDKKtor_ioInputCompanion, SSDKKtor_ioInput, SSDKKtor_ioByteReadPacketCompanion, SSDKKtor_utilsWeekDayCompanion, SSDKKtor_utilsMonthCompanion, SSDKKtor_httpHeaderValueParam, SSDKKtor_httpHeaderValueWithParametersCompanion, SSDKKtor_httpHeaderValueWithParameters, SSDKKtor_httpContentTypeCompanion, SSDKKtor_utilsStringValuesBuilderImpl, SSDKKtor_httpURLBuilderCompanion, SSDKKotlinKTypeProjection, SSDKKotlinx_coroutines_coreAtomicDesc, SSDKKotlinx_coroutines_coreLockFreeLinkedListNodePrepareOp, SSDKKotlinKVariance, SSDKKotlinKTypeProjectionCompanion, SSDKKotlinx_coroutines_coreAtomicOp<__contravariant T>, SSDKKotlinx_coroutines_coreOpDescriptor, SSDKKotlinx_coroutines_coreLockFreeLinkedListNode, SSDKKotlinx_coroutines_coreLockFreeLinkedListNodeAbstractAtomicDesc, SSDKKotlinx_coroutines_coreLockFreeLinkedListNodeAddLastDesc<T>, SSDKKotlinx_coroutines_coreLockFreeLinkedListNodeRemoveFirstDesc<T>;

@protocol SSDKCredentials, SSDKSystemaDeviceManager, SSDKKtor_client_coreHttpClientEngine, SSDKSystemaKVStore, SSDKConfiguration, SSDKTracker, SSDKRecommender, SSDKSmartSearch, SSDKSmartSuggest, SSDKSystemaUserManager, SSDKKotlinComparable, SSDKKotlinx_serialization_coreKSerializer, SSDKKotlinx_serialization_coreEncoder, SSDKKotlinx_serialization_coreSerialDescriptor, SSDKKotlinx_serialization_coreSerializationStrategy, SSDKKotlinx_serialization_coreDecoder, SSDKKotlinx_serialization_coreDeserializationStrategy, SSDKSystemaAI, SSDKKtor_client_coreHttpClientEngineCapability, SSDKKotlinCoroutineContext, SSDKKotlinx_coroutines_coreCoroutineScope, SSDKKtor_ioCloseable, SSDKKtor_httpParameters, SSDKKtor_utilsAttributes, SSDKKtor_httpHeaders, SSDKKtor_httpHttpMessage, SSDKKtor_ioByteReadChannel, SSDKKotlinIterator, SSDKKotlinx_serialization_coreCompositeEncoder, SSDKKotlinAnnotation, SSDKKotlinx_serialization_coreCompositeDecoder, SSDKKotlinx_coroutines_coreJob, SSDKKotlinCoroutineContextKey, SSDKKotlinCoroutineContextElement, SSDKKotlinContinuation, SSDKKotlinContinuationInterceptor, SSDKKotlinx_coroutines_coreRunnable, SSDKKotlinMapEntry, SSDKKtor_utilsStringValues, SSDKKtor_client_coreHttpClientPlugin, SSDKKotlinx_coroutines_coreDisposableHandle, SSDKKotlinSuspendFunction2, SSDKKtor_client_coreHttpRequest, SSDKKtor_ioReadSession, SSDKKotlinSuspendFunction1, SSDKKotlinAppendable, SSDKKotlinx_serialization_coreSerializersModuleCollector, SSDKKotlinKClass, SSDKKotlinx_coroutines_coreChildHandle, SSDKKotlinx_coroutines_coreChildJob, SSDKKotlinSequence, SSDKKotlinx_coroutines_coreSelectClause0, SSDKKotlinFunction, SSDKKtor_httpHttpMessageBuilder, SSDKKotlinKType, SSDKKtor_ioObjectPool, SSDKKotlinKDeclarationContainer, SSDKKotlinKAnnotatedElement, SSDKKotlinKClassifier, SSDKKotlinx_coroutines_coreParentJob, SSDKKotlinx_coroutines_coreSelectInstance, SSDKKotlinSuspendFunction0, SSDKKtor_utilsStringValuesBuilder, SSDKKtor_httpParametersBuilder;

NS_ASSUME_NONNULL_BEGIN
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunknown-warning-option"
#pragma clang diagnostic ignored "-Wincompatible-property-type"
#pragma clang diagnostic ignored "-Wnullability"

#pragma push_macro("_Nullable_result")
#if !__has_feature(nullability_nullable_result)
#undef _Nullable_result
#define _Nullable_result _Nullable
#endif

__attribute__((swift_name("KotlinBase")))
@interface SSDKBase : NSObject
- (instancetype)init __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
+ (void)initialize __attribute__((objc_requires_super));
@end;

@interface SSDKBase (SSDKBaseCopying) <NSCopying>
@end;

__attribute__((swift_name("KotlinMutableSet")))
@interface SSDKMutableSet<ObjectType> : NSMutableSet<ObjectType>
@end;

__attribute__((swift_name("KotlinMutableDictionary")))
@interface SSDKMutableDictionary<KeyType, ObjectType> : NSMutableDictionary<KeyType, ObjectType>
@end;

@interface NSError (NSErrorSSDKKotlinException)
@property (readonly) id _Nullable kotlinException;
@end;

__attribute__((swift_name("KotlinNumber")))
@interface SSDKNumber : NSNumber
- (instancetype)initWithChar:(char)value __attribute__((unavailable));
- (instancetype)initWithUnsignedChar:(unsigned char)value __attribute__((unavailable));
- (instancetype)initWithShort:(short)value __attribute__((unavailable));
- (instancetype)initWithUnsignedShort:(unsigned short)value __attribute__((unavailable));
- (instancetype)initWithInt:(int)value __attribute__((unavailable));
- (instancetype)initWithUnsignedInt:(unsigned int)value __attribute__((unavailable));
- (instancetype)initWithLong:(long)value __attribute__((unavailable));
- (instancetype)initWithUnsignedLong:(unsigned long)value __attribute__((unavailable));
- (instancetype)initWithLongLong:(long long)value __attribute__((unavailable));
- (instancetype)initWithUnsignedLongLong:(unsigned long long)value __attribute__((unavailable));
- (instancetype)initWithFloat:(float)value __attribute__((unavailable));
- (instancetype)initWithDouble:(double)value __attribute__((unavailable));
- (instancetype)initWithBool:(BOOL)value __attribute__((unavailable));
- (instancetype)initWithInteger:(NSInteger)value __attribute__((unavailable));
- (instancetype)initWithUnsignedInteger:(NSUInteger)value __attribute__((unavailable));
+ (instancetype)numberWithChar:(char)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedChar:(unsigned char)value __attribute__((unavailable));
+ (instancetype)numberWithShort:(short)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedShort:(unsigned short)value __attribute__((unavailable));
+ (instancetype)numberWithInt:(int)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedInt:(unsigned int)value __attribute__((unavailable));
+ (instancetype)numberWithLong:(long)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedLong:(unsigned long)value __attribute__((unavailable));
+ (instancetype)numberWithLongLong:(long long)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedLongLong:(unsigned long long)value __attribute__((unavailable));
+ (instancetype)numberWithFloat:(float)value __attribute__((unavailable));
+ (instancetype)numberWithDouble:(double)value __attribute__((unavailable));
+ (instancetype)numberWithBool:(BOOL)value __attribute__((unavailable));
+ (instancetype)numberWithInteger:(NSInteger)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedInteger:(NSUInteger)value __attribute__((unavailable));
@end;

__attribute__((swift_name("KotlinByte")))
@interface SSDKByte : SSDKNumber
- (instancetype)initWithChar:(char)value;
+ (instancetype)numberWithChar:(char)value;
@end;

__attribute__((swift_name("KotlinUByte")))
@interface SSDKUByte : SSDKNumber
- (instancetype)initWithUnsignedChar:(unsigned char)value;
+ (instancetype)numberWithUnsignedChar:(unsigned char)value;
@end;

__attribute__((swift_name("KotlinShort")))
@interface SSDKShort : SSDKNumber
- (instancetype)initWithShort:(short)value;
+ (instancetype)numberWithShort:(short)value;
@end;

__attribute__((swift_name("KotlinUShort")))
@interface SSDKUShort : SSDKNumber
- (instancetype)initWithUnsignedShort:(unsigned short)value;
+ (instancetype)numberWithUnsignedShort:(unsigned short)value;
@end;

__attribute__((swift_name("KotlinInt")))
@interface SSDKInt : SSDKNumber
- (instancetype)initWithInt:(int)value;
+ (instancetype)numberWithInt:(int)value;
@end;

__attribute__((swift_name("KotlinUInt")))
@interface SSDKUInt : SSDKNumber
- (instancetype)initWithUnsignedInt:(unsigned int)value;
+ (instancetype)numberWithUnsignedInt:(unsigned int)value;
@end;

__attribute__((swift_name("KotlinLong")))
@interface SSDKLong : SSDKNumber
- (instancetype)initWithLongLong:(long long)value;
+ (instancetype)numberWithLongLong:(long long)value;
@end;

__attribute__((swift_name("KotlinULong")))
@interface SSDKULong : SSDKNumber
- (instancetype)initWithUnsignedLongLong:(unsigned long long)value;
+ (instancetype)numberWithUnsignedLongLong:(unsigned long long)value;
@end;

__attribute__((swift_name("KotlinFloat")))
@interface SSDKFloat : SSDKNumber
- (instancetype)initWithFloat:(float)value;
+ (instancetype)numberWithFloat:(float)value;
@end;

__attribute__((swift_name("KotlinDouble")))
@interface SSDKDouble : SSDKNumber
- (instancetype)initWithDouble:(double)value;
+ (instancetype)numberWithDouble:(double)value;
@end;

__attribute__((swift_name("KotlinBoolean")))
@interface SSDKBoolean : SSDKNumber
- (instancetype)initWithBool:(BOOL)value;
+ (instancetype)numberWithBool:(BOOL)value;
@end;

__attribute__((swift_name("Recommender")))
@protocol SSDKRecommender
@required

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)getCartComplementaryPayload:(SSDKCartRecommendationRequest *)payload requestOptions:(SSDKRequestOptions * _Nullable)requestOptions completionHandler:(void (^)(SSDKSystemaResult<SSDKRecommendationResponse *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("getCartComplementary(payload:requestOptions:completionHandler:)")));
- (void)getCartComplementaryPayload:(SSDKCartRecommendationRequest *)payload requestOptions:(SSDKRequestOptions * _Nullable)requestOptions result:(void (^)(SSDKSystemaResult<SSDKRecommendationResponse *> *))result __attribute__((swift_name("getCartComplementary(payload:requestOptions:result:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)getCartRelatedPayload:(SSDKCartRecommendationRequest *)payload requestOptions:(SSDKRequestOptions * _Nullable)requestOptions completionHandler:(void (^)(SSDKSystemaResult<SSDKRecommendationResponse *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("getCartRelated(payload:requestOptions:completionHandler:)")));
- (void)getCartRelatedPayload:(SSDKCartRecommendationRequest *)payload requestOptions:(SSDKRequestOptions * _Nullable)requestOptions result:(void (^)(SSDKSystemaResult<SSDKRecommendationResponse *> *))result __attribute__((swift_name("getCartRelated(payload:requestOptions:result:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)getCategoryPopularPayload:(SSDKRecommendationRequest *)payload requestOptions:(SSDKRequestOptions * _Nullable)requestOptions completionHandler:(void (^)(SSDKSystemaResult<SSDKRecommendationResponse *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("getCategoryPopular(payload:requestOptions:completionHandler:)")));
- (void)getCategoryPopularPayload:(SSDKRecommendationRequest *)payload requestOptions:(SSDKRequestOptions * _Nullable)requestOptions result:(void (^)(SSDKSystemaResult<SSDKRecommendationResponse *> *))result __attribute__((swift_name("getCategoryPopular(payload:requestOptions:result:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)getCategoryTrendingPayload:(SSDKRecommendationRequest *)payload requestOptions:(SSDKRequestOptions * _Nullable)requestOptions completionHandler:(void (^)(SSDKSystemaResult<SSDKRecommendationResponse *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("getCategoryTrending(payload:requestOptions:completionHandler:)")));
- (void)getCategoryTrendingPayload:(SSDKRecommendationRequest *)payload requestOptions:(SSDKRequestOptions * _Nullable)requestOptions result:(void (^)(SSDKSystemaResult<SSDKRecommendationResponse *> *))result __attribute__((swift_name("getCategoryTrending(payload:requestOptions:result:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)getComplementaryPayload:(SSDKRecommendationRequest *)payload requestOptions:(SSDKRequestOptions * _Nullable)requestOptions completionHandler:(void (^)(SSDKSystemaResult<SSDKRecommendationResponse *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("getComplementary(payload:requestOptions:completionHandler:)")));
- (void)getComplementaryPayload:(SSDKRecommendationRequest *)payload requestOptions:(SSDKRequestOptions * _Nullable)requestOptions result:(void (^)(SSDKSystemaResult<SSDKRecommendationResponse *> *))result __attribute__((swift_name("getComplementary(payload:requestOptions:result:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)getPopularPayload:(SSDKRecommendationRequest *)payload requestOptions:(SSDKRequestOptions * _Nullable)requestOptions completionHandler:(void (^)(SSDKSystemaResult<SSDKRecommendationResponse *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("getPopular(payload:requestOptions:completionHandler:)")));
- (void)getPopularPayload:(SSDKRecommendationRequest *)payload requestOptions:(SSDKRequestOptions * _Nullable)requestOptions result:(void (^)(SSDKSystemaResult<SSDKRecommendationResponse *> *))result __attribute__((swift_name("getPopular(payload:requestOptions:result:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)getRelatedPayload:(SSDKRecommendationRequest *)payload requestOptions:(SSDKRequestOptions * _Nullable)requestOptions completionHandler:(void (^)(SSDKSystemaResult<SSDKRecommendationResponse *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("getRelated(payload:requestOptions:completionHandler:)")));
- (void)getRelatedPayload:(SSDKRecommendationRequest *)payload requestOptions:(SSDKRequestOptions * _Nullable)requestOptions result:(void (^)(SSDKSystemaResult<SSDKRecommendationResponse *> *))result __attribute__((swift_name("getRelated(payload:requestOptions:result:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)getSimilarPayload:(SSDKRecommendationRequest *)payload requestOptions:(SSDKRequestOptions * _Nullable)requestOptions completionHandler:(void (^)(SSDKSystemaResult<SSDKRecommendationResponse *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("getSimilar(payload:requestOptions:completionHandler:)")));
- (void)getSimilarPayload:(SSDKRecommendationRequest *)payload requestOptions:(SSDKRequestOptions * _Nullable)requestOptions result:(void (^)(SSDKSystemaResult<SSDKRecommendationResponse *> *))result __attribute__((swift_name("getSimilar(payload:requestOptions:result:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)getTrendingPayload:(SSDKRecommendationRequest *)payload requestOptions:(SSDKRequestOptions * _Nullable)requestOptions completionHandler:(void (^)(SSDKSystemaResult<SSDKRecommendationResponse *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("getTrending(payload:requestOptions:completionHandler:)")));
- (void)getTrendingPayload:(SSDKRecommendationRequest *)payload requestOptions:(SSDKRequestOptions * _Nullable)requestOptions result:(void (^)(SSDKSystemaResult<SSDKRecommendationResponse *> *))result __attribute__((swift_name("getTrending(payload:requestOptions:result:)")));
@end;

__attribute__((swift_name("SmartSearch")))
@protocol SSDKSmartSearch
@required

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)smartSearchPayload:(SSDKSmartSearchRequest *)payload requestOptions:(SSDKRequestOptions * _Nullable)requestOptions completionHandler:(void (^)(SSDKSystemaResult<SSDKSmartSearchResponse *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("smartSearch(payload:requestOptions:completionHandler:)")));
- (void)smartSearchPayload:(SSDKSmartSearchRequest *)payload requestOptions:(SSDKRequestOptions * _Nullable)requestOptions result:(void (^)(SSDKSystemaResult<SSDKSmartSearchResponse *> *))result __attribute__((swift_name("smartSearch(payload:requestOptions:result:)")));
@end;

__attribute__((swift_name("SmartSuggest")))
@protocol SSDKSmartSuggest
@required

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)smartSuggestPayload:(SSDKSmartSuggestRequest *)payload requestOptions:(SSDKRequestOptions * _Nullable)requestOptions completionHandler:(void (^)(SSDKSystemaResult<SSDKSmartSuggestResponse *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("smartSuggest(payload:requestOptions:completionHandler:)")));
- (void)smartSuggestPayload:(SSDKSmartSuggestRequest *)payload requestOptions:(SSDKRequestOptions * _Nullable)requestOptions result:(void (^)(SSDKSystemaResult<SSDKSmartSuggestResponse *> *))result __attribute__((swift_name("smartSuggest(payload:requestOptions:result:)")));
@end;

__attribute__((swift_name("Configuration")))
@protocol SSDKConfiguration
@required
@property (readonly) id<SSDKCredentials> credentials __attribute__((swift_name("credentials")));
@property (readonly) id<SSDKSystemaDeviceManager> deviceManager __attribute__((swift_name("deviceManager")));
@property (readonly) id<SSDKKtor_client_coreHttpClientEngine> _Nullable engine __attribute__((swift_name("engine")));
@property (readonly) NSDictionary<SSDKEndpointType *, SSDKKtor_httpUrl *> *hosts __attribute__((swift_name("hosts")));
@property (readonly) SSDKKtor_client_coreHttpClient *httpClient __attribute__((swift_name("httpClient")));
@property (readonly) id<SSDKSystemaKVStore> kvStore __attribute__((swift_name("kvStore")));
@property (readonly) SSDKSystemaLogLevel *logLevel __attribute__((swift_name("logLevel")));
@end;

__attribute__((swift_name("Credentials")))
@protocol SSDKCredentials
@required
@property (readonly) SSDKSystemaAPIKey *apiKey __attribute__((swift_name("apiKey")));
@property (readonly) SSDKSystemaClientID *clientID __attribute__((swift_name("clientID")));
@property (readonly) SSDKEnvironmentType *environment __attribute__((swift_name("environment")));
@property (readonly) NSDictionary<SSDKEndpointType *, SSDKKtor_httpUrl *> *proxyUrls __attribute__((swift_name("proxyUrls")));
@end;

__attribute__((swift_name("Tracker")))
@protocol SSDKTracker
@required

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)trackAcquisitionCompleteOrder:(SSDKPurchaseOrder *)order url:(NSString *)url referrer:(NSString * _Nullable)referrer completionHandler:(void (^)(SSDKSystemaResult<SSDKKtor_client_coreHttpResponse *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("trackAcquisitionComplete(order:url:referrer:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)trackAcquisitionCompleteOrder:(SSDKPurchaseOrder *)order url:(NSString *)url referrer:(NSString * _Nullable)referrer result:(void (^)(SSDKSystemaResult<SSDKKtor_client_coreHttpResponse *> *))result completionHandler:(void (^)(SSDKKotlinUnit * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("trackAcquisitionComplete(order:url:referrer:result:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)trackContainerShownProductId:(NSString * _Nullable)productId containers:(NSArray<SSDKItemContainer *> *)containers url:(NSString * _Nullable)url referrer:(NSString * _Nullable)referrer completionHandler:(void (^)(SSDKSystemaResult<SSDKKtor_client_coreHttpResponse *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("trackContainerShown(productId:containers:url:referrer:completionHandler:)")));
- (void)trackContainerShownProductId:(NSString * _Nullable)productId containers:(NSArray<SSDKItemContainer *> *)containers url:(NSString * _Nullable)url referrer:(NSString * _Nullable)referrer result:(void (^)(SSDKSystemaResult<SSDKKtor_client_coreHttpResponse *> *))result __attribute__((swift_name("trackContainerShown(productId:containers:url:referrer:result:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)trackItemAcquiredProductId:(NSString *)productId items:(NSArray<SSDKCartItem *> *)items url:(NSString *)url referrer:(NSString * _Nullable)referrer completionHandler:(void (^)(SSDKSystemaResult<SSDKKtor_client_coreHttpResponse *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("trackItemAcquired(productId:items:url:referrer:completionHandler:)")));
- (void)trackItemAcquiredProductId:(NSString *)productId items:(NSArray<SSDKCartItem *> *)items url:(NSString *)url referrer:(NSString * _Nullable)referrer result:(void (^)(SSDKSystemaResult<SSDKKtor_client_coreHttpResponse *> *))result __attribute__((swift_name("trackItemAcquired(productId:items:url:referrer:result:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)trackItemClickedProductId:(NSString *)productId url:(NSString *)url recId:(NSString *)recId referrer:(NSString *)referrer completionHandler:(void (^)(SSDKSystemaResult<SSDKKtor_client_coreHttpResponse *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("trackItemClicked(productId:url:recId:referrer:completionHandler:)")));
- (void)trackItemClickedProductId:(NSString *)productId url:(NSString *)url recId:(NSString *)recId referrer:(NSString *)referrer result:(void (^)(SSDKSystemaResult<SSDKKtor_client_coreHttpResponse *> *))result __attribute__((swift_name("trackItemClicked(productId:url:recId:referrer:result:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)trackItemRelinquishedProductId:(NSString *)productId item:(SSDKCartItem *)item url:(NSString *)url referrer:(NSString * _Nullable)referrer completionHandler:(void (^)(SSDKSystemaResult<SSDKKtor_client_coreHttpResponse *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("trackItemRelinquished(productId:item:url:referrer:completionHandler:)")));
- (void)trackItemRelinquishedProductId:(NSString *)productId item:(SSDKCartItem *)item url:(NSString *)url referrer:(NSString * _Nullable)referrer result:(void (^)(SSDKSystemaResult<SSDKKtor_client_coreHttpResponse *> *))result __attribute__((swift_name("trackItemRelinquished(productId:item:url:referrer:result:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)trackPageViewedUrl:(NSString *)url recId:(NSString *)recId referrer:(NSString *)referrer completionHandler:(void (^)(SSDKSystemaResult<SSDKKtor_client_coreHttpResponse *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("trackPageViewed(url:recId:referrer:completionHandler:)")));
- (void)trackPageViewedUrl:(NSString *)url recId:(NSString *)recId referrer:(NSString *)referrer result:(void (^)(SSDKSystemaResult<SSDKKtor_client_coreHttpResponse *> *))result __attribute__((swift_name("trackPageViewed(url:recId:referrer:result:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)trackPageViewedProductId:(NSString *)productId url:(NSString *)url recId:(NSString *)recId referrer:(NSString *)referrer completionHandler:(void (^)(SSDKSystemaResult<SSDKKtor_client_coreHttpResponse *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("trackPageViewed(productId:url:recId:referrer:completionHandler:)")));
- (void)trackPageViewedProductId:(NSString *)productId url:(NSString *)url recId:(NSString *)recId referrer:(NSString *)referrer result:(void (^)(SSDKSystemaResult<SSDKKtor_client_coreHttpResponse *> *))result __attribute__((swift_name("trackPageViewed(productId:url:recId:referrer:result:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)trackWishlistAcquiredProductId:(NSString *)productId items:(NSArray<SSDKWishlistItem *> *)items url:(NSString *)url referrer:(NSString * _Nullable)referrer completionHandler:(void (^)(SSDKSystemaResult<SSDKKtor_client_coreHttpResponse *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("trackWishlistAcquired(productId:items:url:referrer:completionHandler:)")));
- (void)trackWishlistAcquiredProductId:(NSString *)productId items:(NSArray<SSDKWishlistItem *> *)items url:(NSString *)url referrer:(NSString * _Nullable)referrer result:(void (^)(SSDKSystemaResult<SSDKKtor_client_coreHttpResponse *> *))result __attribute__((swift_name("trackWishlistAcquired(productId:items:url:referrer:result:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)trackWishlistRelinquishedProductId:(NSString *)productId item:(SSDKWishlistItem *)item url:(NSString *)url referrer:(NSString * _Nullable)referrer completionHandler:(void (^)(SSDKSystemaResult<SSDKKtor_client_coreHttpResponse *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("trackWishlistRelinquished(productId:item:url:referrer:completionHandler:)")));
- (void)trackWishlistRelinquishedProductId:(NSString *)productId item:(SSDKWishlistItem *)item url:(NSString *)url referrer:(NSString * _Nullable)referrer result:(void (^)(SSDKSystemaResult<SSDKKtor_client_coreHttpResponse *> *))result __attribute__((swift_name("trackWishlistRelinquished(productId:item:url:referrer:result:)")));
@end;

__attribute__((swift_name("SystemaUserManager")))
@protocol SSDKSystemaUserManager
@required

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)clearUserIdHashWithCompletionHandler:(void (^)(SSDKKotlinUnit * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("clearUserIdHash(completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)getUserSnapshotWithCompletionHandler:(void (^)(SSDKSystemaUser * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("getUserSnapshot(completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)setUserIdHashUid:(NSString *)uid hash:(BOOL)hash completionHandler:(void (^)(SSDKKotlinUnit * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("setUserIdHash(uid:hash:completionHandler:)")));
@end;

__attribute__((swift_name("SystemaAI")))
@protocol SSDKSystemaAI <SSDKConfiguration, SSDKCredentials, SSDKTracker, SSDKRecommender, SSDKSmartSearch, SSDKSmartSuggest, SSDKSystemaUserManager>
@required

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)initializeWithCompletionHandler:(void (^)(SSDKKotlinUnit * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("initialize(completionHandler:)")));
@property (readonly) NSDictionary<NSString *, id> *meta __attribute__((swift_name("meta")));
@end;

__attribute__((swift_name("KotlinComparable")))
@protocol SSDKKotlinComparable
@required
- (int32_t)compareToOther:(id _Nullable)other __attribute__((swift_name("compareTo(other:)")));
@end;

__attribute__((swift_name("KotlinEnum")))
@interface SSDKKotlinEnum<E> : SSDKBase <SSDKKotlinComparable>
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) SSDKKotlinEnumCompanion *companion __attribute__((swift_name("companion")));
- (int32_t)compareToOther:(E)other __attribute__((swift_name("compareTo(other:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@property (readonly) int32_t ordinal __attribute__((swift_name("ordinal")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Currency")))
@interface SSDKCurrency : SSDKKotlinEnum<SSDKCurrency *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) SSDKCurrency *usd __attribute__((swift_name("usd")));
@property (class, readonly) SSDKCurrency *aud __attribute__((swift_name("aud")));
@property (class, readonly) SSDKCurrency *nzd __attribute__((swift_name("nzd")));
@property (class, readonly) SSDKCurrency *sgd __attribute__((swift_name("sgd")));
@property (class, readonly) SSDKCurrency *hkd __attribute__((swift_name("hkd")));
+ (SSDKKotlinArray<SSDKCurrency *> *)values __attribute__((swift_name("values()")));
@property (readonly) NSString *value __attribute__((swift_name("value")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Languages")))
@interface SSDKLanguages : SSDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)languages __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SSDKLanguages *shared __attribute__((swift_name("shared")));
@property (readonly) NSString *English __attribute__((swift_name("English")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("QueryItemType")))
@interface SSDKQueryItemType : SSDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)queryItemType __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SSDKQueryItemType *shared __attribute__((swift_name("shared")));
@property (readonly) NSString *Image __attribute__((swift_name("Image")));
@property (readonly) NSString *Product __attribute__((swift_name("Product")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SystemaConstants")))
@interface SSDKSystemaConstants : SSDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)systemaConstants __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SSDKSystemaConstants *shared __attribute__((swift_name("shared")));
@property (readonly) int64_t MaxInMemoryCacheDuration __attribute__((swift_name("MaxInMemoryCacheDuration")));
@property (readonly) int32_t MaxKeyLen __attribute__((swift_name("MaxKeyLen")));
@property (readonly) int64_t MaxSessionIdleDuration __attribute__((swift_name("MaxSessionIdleDuration")));
@property (readonly) NSString *SystemaCacheFileName __attribute__((swift_name("SystemaCacheFileName")));
@property (readonly) NSString *SystemaTagMapping __attribute__((swift_name("SystemaTagMapping")));
@property (readonly) NSString *Version __attribute__((swift_name("Version")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SystemaTags")))
@interface SSDKSystemaTags : SSDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)systemaTags __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SSDKSystemaTags *shared __attribute__((swift_name("shared")));
@property (readonly) NSString *ContainerUrl __attribute__((swift_name("ContainerUrl")));
@property (readonly) NSString *MonitoringEnabled __attribute__((swift_name("MonitoringEnabled")));
@property (readonly) NSString *Observed __attribute__((swift_name("Observed")));
@property (readonly) NSString *PageUrl __attribute__((swift_name("PageUrl")));
@property (readonly) NSString *ProductCurrency __attribute__((swift_name("ProductCurrency")));
@property (readonly) NSString *ProductId __attribute__((swift_name("ProductId")));
@property (readonly) NSString *ProductPrice __attribute__((swift_name("ProductPrice")));
@property (readonly) NSString *RecId __attribute__((swift_name("RecId")));
@property (readonly) NSString *ReferrerUrl __attribute__((swift_name("ReferrerUrl")));
@property (readonly) NSString *ResultId __attribute__((swift_name("ResultId")));
@property (readonly) NSString *Visible __attribute__((swift_name("Visible")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CallType")))
@interface SSDKCallType : SSDKKotlinEnum<SSDKCallType *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) SSDKCallType *read __attribute__((swift_name("read")));
@property (class, readonly) SSDKCallType *write __attribute__((swift_name("write")));
+ (SSDKKotlinArray<SSDKCallType *> *)values __attribute__((swift_name("values()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EndpointType")))
@interface SSDKEndpointType : SSDKKotlinEnum<SSDKEndpointType *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) SSDKEndpointType *recommend __attribute__((swift_name("recommend")));
@property (class, readonly) SSDKEndpointType *search __attribute__((swift_name("search")));
@property (class, readonly) SSDKEndpointType *suggest __attribute__((swift_name("suggest")));
@property (class, readonly) SSDKEndpointType *tracker __attribute__((swift_name("tracker")));
@property (class, readonly) SSDKEndpointType *dynamicconfig __attribute__((swift_name("dynamicconfig")));
+ (SSDKKotlinArray<SSDKEndpointType *> *)values __attribute__((swift_name("values()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EnvironmentType")))
@interface SSDKEnvironmentType : SSDKKotlinEnum<SSDKEnvironmentType *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) SSDKEnvironmentType *prod __attribute__((swift_name("prod")));
@property (class, readonly) SSDKEnvironmentType *test __attribute__((swift_name("test")));
@property (class, readonly) SSDKEnvironmentType *dev __attribute__((swift_name("dev")));
+ (SSDKKotlinArray<SSDKEnvironmentType *> *)values __attribute__((swift_name("values()")));
@property (readonly) NSString *value __attribute__((swift_name("value")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ScoreType")))
@interface SSDKScoreType : SSDKKotlinEnum<SSDKScoreType *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) SSDKScoreType *relevance __attribute__((swift_name("relevance")));
@property (class, readonly) SSDKScoreType *user __attribute__((swift_name("user")));
+ (SSDKKotlinArray<SSDKScoreType *> *)values __attribute__((swift_name("values()")));
@property (readonly) NSString *value __attribute__((swift_name("value")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TrackerEventType")))
@interface SSDKTrackerEventType : SSDKKotlinEnum<SSDKTrackerEventType *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) SSDKTrackerEventType *pageview __attribute__((swift_name("pageview")));
@property (class, readonly) SSDKTrackerEventType *itemclicked __attribute__((swift_name("itemclicked")));
@property (class, readonly) SSDKTrackerEventType *containershown __attribute__((swift_name("containershown")));
@property (class, readonly) SSDKTrackerEventType *addtocart __attribute__((swift_name("addtocart")));
@property (class, readonly) SSDKTrackerEventType *removefromcart __attribute__((swift_name("removefromcart")));
@property (class, readonly) SSDKTrackerEventType *purchase __attribute__((swift_name("purchase")));
@property (class, readonly) SSDKTrackerEventType *addtowishlist __attribute__((swift_name("addtowishlist")));
@property (class, readonly) SSDKTrackerEventType *removefromwishlist __attribute__((swift_name("removefromwishlist")));
+ (SSDKKotlinArray<SSDKTrackerEventType *> *)values __attribute__((swift_name("values()")));
@end;

__attribute__((swift_name("EndpointRecommendation")))
@protocol SSDKEndpointRecommendation
@required

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)getCartComplementaryPayload:(SSDKCartRecommendationRequest *)payload requestOptions:(SSDKRequestOptions *)requestOptions completionHandler_:(void (^)(SSDKRecommendationResponse * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("getCartComplementary(payload:requestOptions:completionHandler_:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)getCartRelatedPayload:(SSDKCartRecommendationRequest *)payload requestOptions:(SSDKRequestOptions *)requestOptions completionHandler_:(void (^)(SSDKRecommendationResponse * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("getCartRelated(payload:requestOptions:completionHandler_:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)getCategoryPopularPayload:(SSDKRecommendationRequest *)payload requestOptions:(SSDKRequestOptions *)requestOptions completionHandler_:(void (^)(SSDKRecommendationResponse * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("getCategoryPopular(payload:requestOptions:completionHandler_:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)getCategoryTrendingPayload:(SSDKRecommendationRequest *)payload requestOptions:(SSDKRequestOptions *)requestOptions completionHandler_:(void (^)(SSDKRecommendationResponse * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("getCategoryTrending(payload:requestOptions:completionHandler_:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)getComplementaryPayload:(SSDKRecommendationRequest *)payload requestOptions:(SSDKRequestOptions *)requestOptions completionHandler_:(void (^)(SSDKRecommendationResponse * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("getComplementary(payload:requestOptions:completionHandler_:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)getPopularPayload:(SSDKRecommendationRequest *)payload requestOptions:(SSDKRequestOptions *)requestOptions completionHandler_:(void (^)(SSDKRecommendationResponse * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("getPopular(payload:requestOptions:completionHandler_:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)getRelatedPayload:(SSDKRecommendationRequest *)payload requestOptions:(SSDKRequestOptions *)requestOptions completionHandler_:(void (^)(SSDKRecommendationResponse * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("getRelated(payload:requestOptions:completionHandler_:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)getSimilarPayload:(SSDKRecommendationRequest *)payload requestOptions:(SSDKRequestOptions *)requestOptions completionHandler_:(void (^)(SSDKRecommendationResponse * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("getSimilar(payload:requestOptions:completionHandler_:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)getTrendingPayload:(SSDKRecommendationRequest *)payload requestOptions:(SSDKRequestOptions *)requestOptions completionHandler_:(void (^)(SSDKRecommendationResponse * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("getTrending(payload:requestOptions:completionHandler_:)")));
@end;

__attribute__((swift_name("EndpointSmartSearch")))
@protocol SSDKEndpointSmartSearch
@required

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)smartSearchPayload:(SSDKSmartSearchRequest *)payload requestOptions:(SSDKRequestOptions *)requestOptions completionHandler_:(void (^)(SSDKSmartSearchResponse * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("smartSearch(payload:requestOptions:completionHandler_:)")));
@end;

__attribute__((swift_name("EndpointSmartSuggest")))
@protocol SSDKEndpointSmartSuggest
@required

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)smartSuggestPayload:(SSDKSmartSuggestRequest *)payload requestOptions:(SSDKRequestOptions *)requestOptions completionHandler_:(void (^)(SSDKSmartSuggestResponse * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("smartSuggest(payload:requestOptions:completionHandler_:)")));
@end;

__attribute__((swift_name("EndpointTracker")))
@protocol SSDKEndpointTracker
@required

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)sendCartItemAcquiredEventEvent:(SSDKCartItemAcquiredEvent *)event requestOptions:(SSDKRequestOptions *)requestOptions completionHandler:(void (^)(SSDKKtor_client_coreHttpResponse * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("sendCartItemAcquiredEvent(event:requestOptions:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)sendCartItemAcquisitionCompleteEventEvent:(SSDKCartItemAcquisitionCompleteEvent *)event requestOptions:(SSDKRequestOptions *)requestOptions completionHandler:(void (^)(SSDKKtor_client_coreHttpResponse * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("sendCartItemAcquisitionCompleteEvent(event:requestOptions:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)sendCartItemRelinquishedEventEvent:(SSDKCartItemRelinquishedEvent *)event requestOptions:(SSDKRequestOptions *)requestOptions completionHandler:(void (^)(SSDKKtor_client_coreHttpResponse * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("sendCartItemRelinquishedEvent(event:requestOptions:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)sendContainerShownEventEvent:(SSDKContainerShownEvent *)event requestOptions:(SSDKRequestOptions *)requestOptions completionHandler:(void (^)(SSDKKtor_client_coreHttpResponse * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("sendContainerShownEvent(event:requestOptions:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)sendItemClickEventEvent:(SSDKItemClickEvent *)event requestOptions:(SSDKRequestOptions *)requestOptions completionHandler:(void (^)(SSDKKtor_client_coreHttpResponse * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("sendItemClickEvent(event:requestOptions:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)sendPageViewEventEvent:(SSDKPageViewEvent *)event requestOptions:(SSDKRequestOptions *)requestOptions completionHandler:(void (^)(SSDKKtor_client_coreHttpResponse * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("sendPageViewEvent(event:requestOptions:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)sendWishlistItemAcquiredEventEvent:(SSDKWishlistItemAcquiredEvent *)event requestOptions:(SSDKRequestOptions *)requestOptions completionHandler:(void (^)(SSDKKtor_client_coreHttpResponse * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("sendWishlistItemAcquiredEvent(event:requestOptions:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)sendWishlistItemRelinquishedEventEvent:(SSDKWishlistItemRelinquishedEvent *)event requestOptions:(SSDKRequestOptions *)requestOptions completionHandler:(void (^)(SSDKKtor_client_coreHttpResponse * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("sendWishlistItemRelinquishedEvent(event:requestOptions:completionHandler:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SystemaRoutes")))
@interface SSDKSystemaRoutes : SSDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)systemaRoutes __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SSDKSystemaRoutes *shared __attribute__((swift_name("shared")));
@property (readonly) NSString *CartComplementary __attribute__((swift_name("CartComplementary")));
@property (readonly) NSString *CartRelated __attribute__((swift_name("CartRelated")));
@property (readonly) NSString *CategoryPopular __attribute__((swift_name("CategoryPopular")));
@property (readonly) NSString *CategoryTrending __attribute__((swift_name("CategoryTrending")));
@property (readonly) NSString *DynamicConfig __attribute__((swift_name("DynamicConfig")));
@property (readonly) NSString *Popular __attribute__((swift_name("Popular")));
@property (readonly) NSString *ProductComplementary __attribute__((swift_name("ProductComplementary")));
@property (readonly) NSString *ProductRelated __attribute__((swift_name("ProductRelated")));
@property (readonly) NSString *ProductSimilar __attribute__((swift_name("ProductSimilar")));
@property (readonly) NSString *SmartSearch __attribute__((swift_name("SmartSearch")));
@property (readonly) NSString *SmartSuggest __attribute__((swift_name("SmartSuggest")));
@property (readonly) NSString *Tracker __attribute__((swift_name("Tracker")));
@property (readonly) NSString *Trending __attribute__((swift_name("Trending")));
@end;

__attribute__((swift_name("SystemaDeviceManager")))
@protocol SSDKSystemaDeviceManager
@required
- (SSDKSystemaDevice *)getDeviceInfo __attribute__((swift_name("getDeviceInfo()")));
- (NSString *)getUserAgent __attribute__((swift_name("getUserAgent()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SystemaLogLevel")))
@interface SSDKSystemaLogLevel : SSDKKotlinEnum<SSDKSystemaLogLevel *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) SSDKSystemaLogLevel *none __attribute__((swift_name("none")));
@property (class, readonly) SSDKSystemaLogLevel *error __attribute__((swift_name("error")));
@property (class, readonly) SSDKSystemaLogLevel *warn __attribute__((swift_name("warn")));
@property (class, readonly) SSDKSystemaLogLevel *info __attribute__((swift_name("info")));
@property (class, readonly) SSDKSystemaLogLevel *debug __attribute__((swift_name("debug")));
+ (SSDKKotlinArray<SSDKSystemaLogLevel *> *)values __attribute__((swift_name("values()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ClientUser")))
@interface SSDKClientUser : SSDKBase
- (instancetype)initWithFingerprint:(NSString *)fingerprint sessionId:(NSString *)sessionId userIdHash:(NSString *)userIdHash userAgent:(NSString *)userAgent maxIdleDuration:(int64_t)maxIdleDuration __attribute__((swift_name("init(fingerprint:sessionId:userIdHash:userAgent:maxIdleDuration:)"))) __attribute__((objc_designated_initializer));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)nexSeqWithCompletionHandler:(void (^)(SSDKInt * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("nexSeq(completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)refreshSessionKvStore:(id<SSDKSystemaKVStore>)kvStore now:(SSDKKotlinx_datetimeInstant *)now completionHandler:(void (^)(SSDKKotlinUnit * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("refreshSession(kvStore:now:completionHandler:)")));
@property (readonly) NSString *fingerprint __attribute__((swift_name("fingerprint")));
@property (readonly) int64_t maxIdleDuration __attribute__((swift_name("maxIdleDuration")));
@property NSString *sessionId __attribute__((swift_name("sessionId")));
@property (readonly) NSString *userAgent __attribute__((swift_name("userAgent")));
@property NSString *userIdHash __attribute__((swift_name("userIdHash")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KeyValue")))
@interface SSDKKeyValue : SSDKBase
- (instancetype)initWithName:(NSString *)name value:(NSString *)value __attribute__((swift_name("init(name:value:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) SSDKKeyValueCompanion *companion __attribute__((swift_name("companion")));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (SSDKKeyValue *)doCopyName:(NSString *)name value:(NSString *)value __attribute__((swift_name("doCopy(name:value:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@property (readonly) NSString *value __attribute__((swift_name("value")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KeyValue.Companion")))
@interface SSDKKeyValueCompanion : SSDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SSDKKeyValueCompanion *shared __attribute__((swift_name("shared")));
- (id<SSDKKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SystemaAPIKey")))
@interface SSDKSystemaAPIKey : SSDKBase
- (instancetype)initWithBasic:(NSString *)basic __attribute__((swift_name("init(basic:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) SSDKSystemaAPIKeyCompanion *companion __attribute__((swift_name("companion")));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (SSDKSystemaAPIKey *)doCopyBasic:(NSString *)basic __attribute__((swift_name("doCopy(basic:)")));
- (BOOL)equalsOther:(id _Nullable)other __attribute__((swift_name("equals(other:)")));
- (int32_t)hashCode __attribute__((swift_name("hashCode()")));
- (NSString *)toString __attribute__((swift_name("toString()")));
@property (readonly) NSString *basic __attribute__((swift_name("basic")));
@end;

__attribute__((swift_name("Kotlinx_serialization_coreSerializationStrategy")))
@protocol SSDKKotlinx_serialization_coreSerializationStrategy
@required
- (void)serializeEncoder:(id<SSDKKotlinx_serialization_coreEncoder>)encoder value:(id _Nullable)value __attribute__((swift_name("serialize(encoder:value:)")));
@property (readonly) id<SSDKKotlinx_serialization_coreSerialDescriptor> descriptor __attribute__((swift_name("descriptor")));
@end;

__attribute__((swift_name("Kotlinx_serialization_coreDeserializationStrategy")))
@protocol SSDKKotlinx_serialization_coreDeserializationStrategy
@required
- (id _Nullable)deserializeDecoder:(id<SSDKKotlinx_serialization_coreDecoder>)decoder __attribute__((swift_name("deserialize(decoder:)")));
@property (readonly) id<SSDKKotlinx_serialization_coreSerialDescriptor> descriptor __attribute__((swift_name("descriptor")));
@end;

__attribute__((swift_name("Kotlinx_serialization_coreKSerializer")))
@protocol SSDKKotlinx_serialization_coreKSerializer <SSDKKotlinx_serialization_coreSerializationStrategy, SSDKKotlinx_serialization_coreDeserializationStrategy>
@required
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SystemaAPIKey.Companion")))
@interface SSDKSystemaAPIKeyCompanion : SSDKBase <SSDKKotlinx_serialization_coreKSerializer>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SSDKSystemaAPIKeyCompanion *shared __attribute__((swift_name("shared")));
- (SSDKSystemaAPIKey *)deserializeDecoder:(id<SSDKKotlinx_serialization_coreDecoder>)decoder __attribute__((swift_name("deserialize(decoder:)")));
- (void)serializeEncoder:(id<SSDKKotlinx_serialization_coreEncoder>)encoder value:(SSDKSystemaAPIKey *)value __attribute__((swift_name("serialize(encoder:value:)")));
- (id<SSDKKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@property (readonly) id<SSDKKotlinx_serialization_coreSerialDescriptor> descriptor __attribute__((swift_name("descriptor")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SystemaClientID")))
@interface SSDKSystemaClientID : SSDKBase
- (instancetype)initWithBasic:(NSString *)basic __attribute__((swift_name("init(basic:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (SSDKSystemaClientID *)doCopyBasic:(NSString *)basic __attribute__((swift_name("doCopy(basic:)")));
- (BOOL)equalsOther:(id _Nullable)other __attribute__((swift_name("equals(other:)")));
- (int32_t)hashCode __attribute__((swift_name("hashCode()")));
- (NSString *)toString __attribute__((swift_name("toString()")));
@property (readonly) NSString *basic __attribute__((swift_name("basic")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SystemaDevice")))
@interface SSDKSystemaDevice : SSDKBase
- (instancetype)initWithOsVersion:(NSString * _Nullable)osVersion model:(NSString * _Nullable)model deviceId:(NSString * _Nullable)deviceId deviceName:(NSString * _Nullable)deviceName fingerprint:(NSString * _Nullable)fingerprint release:(NSString * _Nullable)release product:(NSString * _Nullable)product brand:(NSString * _Nullable)brand display:(NSString * _Nullable)display manufacturer:(NSString * _Nullable)manufacturer __attribute__((swift_name("init(osVersion:model:deviceId:deviceName:fingerprint:release:product:brand:display:manufacturer:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) SSDKSystemaDeviceCompanion *companion __attribute__((swift_name("companion")));
- (NSString * _Nullable)component1 __attribute__((swift_name("component1()")));
- (NSString * _Nullable)component10 __attribute__((swift_name("component10()")));
- (NSString * _Nullable)component2 __attribute__((swift_name("component2()")));
- (NSString * _Nullable)component3 __attribute__((swift_name("component3()")));
- (NSString * _Nullable)component4 __attribute__((swift_name("component4()")));
- (NSString * _Nullable)component5 __attribute__((swift_name("component5()")));
- (NSString * _Nullable)component6 __attribute__((swift_name("component6()")));
- (NSString * _Nullable)component7 __attribute__((swift_name("component7()")));
- (NSString * _Nullable)component8 __attribute__((swift_name("component8()")));
- (NSString * _Nullable)component9 __attribute__((swift_name("component9()")));
- (SSDKSystemaDevice *)doCopyOsVersion:(NSString * _Nullable)osVersion model:(NSString * _Nullable)model deviceId:(NSString * _Nullable)deviceId deviceName:(NSString * _Nullable)deviceName fingerprint:(NSString * _Nullable)fingerprint release:(NSString * _Nullable)release product:(NSString * _Nullable)product brand:(NSString * _Nullable)brand display:(NSString * _Nullable)display manufacturer:(NSString * _Nullable)manufacturer __attribute__((swift_name("doCopy(osVersion:model:deviceId:deviceName:fingerprint:release:product:brand:display:manufacturer:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString * _Nullable brand __attribute__((swift_name("brand")));
@property (readonly) NSString * _Nullable deviceId __attribute__((swift_name("deviceId")));
@property (readonly) NSString * _Nullable deviceName __attribute__((swift_name("deviceName")));
@property (readonly) NSString * _Nullable display __attribute__((swift_name("display")));
@property (readonly) NSString * _Nullable fingerprint __attribute__((swift_name("fingerprint")));
@property (readonly) NSString * _Nullable manufacturer __attribute__((swift_name("manufacturer")));
@property (readonly) NSString * _Nullable model __attribute__((swift_name("model")));
@property (readonly) NSString * _Nullable osVersion __attribute__((swift_name("osVersion")));
@property (readonly) NSString * _Nullable product __attribute__((swift_name("product")));
@property (readonly, getter=release_) NSString * _Nullable release __attribute__((swift_name("release")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SystemaDevice.Companion")))
@interface SSDKSystemaDeviceCompanion : SSDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SSDKSystemaDeviceCompanion *shared __attribute__((swift_name("shared")));
- (id<SSDKKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SystemaResult")))
@interface SSDKSystemaResult<__covariant T> : SSDKBase
- (instancetype)initWithValue:(id)value __attribute__((swift_name("init(value:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) SSDKSystemaResultCompanion *companion __attribute__((swift_name("companion")));
- (SSDKKotlinException *)error __attribute__((swift_name("error()")));
- (T _Nullable)value __attribute__((swift_name("value()")));
@property (readonly) BOOL isError __attribute__((swift_name("isError")));
@property (readonly) BOOL isSuccessful __attribute__((swift_name("isSuccessful")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SystemaResultCompanion")))
@interface SSDKSystemaResultCompanion : SSDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SSDKSystemaResultCompanion *shared __attribute__((swift_name("shared")));
- (SSDKSystemaResult<id> *)errorResult:(SSDKSystemaResult<id> *)result __attribute__((swift_name("error(result:)")));
- (SSDKSystemaResult<id> *)errorError:(SSDKKotlinException *)error __attribute__((swift_name("error(error:)")));
- (SSDKSystemaResult<id> *)successValue:(id)value __attribute__((swift_name("success(value:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SystemaUser")))
@interface SSDKSystemaUser : SSDKBase
- (instancetype)initWithFingerprint:(NSString *)fingerprint sessionId:(NSString *)sessionId userIdHash:(NSString *)userIdHash userAgent:(NSString *)userAgent sequence:(int32_t)sequence sessionCreatedAt:(SSDKKotlinx_datetimeInstant * _Nullable)sessionCreatedAt snapshotAt:(SSDKKotlinx_datetimeInstant *)snapshotAt __attribute__((swift_name("init(fingerprint:sessionId:userIdHash:userAgent:sequence:sessionCreatedAt:snapshotAt:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) SSDKSystemaUserCompanion *companion __attribute__((swift_name("companion")));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (NSString *)component3 __attribute__((swift_name("component3()")));
- (NSString *)component4 __attribute__((swift_name("component4()")));
- (int32_t)component5 __attribute__((swift_name("component5()")));
- (SSDKKotlinx_datetimeInstant * _Nullable)component6 __attribute__((swift_name("component6()")));
- (SSDKKotlinx_datetimeInstant *)component7 __attribute__((swift_name("component7()")));
- (SSDKSystemaUser *)doCopyFingerprint:(NSString *)fingerprint sessionId:(NSString *)sessionId userIdHash:(NSString *)userIdHash userAgent:(NSString *)userAgent sequence:(int32_t)sequence sessionCreatedAt:(SSDKKotlinx_datetimeInstant * _Nullable)sessionCreatedAt snapshotAt:(SSDKKotlinx_datetimeInstant *)snapshotAt __attribute__((swift_name("doCopy(fingerprint:sessionId:userIdHash:userAgent:sequence:sessionCreatedAt:snapshotAt:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *fingerprint __attribute__((swift_name("fingerprint")));
@property (readonly) int32_t sequence __attribute__((swift_name("sequence")));
@property (readonly) SSDKKotlinx_datetimeInstant * _Nullable sessionCreatedAt __attribute__((swift_name("sessionCreatedAt")));
@property (readonly) NSString *sessionId __attribute__((swift_name("sessionId")));
@property (readonly) SSDKKotlinx_datetimeInstant *snapshotAt __attribute__((swift_name("snapshotAt")));
@property (readonly) NSString *userAgent __attribute__((swift_name("userAgent")));
@property (readonly) NSString *userIdHash __attribute__((swift_name("userIdHash")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SystemaUser.Companion")))
@interface SSDKSystemaUserCompanion : SSDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SSDKSystemaUserCompanion *shared __attribute__((swift_name("shared")));
- (id<SSDKKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SystemaErrorEvent")))
@interface SSDKSystemaErrorEvent : SSDKBase
- (instancetype)initWithType:(NSString * _Nullable)type payload:(SSDKKotlinThrowable *)payload createdAt:(SSDKKotlinx_datetimeInstant *)createdAt __attribute__((swift_name("init(type:payload:createdAt:)"))) __attribute__((objc_designated_initializer));
- (NSString * _Nullable)component1 __attribute__((swift_name("component1()")));
- (SSDKKotlinThrowable *)component2 __attribute__((swift_name("component2()")));
- (SSDKKotlinx_datetimeInstant *)component3 __attribute__((swift_name("component3()")));
- (SSDKSystemaErrorEvent *)doCopyType:(NSString * _Nullable)type payload:(SSDKKotlinThrowable *)payload createdAt:(SSDKKotlinx_datetimeInstant *)createdAt __attribute__((swift_name("doCopy(type:payload:createdAt:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) SSDKKotlinx_datetimeInstant *createdAt __attribute__((swift_name("createdAt")));
@property (readonly) SSDKKotlinThrowable *payload __attribute__((swift_name("payload")));
@property (readonly) NSString * _Nullable type __attribute__((swift_name("type")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SystemaLogEvent")))
@interface SSDKSystemaLogEvent : SSDKBase
- (instancetype)initWithType:(NSString * _Nullable)type payload:(NSString *)payload createdAt:(SSDKKotlinx_datetimeInstant *)createdAt __attribute__((swift_name("init(type:payload:createdAt:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) SSDKSystemaLogEventCompanion *companion __attribute__((swift_name("companion")));
- (NSString * _Nullable)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (SSDKKotlinx_datetimeInstant *)component3 __attribute__((swift_name("component3()")));
- (SSDKSystemaLogEvent *)doCopyType:(NSString * _Nullable)type payload:(NSString *)payload createdAt:(SSDKKotlinx_datetimeInstant *)createdAt __attribute__((swift_name("doCopy(type:payload:createdAt:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) SSDKKotlinx_datetimeInstant *createdAt __attribute__((swift_name("createdAt")));
@property (readonly) NSString *payload __attribute__((swift_name("payload")));
@property (readonly) NSString * _Nullable type __attribute__((swift_name("type")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SystemaLogEvent.Companion")))
@interface SSDKSystemaLogEventCompanion : SSDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SSDKSystemaLogEventCompanion *shared __attribute__((swift_name("shared")));
- (id<SSDKKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TrackEventDate")))
@interface SSDKTrackEventDate : SSDKBase
- (instancetype)initWithLocalDate:(NSString *)localDate timeZone:(NSString *)timeZone utcDate:(NSString *)utcDate __attribute__((swift_name("init(localDate:timeZone:utcDate:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) SSDKTrackEventDateCompanion *companion __attribute__((swift_name("companion")));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (NSString *)component3 __attribute__((swift_name("component3()")));
- (SSDKTrackEventDate *)doCopyLocalDate:(NSString *)localDate timeZone:(NSString *)timeZone utcDate:(NSString *)utcDate __attribute__((swift_name("doCopy(localDate:timeZone:utcDate:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *localDate __attribute__((swift_name("localDate")));
@property (readonly) NSString *timeZone __attribute__((swift_name("timeZone")));
@property (readonly) NSString *utcDate __attribute__((swift_name("utcDate")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TrackEventDate.Companion")))
@interface SSDKTrackEventDateCompanion : SSDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SSDKTrackEventDateCompanion *shared __attribute__((swift_name("shared")));
- (id<SSDKKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("WishlistItem")))
@interface SSDKWishlistItem : SSDKBase
- (instancetype)initWithItemId:(NSString *)itemId __attribute__((swift_name("init(itemId:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) SSDKWishlistItemCompanion *companion __attribute__((swift_name("companion")));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (SSDKWishlistItem *)doCopyItemId:(NSString *)itemId __attribute__((swift_name("doCopy(itemId:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *itemId __attribute__((swift_name("itemId")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("WishlistItem.Companion")))
@interface SSDKWishlistItemCompanion : SSDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SSDKWishlistItemCompanion *shared __attribute__((swift_name("shared")));
- (id<SSDKKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("WishlistItemAcquiredEvent")))
@interface SSDKWishlistItemAcquiredEvent : SSDKBase
- (instancetype)initWithClientId:(NSString *)clientId environment:(NSString *)environment fingerprint:(NSString *)fingerprint sessionId:(NSString *)sessionId userAgent:(NSString *)userAgent userName:(NSString * _Nullable)userName sequence:(int32_t)sequence type:(SSDKTrackerEventType *)type productId:(NSString *)productId referrer:(NSString * _Nullable)referrer url:(NSString *)url eventDate:(SSDKTrackEventDate *)eventDate items:(NSArray<SSDKWishlistItem *> *)items version:(NSString *)version __attribute__((swift_name("init(clientId:environment:fingerprint:sessionId:userAgent:userName:sequence:type:productId:referrer:url:eventDate:items:version:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) SSDKWishlistItemAcquiredEventCompanion *companion __attribute__((swift_name("companion")));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString * _Nullable)component10 __attribute__((swift_name("component10()")));
- (NSString *)component11 __attribute__((swift_name("component11()")));
- (SSDKTrackEventDate *)component12 __attribute__((swift_name("component12()")));
- (NSArray<SSDKWishlistItem *> *)component13 __attribute__((swift_name("component13()")));
- (NSString *)component14 __attribute__((swift_name("component14()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (NSString *)component3 __attribute__((swift_name("component3()")));
- (NSString *)component4 __attribute__((swift_name("component4()")));
- (NSString *)component5 __attribute__((swift_name("component5()")));
- (NSString * _Nullable)component6 __attribute__((swift_name("component6()")));
- (int32_t)component7 __attribute__((swift_name("component7()")));
- (SSDKTrackerEventType *)component8 __attribute__((swift_name("component8()")));
- (NSString *)component9 __attribute__((swift_name("component9()")));
- (SSDKWishlistItemAcquiredEvent *)doCopyClientId:(NSString *)clientId environment:(NSString *)environment fingerprint:(NSString *)fingerprint sessionId:(NSString *)sessionId userAgent:(NSString *)userAgent userName:(NSString * _Nullable)userName sequence:(int32_t)sequence type:(SSDKTrackerEventType *)type productId:(NSString *)productId referrer:(NSString * _Nullable)referrer url:(NSString *)url eventDate:(SSDKTrackEventDate *)eventDate items:(NSArray<SSDKWishlistItem *> *)items version:(NSString *)version __attribute__((swift_name("doCopy(clientId:environment:fingerprint:sessionId:userAgent:userName:sequence:type:productId:referrer:url:eventDate:items:version:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *clientId __attribute__((swift_name("clientId")));
@property (readonly) NSString *environment __attribute__((swift_name("environment")));
@property (readonly) SSDKTrackEventDate *eventDate __attribute__((swift_name("eventDate")));
@property (readonly) NSString *fingerprint __attribute__((swift_name("fingerprint")));
@property (readonly) NSArray<SSDKWishlistItem *> *items __attribute__((swift_name("items")));
@property (readonly) NSString *productId __attribute__((swift_name("productId")));
@property (readonly) NSString * _Nullable referrer __attribute__((swift_name("referrer")));
@property (readonly) int32_t sequence __attribute__((swift_name("sequence")));
@property (readonly) NSString *sessionId __attribute__((swift_name("sessionId")));
@property (readonly) SSDKTrackerEventType *type __attribute__((swift_name("type")));
@property (readonly) NSString *url __attribute__((swift_name("url")));
@property (readonly) NSString *userAgent __attribute__((swift_name("userAgent")));
@property (readonly) NSString * _Nullable userName __attribute__((swift_name("userName")));
@property (readonly) NSString *version __attribute__((swift_name("version")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("WishlistItemAcquiredEvent.Companion")))
@interface SSDKWishlistItemAcquiredEventCompanion : SSDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SSDKWishlistItemAcquiredEventCompanion *shared __attribute__((swift_name("shared")));
- (id<SSDKKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("WishlistItemRelinquishedEvent")))
@interface SSDKWishlistItemRelinquishedEvent : SSDKBase
- (instancetype)initWithClientId:(NSString *)clientId environment:(NSString *)environment fingerprint:(NSString *)fingerprint sessionId:(NSString *)sessionId userAgent:(NSString *)userAgent userName:(NSString * _Nullable)userName sequence:(int32_t)sequence type:(SSDKTrackerEventType *)type productId:(NSString *)productId referrer:(NSString * _Nullable)referrer url:(NSString *)url eventDate:(SSDKTrackEventDate *)eventDate item:(SSDKWishlistItem *)item version:(NSString *)version __attribute__((swift_name("init(clientId:environment:fingerprint:sessionId:userAgent:userName:sequence:type:productId:referrer:url:eventDate:item:version:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) SSDKWishlistItemRelinquishedEventCompanion *companion __attribute__((swift_name("companion")));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString * _Nullable)component10 __attribute__((swift_name("component10()")));
- (NSString *)component11 __attribute__((swift_name("component11()")));
- (SSDKTrackEventDate *)component12 __attribute__((swift_name("component12()")));
- (SSDKWishlistItem *)component13 __attribute__((swift_name("component13()")));
- (NSString *)component14 __attribute__((swift_name("component14()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (NSString *)component3 __attribute__((swift_name("component3()")));
- (NSString *)component4 __attribute__((swift_name("component4()")));
- (NSString *)component5 __attribute__((swift_name("component5()")));
- (NSString * _Nullable)component6 __attribute__((swift_name("component6()")));
- (int32_t)component7 __attribute__((swift_name("component7()")));
- (SSDKTrackerEventType *)component8 __attribute__((swift_name("component8()")));
- (NSString *)component9 __attribute__((swift_name("component9()")));
- (SSDKWishlistItemRelinquishedEvent *)doCopyClientId:(NSString *)clientId environment:(NSString *)environment fingerprint:(NSString *)fingerprint sessionId:(NSString *)sessionId userAgent:(NSString *)userAgent userName:(NSString * _Nullable)userName sequence:(int32_t)sequence type:(SSDKTrackerEventType *)type productId:(NSString *)productId referrer:(NSString * _Nullable)referrer url:(NSString *)url eventDate:(SSDKTrackEventDate *)eventDate item:(SSDKWishlistItem *)item version:(NSString *)version __attribute__((swift_name("doCopy(clientId:environment:fingerprint:sessionId:userAgent:userName:sequence:type:productId:referrer:url:eventDate:item:version:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *clientId __attribute__((swift_name("clientId")));
@property (readonly) NSString *environment __attribute__((swift_name("environment")));
@property (readonly) SSDKTrackEventDate *eventDate __attribute__((swift_name("eventDate")));
@property (readonly) NSString *fingerprint __attribute__((swift_name("fingerprint")));
@property (readonly) SSDKWishlistItem *item __attribute__((swift_name("item")));
@property (readonly) NSString *productId __attribute__((swift_name("productId")));
@property (readonly) NSString * _Nullable referrer __attribute__((swift_name("referrer")));
@property (readonly) int32_t sequence __attribute__((swift_name("sequence")));
@property (readonly) NSString *sessionId __attribute__((swift_name("sessionId")));
@property (readonly) SSDKTrackerEventType *type __attribute__((swift_name("type")));
@property (readonly) NSString *url __attribute__((swift_name("url")));
@property (readonly) NSString *userAgent __attribute__((swift_name("userAgent")));
@property (readonly) NSString * _Nullable userName __attribute__((swift_name("userName")));
@property (readonly) NSString *version __attribute__((swift_name("version")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("WishlistItemRelinquishedEvent.Companion")))
@interface SSDKWishlistItemRelinquishedEventCompanion : SSDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SSDKWishlistItemRelinquishedEventCompanion *shared __attribute__((swift_name("shared")));
- (id<SSDKKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CartItem")))
@interface SSDKCartItem : SSDKBase
- (instancetype)initWithItemId:(NSString *)itemId quantity:(int32_t)quantity price:(SSDKDouble * _Nullable)price tax:(SSDKDouble * _Nullable)tax currency:(SSDKCurrency * _Nullable)currency __attribute__((swift_name("init(itemId:quantity:price:tax:currency:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) SSDKCartItemCompanion *companion __attribute__((swift_name("companion")));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (int32_t)component2 __attribute__((swift_name("component2()")));
- (SSDKDouble * _Nullable)component3 __attribute__((swift_name("component3()")));
- (SSDKDouble * _Nullable)component4 __attribute__((swift_name("component4()")));
- (SSDKCurrency * _Nullable)component5 __attribute__((swift_name("component5()")));
- (SSDKCartItem *)doCopyItemId:(NSString *)itemId quantity:(int32_t)quantity price:(SSDKDouble * _Nullable)price tax:(SSDKDouble * _Nullable)tax currency:(SSDKCurrency * _Nullable)currency __attribute__((swift_name("doCopy(itemId:quantity:price:tax:currency:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) SSDKCurrency * _Nullable currency __attribute__((swift_name("currency")));
@property (readonly) NSString *itemId __attribute__((swift_name("itemId")));
@property (readonly) SSDKDouble * _Nullable price __attribute__((swift_name("price")));
@property (readonly) int32_t quantity __attribute__((swift_name("quantity")));
@property (readonly) SSDKDouble * _Nullable tax __attribute__((swift_name("tax")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CartItem.Companion")))
@interface SSDKCartItemCompanion : SSDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SSDKCartItemCompanion *shared __attribute__((swift_name("shared")));
- (id<SSDKKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CartItemAcquiredEvent")))
@interface SSDKCartItemAcquiredEvent : SSDKBase
- (instancetype)initWithClientId:(NSString *)clientId environment:(NSString *)environment fingerprint:(NSString *)fingerprint sessionId:(NSString *)sessionId userAgent:(NSString *)userAgent userName:(NSString * _Nullable)userName sequence:(int32_t)sequence type:(SSDKTrackerEventType *)type productId:(NSString *)productId referrer:(NSString * _Nullable)referrer url:(NSString *)url eventDate:(SSDKTrackEventDate *)eventDate items:(NSArray<SSDKCartItem *> *)items version:(NSString *)version __attribute__((swift_name("init(clientId:environment:fingerprint:sessionId:userAgent:userName:sequence:type:productId:referrer:url:eventDate:items:version:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) SSDKCartItemAcquiredEventCompanion *companion __attribute__((swift_name("companion")));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString * _Nullable)component10 __attribute__((swift_name("component10()")));
- (NSString *)component11 __attribute__((swift_name("component11()")));
- (SSDKTrackEventDate *)component12 __attribute__((swift_name("component12()")));
- (NSArray<SSDKCartItem *> *)component13 __attribute__((swift_name("component13()")));
- (NSString *)component14 __attribute__((swift_name("component14()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (NSString *)component3 __attribute__((swift_name("component3()")));
- (NSString *)component4 __attribute__((swift_name("component4()")));
- (NSString *)component5 __attribute__((swift_name("component5()")));
- (NSString * _Nullable)component6 __attribute__((swift_name("component6()")));
- (int32_t)component7 __attribute__((swift_name("component7()")));
- (SSDKTrackerEventType *)component8 __attribute__((swift_name("component8()")));
- (NSString *)component9 __attribute__((swift_name("component9()")));
- (SSDKCartItemAcquiredEvent *)doCopyClientId:(NSString *)clientId environment:(NSString *)environment fingerprint:(NSString *)fingerprint sessionId:(NSString *)sessionId userAgent:(NSString *)userAgent userName:(NSString * _Nullable)userName sequence:(int32_t)sequence type:(SSDKTrackerEventType *)type productId:(NSString *)productId referrer:(NSString * _Nullable)referrer url:(NSString *)url eventDate:(SSDKTrackEventDate *)eventDate items:(NSArray<SSDKCartItem *> *)items version:(NSString *)version __attribute__((swift_name("doCopy(clientId:environment:fingerprint:sessionId:userAgent:userName:sequence:type:productId:referrer:url:eventDate:items:version:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *clientId __attribute__((swift_name("clientId")));
@property (readonly) NSString *environment __attribute__((swift_name("environment")));
@property (readonly) SSDKTrackEventDate *eventDate __attribute__((swift_name("eventDate")));
@property (readonly) NSString *fingerprint __attribute__((swift_name("fingerprint")));
@property (readonly) NSArray<SSDKCartItem *> *items __attribute__((swift_name("items")));
@property (readonly) NSString *productId __attribute__((swift_name("productId")));
@property (readonly) NSString * _Nullable referrer __attribute__((swift_name("referrer")));
@property (readonly) int32_t sequence __attribute__((swift_name("sequence")));
@property (readonly) NSString *sessionId __attribute__((swift_name("sessionId")));
@property (readonly) SSDKTrackerEventType *type __attribute__((swift_name("type")));
@property (readonly) NSString *url __attribute__((swift_name("url")));
@property (readonly) NSString *userAgent __attribute__((swift_name("userAgent")));
@property (readonly) NSString * _Nullable userName __attribute__((swift_name("userName")));
@property (readonly) NSString *version __attribute__((swift_name("version")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CartItemAcquiredEvent.Companion")))
@interface SSDKCartItemAcquiredEventCompanion : SSDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SSDKCartItemAcquiredEventCompanion *shared __attribute__((swift_name("shared")));
- (id<SSDKKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CartItemAcquisitionCompleteEvent")))
@interface SSDKCartItemAcquisitionCompleteEvent : SSDKBase
- (instancetype)initWithClientId:(NSString *)clientId environment:(NSString *)environment fingerprint:(NSString *)fingerprint sessionId:(NSString *)sessionId userAgent:(NSString *)userAgent userName:(NSString * _Nullable)userName sequence:(int32_t)sequence type:(SSDKTrackerEventType *)type referrer:(NSString * _Nullable)referrer url:(NSString *)url eventDate:(SSDKTrackEventDate *)eventDate order:(SSDKPurchaseOrder *)order version:(NSString *)version __attribute__((swift_name("init(clientId:environment:fingerprint:sessionId:userAgent:userName:sequence:type:referrer:url:eventDate:order:version:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) SSDKCartItemAcquisitionCompleteEventCompanion *companion __attribute__((swift_name("companion")));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component10 __attribute__((swift_name("component10()")));
- (SSDKTrackEventDate *)component11 __attribute__((swift_name("component11()")));
- (SSDKPurchaseOrder *)component12 __attribute__((swift_name("component12()")));
- (NSString *)component13 __attribute__((swift_name("component13()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (NSString *)component3 __attribute__((swift_name("component3()")));
- (NSString *)component4 __attribute__((swift_name("component4()")));
- (NSString *)component5 __attribute__((swift_name("component5()")));
- (NSString * _Nullable)component6 __attribute__((swift_name("component6()")));
- (int32_t)component7 __attribute__((swift_name("component7()")));
- (SSDKTrackerEventType *)component8 __attribute__((swift_name("component8()")));
- (NSString * _Nullable)component9 __attribute__((swift_name("component9()")));
- (SSDKCartItemAcquisitionCompleteEvent *)doCopyClientId:(NSString *)clientId environment:(NSString *)environment fingerprint:(NSString *)fingerprint sessionId:(NSString *)sessionId userAgent:(NSString *)userAgent userName:(NSString * _Nullable)userName sequence:(int32_t)sequence type:(SSDKTrackerEventType *)type referrer:(NSString * _Nullable)referrer url:(NSString *)url eventDate:(SSDKTrackEventDate *)eventDate order:(SSDKPurchaseOrder *)order version:(NSString *)version __attribute__((swift_name("doCopy(clientId:environment:fingerprint:sessionId:userAgent:userName:sequence:type:referrer:url:eventDate:order:version:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *clientId __attribute__((swift_name("clientId")));
@property (readonly) NSString *environment __attribute__((swift_name("environment")));
@property (readonly) SSDKTrackEventDate *eventDate __attribute__((swift_name("eventDate")));
@property (readonly) NSString *fingerprint __attribute__((swift_name("fingerprint")));
@property (readonly) SSDKPurchaseOrder *order __attribute__((swift_name("order")));
@property (readonly) NSString * _Nullable referrer __attribute__((swift_name("referrer")));
@property (readonly) int32_t sequence __attribute__((swift_name("sequence")));
@property (readonly) NSString *sessionId __attribute__((swift_name("sessionId")));
@property (readonly) SSDKTrackerEventType *type __attribute__((swift_name("type")));
@property (readonly) NSString *url __attribute__((swift_name("url")));
@property (readonly) NSString *userAgent __attribute__((swift_name("userAgent")));
@property (readonly) NSString * _Nullable userName __attribute__((swift_name("userName")));
@property (readonly) NSString *version __attribute__((swift_name("version")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CartItemAcquisitionCompleteEvent.Companion")))
@interface SSDKCartItemAcquisitionCompleteEventCompanion : SSDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SSDKCartItemAcquisitionCompleteEventCompanion *shared __attribute__((swift_name("shared")));
- (id<SSDKKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CartItemRelinquishedEvent")))
@interface SSDKCartItemRelinquishedEvent : SSDKBase
- (instancetype)initWithClientId:(NSString *)clientId environment:(NSString *)environment fingerprint:(NSString *)fingerprint sessionId:(NSString *)sessionId userAgent:(NSString *)userAgent userName:(NSString * _Nullable)userName sequence:(int32_t)sequence type:(SSDKTrackerEventType *)type productId:(NSString *)productId referrer:(NSString * _Nullable)referrer url:(NSString *)url eventDate:(SSDKTrackEventDate *)eventDate item:(SSDKCartItem *)item version:(NSString *)version __attribute__((swift_name("init(clientId:environment:fingerprint:sessionId:userAgent:userName:sequence:type:productId:referrer:url:eventDate:item:version:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) SSDKCartItemRelinquishedEventCompanion *companion __attribute__((swift_name("companion")));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString * _Nullable)component10 __attribute__((swift_name("component10()")));
- (NSString *)component11 __attribute__((swift_name("component11()")));
- (SSDKTrackEventDate *)component12 __attribute__((swift_name("component12()")));
- (SSDKCartItem *)component13 __attribute__((swift_name("component13()")));
- (NSString *)component14 __attribute__((swift_name("component14()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (NSString *)component3 __attribute__((swift_name("component3()")));
- (NSString *)component4 __attribute__((swift_name("component4()")));
- (NSString *)component5 __attribute__((swift_name("component5()")));
- (NSString * _Nullable)component6 __attribute__((swift_name("component6()")));
- (int32_t)component7 __attribute__((swift_name("component7()")));
- (SSDKTrackerEventType *)component8 __attribute__((swift_name("component8()")));
- (NSString *)component9 __attribute__((swift_name("component9()")));
- (SSDKCartItemRelinquishedEvent *)doCopyClientId:(NSString *)clientId environment:(NSString *)environment fingerprint:(NSString *)fingerprint sessionId:(NSString *)sessionId userAgent:(NSString *)userAgent userName:(NSString * _Nullable)userName sequence:(int32_t)sequence type:(SSDKTrackerEventType *)type productId:(NSString *)productId referrer:(NSString * _Nullable)referrer url:(NSString *)url eventDate:(SSDKTrackEventDate *)eventDate item:(SSDKCartItem *)item version:(NSString *)version __attribute__((swift_name("doCopy(clientId:environment:fingerprint:sessionId:userAgent:userName:sequence:type:productId:referrer:url:eventDate:item:version:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *clientId __attribute__((swift_name("clientId")));
@property (readonly) NSString *environment __attribute__((swift_name("environment")));
@property (readonly) SSDKTrackEventDate *eventDate __attribute__((swift_name("eventDate")));
@property (readonly) NSString *fingerprint __attribute__((swift_name("fingerprint")));
@property (readonly) SSDKCartItem *item __attribute__((swift_name("item")));
@property (readonly) NSString *productId __attribute__((swift_name("productId")));
@property (readonly) NSString * _Nullable referrer __attribute__((swift_name("referrer")));
@property (readonly) int32_t sequence __attribute__((swift_name("sequence")));
@property (readonly) NSString *sessionId __attribute__((swift_name("sessionId")));
@property (readonly) SSDKTrackerEventType *type __attribute__((swift_name("type")));
@property (readonly) NSString *url __attribute__((swift_name("url")));
@property (readonly) NSString *userAgent __attribute__((swift_name("userAgent")));
@property (readonly) NSString * _Nullable userName __attribute__((swift_name("userName")));
@property (readonly) NSString *version __attribute__((swift_name("version")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CartItemRelinquishedEvent.Companion")))
@interface SSDKCartItemRelinquishedEventCompanion : SSDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SSDKCartItemRelinquishedEventCompanion *shared __attribute__((swift_name("shared")));
- (id<SSDKKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OrderItem")))
@interface SSDKOrderItem : SSDKBase
- (instancetype)initWithItemId:(NSString *)itemId quantity:(int32_t)quantity unitCost:(double)unitCost unitTaxAmount:(double)unitTaxAmount currency:(NSString *)currency __attribute__((swift_name("init(itemId:quantity:unitCost:unitTaxAmount:currency:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) SSDKOrderItemCompanion *companion __attribute__((swift_name("companion")));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (int32_t)component2 __attribute__((swift_name("component2()")));
- (double)component3 __attribute__((swift_name("component3()")));
- (double)component4 __attribute__((swift_name("component4()")));
- (NSString *)component5 __attribute__((swift_name("component5()")));
- (SSDKOrderItem *)doCopyItemId:(NSString *)itemId quantity:(int32_t)quantity unitCost:(double)unitCost unitTaxAmount:(double)unitTaxAmount currency:(NSString *)currency __attribute__((swift_name("doCopy(itemId:quantity:unitCost:unitTaxAmount:currency:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *currency __attribute__((swift_name("currency")));
@property (readonly) NSString *itemId __attribute__((swift_name("itemId")));
@property (readonly) int32_t quantity __attribute__((swift_name("quantity")));
@property (readonly) double unitCost __attribute__((swift_name("unitCost")));
@property (readonly) double unitTaxAmount __attribute__((swift_name("unitTaxAmount")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OrderItem.Companion")))
@interface SSDKOrderItemCompanion : SSDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SSDKOrderItemCompanion *shared __attribute__((swift_name("shared")));
- (id<SSDKKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("PurchaseOrder")))
@interface SSDKPurchaseOrder : SSDKBase
- (instancetype)initWithOrderId:(NSString *)orderId chargedAmount:(double)chargedAmount totalAmount:(double)totalAmount taxAmount:(double)taxAmount shippingAmount:(double)shippingAmount discountAmount:(SSDKDouble * _Nullable)discountAmount discountCodes:(NSString * _Nullable)discountCodes currency:(NSString * _Nullable)currency shippingAddress:(SSDKShippingAddress * _Nullable)shippingAddress items:(NSArray<SSDKOrderItem *> *)items __attribute__((swift_name("init(orderId:chargedAmount:totalAmount:taxAmount:shippingAmount:discountAmount:discountCodes:currency:shippingAddress:items:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) SSDKPurchaseOrderCompanion *companion __attribute__((swift_name("companion")));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSArray<SSDKOrderItem *> *)component10 __attribute__((swift_name("component10()")));
- (double)component2 __attribute__((swift_name("component2()")));
- (double)component3 __attribute__((swift_name("component3()")));
- (double)component4 __attribute__((swift_name("component4()")));
- (double)component5 __attribute__((swift_name("component5()")));
- (SSDKDouble * _Nullable)component6 __attribute__((swift_name("component6()")));
- (NSString * _Nullable)component7 __attribute__((swift_name("component7()")));
- (NSString * _Nullable)component8 __attribute__((swift_name("component8()")));
- (SSDKShippingAddress * _Nullable)component9 __attribute__((swift_name("component9()")));
- (SSDKPurchaseOrder *)doCopyOrderId:(NSString *)orderId chargedAmount:(double)chargedAmount totalAmount:(double)totalAmount taxAmount:(double)taxAmount shippingAmount:(double)shippingAmount discountAmount:(SSDKDouble * _Nullable)discountAmount discountCodes:(NSString * _Nullable)discountCodes currency:(NSString * _Nullable)currency shippingAddress:(SSDKShippingAddress * _Nullable)shippingAddress items:(NSArray<SSDKOrderItem *> *)items __attribute__((swift_name("doCopy(orderId:chargedAmount:totalAmount:taxAmount:shippingAmount:discountAmount:discountCodes:currency:shippingAddress:items:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) double chargedAmount __attribute__((swift_name("chargedAmount")));
@property (readonly) NSString * _Nullable currency __attribute__((swift_name("currency")));
@property (readonly) SSDKDouble * _Nullable discountAmount __attribute__((swift_name("discountAmount")));
@property (readonly) NSString * _Nullable discountCodes __attribute__((swift_name("discountCodes")));
@property (readonly) NSArray<SSDKOrderItem *> *items __attribute__((swift_name("items")));
@property (readonly) NSString *orderId __attribute__((swift_name("orderId")));
@property (readonly) SSDKShippingAddress * _Nullable shippingAddress __attribute__((swift_name("shippingAddress")));
@property (readonly) double shippingAmount __attribute__((swift_name("shippingAmount")));
@property (readonly) double taxAmount __attribute__((swift_name("taxAmount")));
@property (readonly) double totalAmount __attribute__((swift_name("totalAmount")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("PurchaseOrder.Companion")))
@interface SSDKPurchaseOrderCompanion : SSDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SSDKPurchaseOrderCompanion *shared __attribute__((swift_name("shared")));
- (id<SSDKKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ShippingAddress")))
@interface SSDKShippingAddress : SSDKBase
- (instancetype)initWithCity:(NSString *)city state:(NSString *)state postCode:(NSString *)postCode country:(NSString *)country __attribute__((swift_name("init(city:state:postCode:country:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) SSDKShippingAddressCompanion *companion __attribute__((swift_name("companion")));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (NSString *)component3 __attribute__((swift_name("component3()")));
- (NSString *)component4 __attribute__((swift_name("component4()")));
- (SSDKShippingAddress *)doCopyCity:(NSString *)city state:(NSString *)state postCode:(NSString *)postCode country:(NSString *)country __attribute__((swift_name("doCopy(city:state:postCode:country:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *city __attribute__((swift_name("city")));
@property (readonly) NSString *country __attribute__((swift_name("country")));
@property (readonly) NSString *postCode __attribute__((swift_name("postCode")));
@property (readonly) NSString *state __attribute__((swift_name("state")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ShippingAddress.Companion")))
@interface SSDKShippingAddressCompanion : SSDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SSDKShippingAddressCompanion *shared __attribute__((swift_name("shared")));
- (id<SSDKKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ContainerShownEvent")))
@interface SSDKContainerShownEvent : SSDKBase
- (instancetype)initWithClientId:(NSString *)clientId environment:(NSString *)environment fingerprint:(NSString *)fingerprint sessionId:(NSString *)sessionId userAgent:(NSString *)userAgent userName:(NSString * _Nullable)userName sequence:(int32_t)sequence type:(SSDKTrackerEventType *)type productId:(NSString * _Nullable)productId referrer:(NSString * _Nullable)referrer url:(NSString *)url eventDate:(SSDKTrackEventDate *)eventDate containers:(NSArray<SSDKItemContainer *> * _Nullable)containers version:(NSString *)version __attribute__((swift_name("init(clientId:environment:fingerprint:sessionId:userAgent:userName:sequence:type:productId:referrer:url:eventDate:containers:version:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) SSDKContainerShownEventCompanion *companion __attribute__((swift_name("companion")));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString * _Nullable)component10 __attribute__((swift_name("component10()")));
- (NSString *)component11 __attribute__((swift_name("component11()")));
- (SSDKTrackEventDate *)component12 __attribute__((swift_name("component12()")));
- (NSArray<SSDKItemContainer *> * _Nullable)component13 __attribute__((swift_name("component13()")));
- (NSString *)component14 __attribute__((swift_name("component14()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (NSString *)component3 __attribute__((swift_name("component3()")));
- (NSString *)component4 __attribute__((swift_name("component4()")));
- (NSString *)component5 __attribute__((swift_name("component5()")));
- (NSString * _Nullable)component6 __attribute__((swift_name("component6()")));
- (int32_t)component7 __attribute__((swift_name("component7()")));
- (SSDKTrackerEventType *)component8 __attribute__((swift_name("component8()")));
- (NSString * _Nullable)component9 __attribute__((swift_name("component9()")));
- (SSDKContainerShownEvent *)doCopyClientId:(NSString *)clientId environment:(NSString *)environment fingerprint:(NSString *)fingerprint sessionId:(NSString *)sessionId userAgent:(NSString *)userAgent userName:(NSString * _Nullable)userName sequence:(int32_t)sequence type:(SSDKTrackerEventType *)type productId:(NSString * _Nullable)productId referrer:(NSString * _Nullable)referrer url:(NSString *)url eventDate:(SSDKTrackEventDate *)eventDate containers:(NSArray<SSDKItemContainer *> * _Nullable)containers version:(NSString *)version __attribute__((swift_name("doCopy(clientId:environment:fingerprint:sessionId:userAgent:userName:sequence:type:productId:referrer:url:eventDate:containers:version:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *clientId __attribute__((swift_name("clientId")));
@property (readonly) NSArray<SSDKItemContainer *> * _Nullable containers __attribute__((swift_name("containers")));
@property (readonly) NSString *environment __attribute__((swift_name("environment")));
@property (readonly) SSDKTrackEventDate *eventDate __attribute__((swift_name("eventDate")));
@property (readonly) NSString *fingerprint __attribute__((swift_name("fingerprint")));
@property (readonly) NSString * _Nullable productId __attribute__((swift_name("productId")));
@property (readonly) NSString * _Nullable referrer __attribute__((swift_name("referrer")));
@property (readonly) int32_t sequence __attribute__((swift_name("sequence")));
@property (readonly) NSString *sessionId __attribute__((swift_name("sessionId")));
@property (readonly) SSDKTrackerEventType *type __attribute__((swift_name("type")));
@property (readonly) NSString *url __attribute__((swift_name("url")));
@property (readonly) NSString *userAgent __attribute__((swift_name("userAgent")));
@property (readonly) NSString * _Nullable userName __attribute__((swift_name("userName")));
@property (readonly) NSString *version __attribute__((swift_name("version")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ContainerShownEvent.Companion")))
@interface SSDKContainerShownEventCompanion : SSDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SSDKContainerShownEventCompanion *shared __attribute__((swift_name("shared")));
- (id<SSDKKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ItemClickEvent")))
@interface SSDKItemClickEvent : SSDKBase
- (instancetype)initWithClientId:(NSString *)clientId environment:(NSString *)environment fingerprint:(NSString *)fingerprint sessionId:(NSString *)sessionId userAgent:(NSString *)userAgent userName:(NSString * _Nullable)userName sequence:(int32_t)sequence productId:(NSString *)productId recId:(NSString *)recId type:(SSDKTrackerEventType *)type referrer:(NSString * _Nullable)referrer url:(NSString *)url eventDate:(SSDKTrackEventDate *)eventDate version:(NSString *)version __attribute__((swift_name("init(clientId:environment:fingerprint:sessionId:userAgent:userName:sequence:productId:recId:type:referrer:url:eventDate:version:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) SSDKItemClickEventCompanion *companion __attribute__((swift_name("companion")));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (SSDKTrackerEventType *)component10 __attribute__((swift_name("component10()")));
- (NSString * _Nullable)component11 __attribute__((swift_name("component11()")));
- (NSString *)component12 __attribute__((swift_name("component12()")));
- (SSDKTrackEventDate *)component13 __attribute__((swift_name("component13()")));
- (NSString *)component14 __attribute__((swift_name("component14()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (NSString *)component3 __attribute__((swift_name("component3()")));
- (NSString *)component4 __attribute__((swift_name("component4()")));
- (NSString *)component5 __attribute__((swift_name("component5()")));
- (NSString * _Nullable)component6 __attribute__((swift_name("component6()")));
- (int32_t)component7 __attribute__((swift_name("component7()")));
- (NSString *)component8 __attribute__((swift_name("component8()")));
- (NSString *)component9 __attribute__((swift_name("component9()")));
- (SSDKItemClickEvent *)doCopyClientId:(NSString *)clientId environment:(NSString *)environment fingerprint:(NSString *)fingerprint sessionId:(NSString *)sessionId userAgent:(NSString *)userAgent userName:(NSString * _Nullable)userName sequence:(int32_t)sequence productId:(NSString *)productId recId:(NSString *)recId type:(SSDKTrackerEventType *)type referrer:(NSString * _Nullable)referrer url:(NSString *)url eventDate:(SSDKTrackEventDate *)eventDate version:(NSString *)version __attribute__((swift_name("doCopy(clientId:environment:fingerprint:sessionId:userAgent:userName:sequence:productId:recId:type:referrer:url:eventDate:version:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *clientId __attribute__((swift_name("clientId")));
@property (readonly) NSString *environment __attribute__((swift_name("environment")));
@property (readonly) SSDKTrackEventDate *eventDate __attribute__((swift_name("eventDate")));
@property (readonly) NSString *fingerprint __attribute__((swift_name("fingerprint")));
@property (readonly) NSString *productId __attribute__((swift_name("productId")));
@property (readonly) NSString *recId __attribute__((swift_name("recId")));
@property (readonly) NSString * _Nullable referrer __attribute__((swift_name("referrer")));
@property (readonly) int32_t sequence __attribute__((swift_name("sequence")));
@property (readonly) NSString *sessionId __attribute__((swift_name("sessionId")));
@property (readonly) SSDKTrackerEventType *type __attribute__((swift_name("type")));
@property (readonly) NSString *url __attribute__((swift_name("url")));
@property (readonly) NSString *userAgent __attribute__((swift_name("userAgent")));
@property (readonly) NSString * _Nullable userName __attribute__((swift_name("userName")));
@property (readonly) NSString *version __attribute__((swift_name("version")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ItemClickEvent.Companion")))
@interface SSDKItemClickEventCompanion : SSDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SSDKItemClickEventCompanion *shared __attribute__((swift_name("shared")));
- (id<SSDKKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ItemContainer")))
@interface SSDKItemContainer : SSDKBase
- (instancetype)initWithRecItems:(NSArray<NSDictionary<NSString *, NSString *> *> *)recItems resultId:(NSString *)resultId __attribute__((swift_name("init(recItems:resultId:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) SSDKItemContainerCompanion *companion __attribute__((swift_name("companion")));
- (NSArray<NSDictionary<NSString *, NSString *> *> *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (SSDKItemContainer *)doCopyRecItems:(NSArray<NSDictionary<NSString *, NSString *> *> *)recItems resultId:(NSString *)resultId __attribute__((swift_name("doCopy(recItems:resultId:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSArray<NSDictionary<NSString *, NSString *> *> *recItems __attribute__((swift_name("recItems")));
@property (readonly) NSString *resultId __attribute__((swift_name("resultId")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ItemContainer.Companion")))
@interface SSDKItemContainerCompanion : SSDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SSDKItemContainerCompanion *shared __attribute__((swift_name("shared")));
- (id<SSDKKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("PageViewEvent")))
@interface SSDKPageViewEvent : SSDKBase
- (instancetype)initWithClientId:(NSString *)clientId environment:(NSString *)environment fingerprint:(NSString *)fingerprint sessionId:(NSString *)sessionId userAgent:(NSString *)userAgent userName:(NSString * _Nullable)userName sequence:(int32_t)sequence productId:(NSString * _Nullable)productId recId:(NSString *)recId type:(SSDKTrackerEventType *)type referrer:(NSString * _Nullable)referrer url:(NSString *)url eventDate:(SSDKTrackEventDate *)eventDate version:(NSString *)version __attribute__((swift_name("init(clientId:environment:fingerprint:sessionId:userAgent:userName:sequence:productId:recId:type:referrer:url:eventDate:version:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) SSDKPageViewEventCompanion *companion __attribute__((swift_name("companion")));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (SSDKTrackerEventType *)component10 __attribute__((swift_name("component10()")));
- (NSString * _Nullable)component11 __attribute__((swift_name("component11()")));
- (NSString *)component12 __attribute__((swift_name("component12()")));
- (SSDKTrackEventDate *)component13 __attribute__((swift_name("component13()")));
- (NSString *)component14 __attribute__((swift_name("component14()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (NSString *)component3 __attribute__((swift_name("component3()")));
- (NSString *)component4 __attribute__((swift_name("component4()")));
- (NSString *)component5 __attribute__((swift_name("component5()")));
- (NSString * _Nullable)component6 __attribute__((swift_name("component6()")));
- (int32_t)component7 __attribute__((swift_name("component7()")));
- (NSString * _Nullable)component8 __attribute__((swift_name("component8()")));
- (NSString *)component9 __attribute__((swift_name("component9()")));
- (SSDKPageViewEvent *)doCopyClientId:(NSString *)clientId environment:(NSString *)environment fingerprint:(NSString *)fingerprint sessionId:(NSString *)sessionId userAgent:(NSString *)userAgent userName:(NSString * _Nullable)userName sequence:(int32_t)sequence productId:(NSString * _Nullable)productId recId:(NSString *)recId type:(SSDKTrackerEventType *)type referrer:(NSString * _Nullable)referrer url:(NSString *)url eventDate:(SSDKTrackEventDate *)eventDate version:(NSString *)version __attribute__((swift_name("doCopy(clientId:environment:fingerprint:sessionId:userAgent:userName:sequence:productId:recId:type:referrer:url:eventDate:version:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *clientId __attribute__((swift_name("clientId")));
@property (readonly) NSString *environment __attribute__((swift_name("environment")));
@property (readonly) SSDKTrackEventDate *eventDate __attribute__((swift_name("eventDate")));
@property (readonly) NSString *fingerprint __attribute__((swift_name("fingerprint")));
@property (readonly) NSString * _Nullable productId __attribute__((swift_name("productId")));
@property (readonly) NSString *recId __attribute__((swift_name("recId")));
@property (readonly) NSString * _Nullable referrer __attribute__((swift_name("referrer")));
@property (readonly) int32_t sequence __attribute__((swift_name("sequence")));
@property (readonly) NSString *sessionId __attribute__((swift_name("sessionId")));
@property (readonly) SSDKTrackerEventType *type __attribute__((swift_name("type")));
@property (readonly) NSString *url __attribute__((swift_name("url")));
@property (readonly) NSString *userAgent __attribute__((swift_name("userAgent")));
@property (readonly) NSString * _Nullable userName __attribute__((swift_name("userName")));
@property (readonly) NSString *version __attribute__((swift_name("version")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("PageViewEvent.Companion")))
@interface SSDKPageViewEventCompanion : SSDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SSDKPageViewEventCompanion *shared __attribute__((swift_name("shared")));
- (id<SSDKKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Category")))
@interface SSDKCategory : SSDKBase
- (instancetype)initWithNames:(NSArray<NSString *> *)names link:(NSString *)link image:(NSString *)image __attribute__((swift_name("init(names:link:image:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) SSDKCategoryCompanion *companion __attribute__((swift_name("companion")));
- (NSArray<NSString *> *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (NSString *)component3 __attribute__((swift_name("component3()")));
- (SSDKCategory *)doCopyNames:(NSArray<NSString *> *)names link:(NSString *)link image:(NSString *)image __attribute__((swift_name("doCopy(names:link:image:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *image __attribute__((swift_name("image")));
@property (readonly) NSString *link __attribute__((swift_name("link")));
@property (readonly) NSArray<NSString *> *names __attribute__((swift_name("names")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Category.Companion")))
@interface SSDKCategoryCompanion : SSDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SSDKCategoryCompanion *shared __attribute__((swift_name("shared")));
- (id<SSDKKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Product")))
@interface SSDKProduct : SSDKBase
- (instancetype)initWithId:(NSString *)id brand:(NSString * _Nullable)brand currency:(NSString * _Nullable)currency description:(NSString * _Nullable)description image:(NSString *)image images:(NSArray<NSString *> * _Nullable)images inStock:(SSDKBoolean * _Nullable)inStock itemGroupId:(NSString * _Nullable)itemGroupId link:(NSString *)link price:(SSDKDouble * _Nullable)price promotion:(NSString * _Nullable)promotion salePrice:(SSDKFloat * _Nullable)salePrice title:(NSString *)title recId:(NSString *)recId tags:(NSArray<NSString *> * _Nullable)tags attributes:(NSDictionary<NSString *, SSDKKotlinx_serialization_jsonJsonElement *> * _Nullable)attributes __attribute__((swift_name("init(id:brand:currency:description:image:images:inStock:itemGroupId:link:price:promotion:salePrice:title:recId:tags:attributes:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) SSDKProductCompanion *companion __attribute__((swift_name("companion")));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (SSDKDouble * _Nullable)component10 __attribute__((swift_name("component10()")));
- (NSString * _Nullable)component11 __attribute__((swift_name("component11()")));
- (SSDKFloat * _Nullable)component12 __attribute__((swift_name("component12()")));
- (NSString *)component13 __attribute__((swift_name("component13()")));
- (NSString *)component14 __attribute__((swift_name("component14()")));
- (NSArray<NSString *> * _Nullable)component15 __attribute__((swift_name("component15()")));
- (NSDictionary<NSString *, SSDKKotlinx_serialization_jsonJsonElement *> * _Nullable)component16 __attribute__((swift_name("component16()")));
- (NSString * _Nullable)component2 __attribute__((swift_name("component2()")));
- (NSString * _Nullable)component3 __attribute__((swift_name("component3()")));
- (NSString * _Nullable)component4 __attribute__((swift_name("component4()")));
- (NSString *)component5 __attribute__((swift_name("component5()")));
- (NSArray<NSString *> * _Nullable)component6 __attribute__((swift_name("component6()")));
- (SSDKBoolean * _Nullable)component7 __attribute__((swift_name("component7()")));
- (NSString * _Nullable)component8 __attribute__((swift_name("component8()")));
- (NSString *)component9 __attribute__((swift_name("component9()")));
- (SSDKProduct *)doCopyId:(NSString *)id brand:(NSString * _Nullable)brand currency:(NSString * _Nullable)currency description:(NSString * _Nullable)description image:(NSString *)image images:(NSArray<NSString *> * _Nullable)images inStock:(SSDKBoolean * _Nullable)inStock itemGroupId:(NSString * _Nullable)itemGroupId link:(NSString *)link price:(SSDKDouble * _Nullable)price promotion:(NSString * _Nullable)promotion salePrice:(SSDKFloat * _Nullable)salePrice title:(NSString *)title recId:(NSString *)recId tags:(NSArray<NSString *> * _Nullable)tags attributes:(NSDictionary<NSString *, SSDKKotlinx_serialization_jsonJsonElement *> * _Nullable)attributes __attribute__((swift_name("doCopy(id:brand:currency:description:image:images:inStock:itemGroupId:link:price:promotion:salePrice:title:recId:tags:attributes:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSDictionary<NSString *, SSDKKotlinx_serialization_jsonJsonElement *> * _Nullable attributes __attribute__((swift_name("attributes")));
@property (readonly) NSString * _Nullable brand __attribute__((swift_name("brand")));
@property (readonly) NSString * _Nullable currency __attribute__((swift_name("currency")));
@property (readonly) NSString * _Nullable description_ __attribute__((swift_name("description_")));
@property (readonly) NSString *id __attribute__((swift_name("id")));
@property (readonly) NSString *image __attribute__((swift_name("image")));
@property (readonly) NSArray<NSString *> * _Nullable images __attribute__((swift_name("images")));
@property (readonly) SSDKBoolean * _Nullable inStock __attribute__((swift_name("inStock")));
@property (readonly) NSString * _Nullable itemGroupId __attribute__((swift_name("itemGroupId")));
@property (readonly) NSString *link __attribute__((swift_name("link")));
@property (readonly) SSDKDouble * _Nullable price __attribute__((swift_name("price")));
@property (readonly) NSString * _Nullable promotion __attribute__((swift_name("promotion")));
@property (readonly) NSString *recId __attribute__((swift_name("recId")));
@property (readonly) SSDKFloat * _Nullable salePrice __attribute__((swift_name("salePrice")));
@property (readonly) NSArray<NSString *> * _Nullable tags __attribute__((swift_name("tags")));
@property (readonly) NSString *title __attribute__((swift_name("title")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Product.Companion")))
@interface SSDKProductCompanion : SSDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SSDKProductCompanion *shared __attribute__((swift_name("shared")));
- (id<SSDKKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CartRecommendationRequest")))
@interface SSDKCartRecommendationRequest : SSDKBase
- (instancetype)initWithEnvironment:(NSString * _Nullable)environment user:(SSDKRequestUser * _Nullable)user id:(NSArray<NSString *> * _Nullable)id category:(NSArray<NSString *> * _Nullable)category size:(SSDKInt * _Nullable)size start:(SSDKInt * _Nullable)start filter:(SSDKFilter * _Nullable)filter exclusion:(SSDKFilter * _Nullable)exclusion paginationTimestamp:(SSDKLong * _Nullable)paginationTimestamp language:(NSString * _Nullable)language display:(NSArray<NSString *> * _Nullable)display displayVariants:(NSArray<NSString *> * _Nullable)displayVariants meta:(NSDictionary<NSString *, SSDKKotlinx_serialization_jsonJsonElement *> * _Nullable)meta __attribute__((swift_name("init(environment:user:id:category:size:start:filter:exclusion:paginationTimestamp:language:display:displayVariants:meta:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) SSDKCartRecommendationRequestCompanion *companion __attribute__((swift_name("companion")));
- (NSString * _Nullable)component1 __attribute__((swift_name("component1()")));
- (NSString * _Nullable)component10 __attribute__((swift_name("component10()")));
- (NSArray<NSString *> * _Nullable)component11 __attribute__((swift_name("component11()")));
- (NSArray<NSString *> * _Nullable)component12 __attribute__((swift_name("component12()")));
- (NSDictionary<NSString *, SSDKKotlinx_serialization_jsonJsonElement *> * _Nullable)component13 __attribute__((swift_name("component13()")));
- (SSDKRequestUser * _Nullable)component2 __attribute__((swift_name("component2()")));
- (NSArray<NSString *> * _Nullable)component3 __attribute__((swift_name("component3()")));
- (NSArray<NSString *> * _Nullable)component4 __attribute__((swift_name("component4()")));
- (SSDKInt * _Nullable)component5 __attribute__((swift_name("component5()")));
- (SSDKInt * _Nullable)component6 __attribute__((swift_name("component6()")));
- (SSDKFilter * _Nullable)component7 __attribute__((swift_name("component7()")));
- (SSDKFilter * _Nullable)component8 __attribute__((swift_name("component8()")));
- (SSDKLong * _Nullable)component9 __attribute__((swift_name("component9()")));
- (SSDKCartRecommendationRequest *)doCopyEnvironment:(NSString * _Nullable)environment user:(SSDKRequestUser * _Nullable)user id:(NSArray<NSString *> * _Nullable)id category:(NSArray<NSString *> * _Nullable)category size:(SSDKInt * _Nullable)size start:(SSDKInt * _Nullable)start filter:(SSDKFilter * _Nullable)filter exclusion:(SSDKFilter * _Nullable)exclusion paginationTimestamp:(SSDKLong * _Nullable)paginationTimestamp language:(NSString * _Nullable)language display:(NSArray<NSString *> * _Nullable)display displayVariants:(NSArray<NSString *> * _Nullable)displayVariants meta:(NSDictionary<NSString *, SSDKKotlinx_serialization_jsonJsonElement *> * _Nullable)meta __attribute__((swift_name("doCopy(environment:user:id:category:size:start:filter:exclusion:paginationTimestamp:language:display:displayVariants:meta:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSArray<NSString *> * _Nullable category __attribute__((swift_name("category")));
@property (readonly) NSArray<NSString *> * _Nullable display __attribute__((swift_name("display")));
@property (readonly) NSArray<NSString *> * _Nullable displayVariants __attribute__((swift_name("displayVariants")));
@property NSString * _Nullable environment __attribute__((swift_name("environment")));
@property (readonly) SSDKFilter * _Nullable exclusion __attribute__((swift_name("exclusion")));
@property (readonly) SSDKFilter * _Nullable filter __attribute__((swift_name("filter")));
@property (readonly) NSArray<NSString *> * _Nullable id __attribute__((swift_name("id")));
@property (readonly) NSString * _Nullable language __attribute__((swift_name("language")));
@property (readonly) NSDictionary<NSString *, SSDKKotlinx_serialization_jsonJsonElement *> * _Nullable meta __attribute__((swift_name("meta")));
@property (readonly) SSDKLong * _Nullable paginationTimestamp __attribute__((swift_name("paginationTimestamp")));
@property (readonly) SSDKInt * _Nullable size __attribute__((swift_name("size")));
@property (readonly) SSDKInt * _Nullable start __attribute__((swift_name("start")));
@property SSDKRequestUser * _Nullable user __attribute__((swift_name("user")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CartRecommendationRequest.Companion")))
@interface SSDKCartRecommendationRequestCompanion : SSDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SSDKCartRecommendationRequestCompanion *shared __attribute__((swift_name("shared")));
- (id<SSDKKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Filter")))
@interface SSDKFilter : SSDKBase
- (instancetype)initWithId:(NSArray<NSString *> * _Nullable)id text:(NSString * _Nullable)text brand:(NSString * _Nullable)brand category:(NSArray<NSString *> * _Nullable)category price:(NSDictionary<NSString *, NSArray<SSDKDouble *> *> * _Nullable)price salePrice:(NSDictionary<NSString *, NSArray<SSDKDouble *> *> * _Nullable)salePrice onSaleOnly:(SSDKBoolean * _Nullable)onSaleOnly inStockOnly:(SSDKBoolean * _Nullable)inStockOnly tags:(NSArray<NSString *> * _Nullable)tags meta:(NSDictionary<NSString *, NSString *> * _Nullable)meta __attribute__((swift_name("init(id:text:brand:category:price:salePrice:onSaleOnly:inStockOnly:tags:meta:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) SSDKFilterCompanion *companion __attribute__((swift_name("companion")));
- (NSArray<NSString *> * _Nullable)component1 __attribute__((swift_name("component1()")));
- (NSDictionary<NSString *, NSString *> * _Nullable)component10 __attribute__((swift_name("component10()")));
- (NSString * _Nullable)component2 __attribute__((swift_name("component2()")));
- (NSString * _Nullable)component3 __attribute__((swift_name("component3()")));
- (NSArray<NSString *> * _Nullable)component4 __attribute__((swift_name("component4()")));
- (NSDictionary<NSString *, NSArray<SSDKDouble *> *> * _Nullable)component5 __attribute__((swift_name("component5()")));
- (NSDictionary<NSString *, NSArray<SSDKDouble *> *> * _Nullable)component6 __attribute__((swift_name("component6()")));
- (SSDKBoolean * _Nullable)component7 __attribute__((swift_name("component7()")));
- (SSDKBoolean * _Nullable)component8 __attribute__((swift_name("component8()")));
- (NSArray<NSString *> * _Nullable)component9 __attribute__((swift_name("component9()")));
- (SSDKFilter *)doCopyId:(NSArray<NSString *> * _Nullable)id text:(NSString * _Nullable)text brand:(NSString * _Nullable)brand category:(NSArray<NSString *> * _Nullable)category price:(NSDictionary<NSString *, NSArray<SSDKDouble *> *> * _Nullable)price salePrice:(NSDictionary<NSString *, NSArray<SSDKDouble *> *> * _Nullable)salePrice onSaleOnly:(SSDKBoolean * _Nullable)onSaleOnly inStockOnly:(SSDKBoolean * _Nullable)inStockOnly tags:(NSArray<NSString *> * _Nullable)tags meta:(NSDictionary<NSString *, NSString *> * _Nullable)meta __attribute__((swift_name("doCopy(id:text:brand:category:price:salePrice:onSaleOnly:inStockOnly:tags:meta:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString * _Nullable brand __attribute__((swift_name("brand")));
@property (readonly) NSArray<NSString *> * _Nullable category __attribute__((swift_name("category")));
@property (readonly) NSArray<NSString *> * _Nullable id __attribute__((swift_name("id")));
@property (readonly) SSDKBoolean * _Nullable inStockOnly __attribute__((swift_name("inStockOnly")));
@property (readonly) NSDictionary<NSString *, NSString *> * _Nullable meta __attribute__((swift_name("meta")));
@property (readonly) SSDKBoolean * _Nullable onSaleOnly __attribute__((swift_name("onSaleOnly")));
@property (readonly) NSDictionary<NSString *, NSArray<SSDKDouble *> *> * _Nullable price __attribute__((swift_name("price")));
@property (readonly) NSDictionary<NSString *, NSArray<SSDKDouble *> *> * _Nullable salePrice __attribute__((swift_name("salePrice")));
@property (readonly) NSArray<NSString *> * _Nullable tags __attribute__((swift_name("tags")));
@property (readonly) NSString * _Nullable text __attribute__((swift_name("text")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Filter.Companion")))
@interface SSDKFilterCompanion : SSDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SSDKFilterCompanion *shared __attribute__((swift_name("shared")));
- (id<SSDKKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("QueryItem")))
@interface SSDKQueryItem : SSDKBase
- (instancetype)initWithId:(NSString *)id type:(NSString *)type __attribute__((swift_name("init(id:type:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) SSDKQueryItemCompanion *companion __attribute__((swift_name("companion")));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (SSDKQueryItem *)doCopyId:(NSString *)id type:(NSString *)type __attribute__((swift_name("doCopy(id:type:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *id __attribute__((swift_name("id")));
@property (readonly) NSString *type __attribute__((swift_name("type")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("QueryItem.Companion")))
@interface SSDKQueryItemCompanion : SSDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SSDKQueryItemCompanion *shared __attribute__((swift_name("shared")));
- (id<SSDKKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("RecommendationRequest")))
@interface SSDKRecommendationRequest : SSDKBase
- (instancetype)initWithEnvironment:(NSString * _Nullable)environment user:(SSDKRequestUser * _Nullable)user id:(NSString * _Nullable)id category:(NSArray<NSString *> * _Nullable)category size:(SSDKInt * _Nullable)size start:(SSDKInt * _Nullable)start filter:(SSDKFilter * _Nullable)filter exclusion:(SSDKFilter * _Nullable)exclusion paginationTimestamp:(SSDKLong * _Nullable)paginationTimestamp language:(NSString * _Nullable)language display:(NSArray<NSString *> * _Nullable)display displayVariants:(NSArray<NSString *> * _Nullable)displayVariants meta:(NSDictionary<NSString *, SSDKKotlinx_serialization_jsonJsonElement *> * _Nullable)meta __attribute__((swift_name("init(environment:user:id:category:size:start:filter:exclusion:paginationTimestamp:language:display:displayVariants:meta:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) SSDKRecommendationRequestCompanion *companion __attribute__((swift_name("companion")));
- (NSString * _Nullable)component1 __attribute__((swift_name("component1()")));
- (NSString * _Nullable)component10 __attribute__((swift_name("component10()")));
- (NSArray<NSString *> * _Nullable)component11 __attribute__((swift_name("component11()")));
- (NSArray<NSString *> * _Nullable)component12 __attribute__((swift_name("component12()")));
- (NSDictionary<NSString *, SSDKKotlinx_serialization_jsonJsonElement *> * _Nullable)component13 __attribute__((swift_name("component13()")));
- (SSDKRequestUser * _Nullable)component2 __attribute__((swift_name("component2()")));
- (NSString * _Nullable)component3 __attribute__((swift_name("component3()")));
- (NSArray<NSString *> * _Nullable)component4 __attribute__((swift_name("component4()")));
- (SSDKInt * _Nullable)component5 __attribute__((swift_name("component5()")));
- (SSDKInt * _Nullable)component6 __attribute__((swift_name("component6()")));
- (SSDKFilter * _Nullable)component7 __attribute__((swift_name("component7()")));
- (SSDKFilter * _Nullable)component8 __attribute__((swift_name("component8()")));
- (SSDKLong * _Nullable)component9 __attribute__((swift_name("component9()")));
- (SSDKRecommendationRequest *)doCopyEnvironment:(NSString * _Nullable)environment user:(SSDKRequestUser * _Nullable)user id:(NSString * _Nullable)id category:(NSArray<NSString *> * _Nullable)category size:(SSDKInt * _Nullable)size start:(SSDKInt * _Nullable)start filter:(SSDKFilter * _Nullable)filter exclusion:(SSDKFilter * _Nullable)exclusion paginationTimestamp:(SSDKLong * _Nullable)paginationTimestamp language:(NSString * _Nullable)language display:(NSArray<NSString *> * _Nullable)display displayVariants:(NSArray<NSString *> * _Nullable)displayVariants meta:(NSDictionary<NSString *, SSDKKotlinx_serialization_jsonJsonElement *> * _Nullable)meta __attribute__((swift_name("doCopy(environment:user:id:category:size:start:filter:exclusion:paginationTimestamp:language:display:displayVariants:meta:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSArray<NSString *> * _Nullable category __attribute__((swift_name("category")));
@property (readonly) NSArray<NSString *> * _Nullable display __attribute__((swift_name("display")));
@property (readonly) NSArray<NSString *> * _Nullable displayVariants __attribute__((swift_name("displayVariants")));
@property NSString * _Nullable environment __attribute__((swift_name("environment")));
@property (readonly) SSDKFilter * _Nullable exclusion __attribute__((swift_name("exclusion")));
@property (readonly) SSDKFilter * _Nullable filter __attribute__((swift_name("filter")));
@property (readonly) NSString * _Nullable id __attribute__((swift_name("id")));
@property (readonly) NSString * _Nullable language __attribute__((swift_name("language")));
@property (readonly) NSDictionary<NSString *, SSDKKotlinx_serialization_jsonJsonElement *> * _Nullable meta __attribute__((swift_name("meta")));
@property (readonly) SSDKLong * _Nullable paginationTimestamp __attribute__((swift_name("paginationTimestamp")));
@property (readonly) SSDKInt * _Nullable size __attribute__((swift_name("size")));
@property (readonly) SSDKInt * _Nullable start __attribute__((swift_name("start")));
@property SSDKRequestUser * _Nullable user __attribute__((swift_name("user")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("RecommendationRequest.Companion")))
@interface SSDKRecommendationRequestCompanion : SSDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SSDKRecommendationRequestCompanion *shared __attribute__((swift_name("shared")));
- (id<SSDKKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("RequestUser")))
@interface SSDKRequestUser : SSDKBase
- (instancetype)initWithFid:(NSString * _Nullable)fid sid:(NSString *)sid uid:(NSString * _Nullable)uid __attribute__((swift_name("init(fid:sid:uid:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) SSDKRequestUserCompanion *companion __attribute__((swift_name("companion")));
- (NSString * _Nullable)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (NSString * _Nullable)component3 __attribute__((swift_name("component3()")));
- (SSDKRequestUser *)doCopyFid:(NSString * _Nullable)fid sid:(NSString *)sid uid:(NSString * _Nullable)uid __attribute__((swift_name("doCopy(fid:sid:uid:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString * _Nullable fid __attribute__((swift_name("fid")));
@property (readonly) NSString *sid __attribute__((swift_name("sid")));
@property (readonly) NSString * _Nullable uid __attribute__((swift_name("uid")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("RequestUser.Companion")))
@interface SSDKRequestUserCompanion : SSDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SSDKRequestUserCompanion *shared __attribute__((swift_name("shared")));
- (id<SSDKKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SmartSearchRequest")))
@interface SSDKSmartSearchRequest : SSDKBase
- (instancetype)initWithEnvironment:(NSString * _Nullable)environment user:(SSDKRequestUser * _Nullable)user query:(NSArray<SSDKQueryItem *> * _Nullable)query filter:(SSDKFilter * _Nullable)filter exclusion:(SSDKFilter * _Nullable)exclusion size:(SSDKInt * _Nullable)size language:(NSString * _Nullable)language start:(SSDKInt * _Nullable)start facetSize:(SSDKInt * _Nullable)facetSize facets:(NSArray<NSString *> * _Nullable)facets score:(NSString * _Nullable)score meta:(NSDictionary<NSString *, SSDKKotlinx_serialization_jsonJsonElement *> * _Nullable)meta __attribute__((swift_name("init(environment:user:query:filter:exclusion:size:language:start:facetSize:facets:score:meta:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) SSDKSmartSearchRequestCompanion *companion __attribute__((swift_name("companion")));
- (NSString * _Nullable)component1 __attribute__((swift_name("component1()")));
- (NSArray<NSString *> * _Nullable)component10 __attribute__((swift_name("component10()")));
- (NSString * _Nullable)component11 __attribute__((swift_name("component11()")));
- (NSDictionary<NSString *, SSDKKotlinx_serialization_jsonJsonElement *> * _Nullable)component12 __attribute__((swift_name("component12()")));
- (SSDKRequestUser * _Nullable)component2 __attribute__((swift_name("component2()")));
- (NSArray<SSDKQueryItem *> * _Nullable)component3 __attribute__((swift_name("component3()")));
- (SSDKFilter * _Nullable)component4 __attribute__((swift_name("component4()")));
- (SSDKFilter * _Nullable)component5 __attribute__((swift_name("component5()")));
- (SSDKInt * _Nullable)component6 __attribute__((swift_name("component6()")));
- (NSString * _Nullable)component7 __attribute__((swift_name("component7()")));
- (SSDKInt * _Nullable)component8 __attribute__((swift_name("component8()")));
- (SSDKInt * _Nullable)component9 __attribute__((swift_name("component9()")));
- (SSDKSmartSearchRequest *)doCopyEnvironment:(NSString * _Nullable)environment user:(SSDKRequestUser * _Nullable)user query:(NSArray<SSDKQueryItem *> * _Nullable)query filter:(SSDKFilter * _Nullable)filter exclusion:(SSDKFilter * _Nullable)exclusion size:(SSDKInt * _Nullable)size language:(NSString * _Nullable)language start:(SSDKInt * _Nullable)start facetSize:(SSDKInt * _Nullable)facetSize facets:(NSArray<NSString *> * _Nullable)facets score:(NSString * _Nullable)score meta:(NSDictionary<NSString *, SSDKKotlinx_serialization_jsonJsonElement *> * _Nullable)meta __attribute__((swift_name("doCopy(environment:user:query:filter:exclusion:size:language:start:facetSize:facets:score:meta:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property NSString * _Nullable environment __attribute__((swift_name("environment")));
@property (readonly) SSDKFilter * _Nullable exclusion __attribute__((swift_name("exclusion")));
@property (readonly) SSDKInt * _Nullable facetSize __attribute__((swift_name("facetSize")));
@property (readonly) NSArray<NSString *> * _Nullable facets __attribute__((swift_name("facets")));
@property (readonly) SSDKFilter * _Nullable filter __attribute__((swift_name("filter")));
@property (readonly) NSString * _Nullable language __attribute__((swift_name("language")));
@property (readonly) NSDictionary<NSString *, SSDKKotlinx_serialization_jsonJsonElement *> * _Nullable meta __attribute__((swift_name("meta")));
@property (readonly) NSArray<SSDKQueryItem *> * _Nullable query __attribute__((swift_name("query")));
@property (readonly) NSString * _Nullable score __attribute__((swift_name("score")));
@property (readonly) SSDKInt * _Nullable size __attribute__((swift_name("size")));
@property (readonly) SSDKInt * _Nullable start __attribute__((swift_name("start")));
@property SSDKRequestUser * _Nullable user __attribute__((swift_name("user")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SmartSearchRequest.Companion")))
@interface SSDKSmartSearchRequestCompanion : SSDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SSDKSmartSearchRequestCompanion *shared __attribute__((swift_name("shared")));
- (id<SSDKKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SmartSuggestRequest")))
@interface SSDKSmartSuggestRequest : SSDKBase
- (instancetype)initWithEnvironment:(NSString * _Nullable)environment user:(SSDKRequestUser * _Nullable)user query:(NSString *)query meta:(NSDictionary<NSString *, SSDKKotlinx_serialization_jsonJsonElement *> * _Nullable)meta __attribute__((swift_name("init(environment:user:query:meta:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) SSDKSmartSuggestRequestCompanion *companion __attribute__((swift_name("companion")));
- (NSString * _Nullable)component1 __attribute__((swift_name("component1()")));
- (SSDKRequestUser * _Nullable)component2 __attribute__((swift_name("component2()")));
- (NSString *)component3 __attribute__((swift_name("component3()")));
- (NSDictionary<NSString *, SSDKKotlinx_serialization_jsonJsonElement *> * _Nullable)component4 __attribute__((swift_name("component4()")));
- (SSDKSmartSuggestRequest *)doCopyEnvironment:(NSString * _Nullable)environment user:(SSDKRequestUser * _Nullable)user query:(NSString *)query meta:(NSDictionary<NSString *, SSDKKotlinx_serialization_jsonJsonElement *> * _Nullable)meta __attribute__((swift_name("doCopy(environment:user:query:meta:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property NSString * _Nullable environment __attribute__((swift_name("environment")));
@property (readonly) NSDictionary<NSString *, SSDKKotlinx_serialization_jsonJsonElement *> * _Nullable meta __attribute__((swift_name("meta")));
@property (readonly) NSString *query __attribute__((swift_name("query")));
@property SSDKRequestUser * _Nullable user __attribute__((swift_name("user")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SmartSuggestRequest.Companion")))
@interface SSDKSmartSuggestRequestCompanion : SSDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SSDKSmartSuggestRequestCompanion *shared __attribute__((swift_name("shared")));
- (id<SSDKKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("RecommendationResponse")))
@interface SSDKRecommendationResponse : SSDKBase
- (instancetype)initWithResults:(NSArray<SSDKProduct *> *)results size:(int32_t)size total:(SSDKInt * _Nullable)total paginationTimestamp:(int64_t)paginationTimestamp time:(NSString *)time resultId:(NSString *)resultId __attribute__((swift_name("init(results:size:total:paginationTimestamp:time:resultId:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) SSDKRecommendationResponseCompanion *companion __attribute__((swift_name("companion")));
- (NSArray<SSDKProduct *> *)component1 __attribute__((swift_name("component1()")));
- (int32_t)component2 __attribute__((swift_name("component2()")));
- (SSDKInt * _Nullable)component3 __attribute__((swift_name("component3()")));
- (int64_t)component4 __attribute__((swift_name("component4()")));
- (NSString *)component5 __attribute__((swift_name("component5()")));
- (NSString *)component6 __attribute__((swift_name("component6()")));
- (SSDKRecommendationResponse *)doCopyResults:(NSArray<SSDKProduct *> *)results size:(int32_t)size total:(SSDKInt * _Nullable)total paginationTimestamp:(int64_t)paginationTimestamp time:(NSString *)time resultId:(NSString *)resultId __attribute__((swift_name("doCopy(results:size:total:paginationTimestamp:time:resultId:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int64_t paginationTimestamp __attribute__((swift_name("paginationTimestamp")));
@property (readonly) NSString *resultId __attribute__((swift_name("resultId")));
@property (readonly) NSArray<SSDKProduct *> *results __attribute__((swift_name("results")));
@property (readonly) int32_t size __attribute__((swift_name("size")));
@property (readonly) NSString *time __attribute__((swift_name("time")));
@property (readonly) SSDKInt * _Nullable total __attribute__((swift_name("total")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("RecommendationResponse.Companion")))
@interface SSDKRecommendationResponseCompanion : SSDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SSDKRecommendationResponseCompanion *shared __attribute__((swift_name("shared")));
- (id<SSDKKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ResponseDefault")))
@interface SSDKResponseDefault<T> : SSDKBase
- (instancetype)initWithResult:(T _Nullable)result __attribute__((swift_name("init(result:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) SSDKResponseDefaultCompanion *companion __attribute__((swift_name("companion")));
- (T _Nullable)component1 __attribute__((swift_name("component1()")));
- (SSDKResponseDefault<T> *)doCopyResult:(T _Nullable)result __attribute__((swift_name("doCopy(result:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) T _Nullable result __attribute__((swift_name("result")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ResponseDefaultCompanion")))
@interface SSDKResponseDefaultCompanion : SSDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SSDKResponseDefaultCompanion *shared __attribute__((swift_name("shared")));
- (id<SSDKKotlinx_serialization_coreKSerializer>)serializerTypeParamsSerializers:(SSDKKotlinArray<id<SSDKKotlinx_serialization_coreKSerializer>> *)typeParamsSerializers __attribute__((swift_name("serializer(typeParamsSerializers:)")));
- (id<SSDKKotlinx_serialization_coreKSerializer>)serializerTypeSerial0:(id<SSDKKotlinx_serialization_coreKSerializer>)typeSerial0 __attribute__((swift_name("serializer(typeSerial0:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SmartSearchResponse")))
@interface SSDKSmartSearchResponse : SSDKBase
- (instancetype)initWithResults:(NSArray<SSDKProduct *> *)results size:(int32_t)size total:(int32_t)total facets:(NSDictionary<NSString *, SSDKKotlinx_serialization_jsonJsonElement *> * _Nullable)facets time:(NSString *)time paginationTimestamp:(int64_t)paginationTimestamp resultId:(NSString *)resultId __attribute__((swift_name("init(results:size:total:facets:time:paginationTimestamp:resultId:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) SSDKSmartSearchResponseCompanion *companion __attribute__((swift_name("companion")));
- (NSArray<SSDKProduct *> *)component1 __attribute__((swift_name("component1()")));
- (int32_t)component2 __attribute__((swift_name("component2()")));
- (int32_t)component3 __attribute__((swift_name("component3()")));
- (NSDictionary<NSString *, SSDKKotlinx_serialization_jsonJsonElement *> * _Nullable)component4 __attribute__((swift_name("component4()")));
- (NSString *)component5 __attribute__((swift_name("component5()")));
- (int64_t)component6 __attribute__((swift_name("component6()")));
- (NSString *)component7 __attribute__((swift_name("component7()")));
- (SSDKSmartSearchResponse *)doCopyResults:(NSArray<SSDKProduct *> *)results size:(int32_t)size total:(int32_t)total facets:(NSDictionary<NSString *, SSDKKotlinx_serialization_jsonJsonElement *> * _Nullable)facets time:(NSString *)time paginationTimestamp:(int64_t)paginationTimestamp resultId:(NSString *)resultId __attribute__((swift_name("doCopy(results:size:total:facets:time:paginationTimestamp:resultId:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSDictionary<NSString *, SSDKKotlinx_serialization_jsonJsonElement *> * _Nullable facets __attribute__((swift_name("facets")));
@property (readonly) int64_t paginationTimestamp __attribute__((swift_name("paginationTimestamp")));
@property (readonly) NSString *resultId __attribute__((swift_name("resultId")));
@property (readonly) NSArray<SSDKProduct *> *results __attribute__((swift_name("results")));
@property (readonly) int32_t size __attribute__((swift_name("size")));
@property (readonly) NSString *time __attribute__((swift_name("time")));
@property (readonly) int32_t total __attribute__((swift_name("total")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SmartSearchResponse.Companion")))
@interface SSDKSmartSearchResponseCompanion : SSDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SSDKSmartSearchResponseCompanion *shared __attribute__((swift_name("shared")));
- (id<SSDKKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SmartSuggestResponse")))
@interface SSDKSmartSuggestResponse : SSDKBase
- (instancetype)initWithQuery:(NSString *)query results:(SSDKSmartSuggestResult *)results time:(NSString *)time resultId:(NSString *)resultId __attribute__((swift_name("init(query:results:time:resultId:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) SSDKSmartSuggestResponseCompanion *companion __attribute__((swift_name("companion")));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (SSDKSmartSuggestResult *)component2 __attribute__((swift_name("component2()")));
- (NSString *)component3 __attribute__((swift_name("component3()")));
- (NSString *)component4 __attribute__((swift_name("component4()")));
- (SSDKSmartSuggestResponse *)doCopyQuery:(NSString *)query results:(SSDKSmartSuggestResult *)results time:(NSString *)time resultId:(NSString *)resultId __attribute__((swift_name("doCopy(query:results:time:resultId:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *query __attribute__((swift_name("query")));
@property (readonly) NSString *resultId __attribute__((swift_name("resultId")));
@property (readonly) SSDKSmartSuggestResult *results __attribute__((swift_name("results")));
@property (readonly) NSString *time __attribute__((swift_name("time")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SmartSuggestResponse.Companion")))
@interface SSDKSmartSuggestResponseCompanion : SSDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SSDKSmartSuggestResponseCompanion *shared __attribute__((swift_name("shared")));
- (id<SSDKKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SmartSuggestResult")))
@interface SSDKSmartSuggestResult : SSDKBase
- (instancetype)initWithTerms:(NSArray<NSString *> *)terms didYouMean:(NSArray<NSString *> *)didYouMean categories:(NSArray<SSDKCategory *> *)categories products:(NSArray<SSDKProduct *> *)products __attribute__((swift_name("init(terms:didYouMean:categories:products:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) SSDKSmartSuggestResultCompanion *companion __attribute__((swift_name("companion")));
- (NSArray<NSString *> *)component1 __attribute__((swift_name("component1()")));
- (NSArray<NSString *> *)component2 __attribute__((swift_name("component2()")));
- (NSArray<SSDKCategory *> *)component3 __attribute__((swift_name("component3()")));
- (NSArray<SSDKProduct *> *)component4 __attribute__((swift_name("component4()")));
- (SSDKSmartSuggestResult *)doCopyTerms:(NSArray<NSString *> *)terms didYouMean:(NSArray<NSString *> *)didYouMean categories:(NSArray<SSDKCategory *> *)categories products:(NSArray<SSDKProduct *> *)products __attribute__((swift_name("doCopy(terms:didYouMean:categories:products:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSArray<SSDKCategory *> *categories __attribute__((swift_name("categories")));
@property (readonly) NSArray<NSString *> *didYouMean __attribute__((swift_name("didYouMean")));
@property (readonly) NSArray<SSDKProduct *> *products __attribute__((swift_name("products")));
@property (readonly) NSArray<NSString *> *terms __attribute__((swift_name("terms")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SmartSuggestResult.Companion")))
@interface SSDKSmartSuggestResultCompanion : SSDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SSDKSmartSuggestResultCompanion *shared __attribute__((swift_name("shared")));
- (id<SSDKKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ApiMaintenance")))
@interface SSDKApiMaintenance : SSDKBase
- (instancetype)initWithScheduled:(BOOL)scheduled message:(NSString * _Nullable)message startAt:(SSDKKotlinx_datetimeInstant * _Nullable)startAt endAt:(SSDKKotlinx_datetimeInstant * _Nullable)endAt __attribute__((swift_name("init(scheduled:message:startAt:endAt:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) SSDKApiMaintenanceCompanion *companion __attribute__((swift_name("companion")));
- (BOOL)component1 __attribute__((swift_name("component1()")));
- (NSString * _Nullable)component2 __attribute__((swift_name("component2()")));
- (SSDKKotlinx_datetimeInstant * _Nullable)component3 __attribute__((swift_name("component3()")));
- (SSDKKotlinx_datetimeInstant * _Nullable)component4 __attribute__((swift_name("component4()")));
- (SSDKApiMaintenance *)doCopyScheduled:(BOOL)scheduled message:(NSString * _Nullable)message startAt:(SSDKKotlinx_datetimeInstant * _Nullable)startAt endAt:(SSDKKotlinx_datetimeInstant * _Nullable)endAt __attribute__((swift_name("doCopy(scheduled:message:startAt:endAt:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) SSDKKotlinx_datetimeInstant * _Nullable endAt __attribute__((swift_name("endAt")));
@property (readonly) NSString * _Nullable message __attribute__((swift_name("message")));
@property (readonly) BOOL scheduled __attribute__((swift_name("scheduled")));
@property (readonly) SSDKKotlinx_datetimeInstant * _Nullable startAt __attribute__((swift_name("startAt")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ApiMaintenance.Companion")))
@interface SSDKApiMaintenanceCompanion : SSDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SSDKApiMaintenanceCompanion *shared __attribute__((swift_name("shared")));
- (id<SSDKKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DynamicConfig")))
@interface SSDKDynamicConfig : SSDKBase
- (instancetype)initWithEndPoints:(NSDictionary<NSString *, NSString *> * _Nullable)endPoints settings:(NSDictionary<NSString *, SSDKDynamicSettings *> *)settings maintenance:(SSDKApiMaintenance * _Nullable)maintenance sessionId:(NSString * _Nullable)sessionId expiresAt:(SSDKKotlinx_datetimeInstant * _Nullable)expiresAt createdAt:(SSDKKotlinx_datetimeInstant *)createdAt version:(NSString * _Nullable)version __attribute__((swift_name("init(endPoints:settings:maintenance:sessionId:expiresAt:createdAt:version:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) SSDKDynamicConfigCompanion *companion __attribute__((swift_name("companion")));
- (NSDictionary<NSString *, NSString *> * _Nullable)component1 __attribute__((swift_name("component1()")));
- (NSDictionary<NSString *, SSDKDynamicSettings *> *)component2 __attribute__((swift_name("component2()")));
- (SSDKApiMaintenance * _Nullable)component3 __attribute__((swift_name("component3()")));
- (NSString * _Nullable)component4 __attribute__((swift_name("component4()")));
- (SSDKKotlinx_datetimeInstant * _Nullable)component5 __attribute__((swift_name("component5()")));
- (SSDKKotlinx_datetimeInstant *)component6 __attribute__((swift_name("component6()")));
- (NSString * _Nullable)component7 __attribute__((swift_name("component7()")));
- (SSDKDynamicConfig *)doCopyEndPoints:(NSDictionary<NSString *, NSString *> * _Nullable)endPoints settings:(NSDictionary<NSString *, SSDKDynamicSettings *> *)settings maintenance:(SSDKApiMaintenance * _Nullable)maintenance sessionId:(NSString * _Nullable)sessionId expiresAt:(SSDKKotlinx_datetimeInstant * _Nullable)expiresAt createdAt:(SSDKKotlinx_datetimeInstant *)createdAt version:(NSString * _Nullable)version __attribute__((swift_name("doCopy(endPoints:settings:maintenance:sessionId:expiresAt:createdAt:version:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) SSDKKotlinx_datetimeInstant *createdAt __attribute__((swift_name("createdAt")));
@property (readonly) NSDictionary<NSString *, NSString *> * _Nullable endPoints __attribute__((swift_name("endPoints")));
@property (readonly) SSDKKotlinx_datetimeInstant * _Nullable expiresAt __attribute__((swift_name("expiresAt")));
@property (readonly) SSDKApiMaintenance * _Nullable maintenance __attribute__((swift_name("maintenance")));
@property (readonly) NSString * _Nullable sessionId __attribute__((swift_name("sessionId")));
@property (readonly) NSDictionary<NSString *, SSDKDynamicSettings *> *settings __attribute__((swift_name("settings")));
@property (readonly) NSString * _Nullable version __attribute__((swift_name("version")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DynamicConfig.Companion")))
@interface SSDKDynamicConfigCompanion : SSDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SSDKDynamicConfigCompanion *shared __attribute__((swift_name("shared")));
- (id<SSDKKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DynamicSettings")))
@interface SSDKDynamicSettings : SSDKBase
- (instancetype)initWithExtend:(NSString *)extend queryParams:(NSArray<SSDKKeyValue *> *)queryParams payload:(SSDKPayloadTemplate *)payload __attribute__((swift_name("init(extend:queryParams:payload:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) SSDKDynamicSettingsCompanion *companion __attribute__((swift_name("companion")));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSArray<SSDKKeyValue *> *)component2 __attribute__((swift_name("component2()")));
- (SSDKPayloadTemplate *)component3 __attribute__((swift_name("component3()")));
- (SSDKDynamicSettings *)doCopyExtend:(NSString *)extend queryParams:(NSArray<SSDKKeyValue *> *)queryParams payload:(SSDKPayloadTemplate *)payload __attribute__((swift_name("doCopy(extend:queryParams:payload:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *extend __attribute__((swift_name("extend")));
@property (readonly) SSDKPayloadTemplate *payload __attribute__((swift_name("payload")));
@property (readonly) NSArray<SSDKKeyValue *> *queryParams __attribute__((swift_name("queryParams")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DynamicSettings.Companion")))
@interface SSDKDynamicSettingsCompanion : SSDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SSDKDynamicSettingsCompanion *shared __attribute__((swift_name("shared")));
- (id<SSDKKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("PayloadTemplate")))
@interface SSDKPayloadTemplate : SSDKBase
- (instancetype)initWithQuery:(NSArray<NSString *> * _Nullable)query filter:(SSDKFilter * _Nullable)filter exclusion:(SSDKFilter * _Nullable)exclusion size:(SSDKInt * _Nullable)size language:(NSString * _Nullable)language start:(SSDKInt * _Nullable)start facetSize:(SSDKInt * _Nullable)facetSize facets:(NSArray<NSString *> * _Nullable)facets score:(NSString * _Nullable)score display:(NSArray<NSString *> * _Nullable)display displayVariants:(NSArray<NSString *> * _Nullable)displayVariants meta:(NSDictionary<NSString *, SSDKKotlinx_serialization_jsonJsonElement *> * _Nullable)meta __attribute__((swift_name("init(query:filter:exclusion:size:language:start:facetSize:facets:score:display:displayVariants:meta:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) SSDKPayloadTemplateCompanion *companion __attribute__((swift_name("companion")));
- (NSArray<NSString *> * _Nullable)component1 __attribute__((swift_name("component1()")));
- (NSArray<NSString *> * _Nullable)component10 __attribute__((swift_name("component10()")));
- (NSArray<NSString *> * _Nullable)component11 __attribute__((swift_name("component11()")));
- (NSDictionary<NSString *, SSDKKotlinx_serialization_jsonJsonElement *> * _Nullable)component12 __attribute__((swift_name("component12()")));
- (SSDKFilter * _Nullable)component2 __attribute__((swift_name("component2()")));
- (SSDKFilter * _Nullable)component3 __attribute__((swift_name("component3()")));
- (SSDKInt * _Nullable)component4 __attribute__((swift_name("component4()")));
- (NSString * _Nullable)component5 __attribute__((swift_name("component5()")));
- (SSDKInt * _Nullable)component6 __attribute__((swift_name("component6()")));
- (SSDKInt * _Nullable)component7 __attribute__((swift_name("component7()")));
- (NSArray<NSString *> * _Nullable)component8 __attribute__((swift_name("component8()")));
- (NSString * _Nullable)component9 __attribute__((swift_name("component9()")));
- (SSDKPayloadTemplate *)doCopyQuery:(NSArray<NSString *> * _Nullable)query filter:(SSDKFilter * _Nullable)filter exclusion:(SSDKFilter * _Nullable)exclusion size:(SSDKInt * _Nullable)size language:(NSString * _Nullable)language start:(SSDKInt * _Nullable)start facetSize:(SSDKInt * _Nullable)facetSize facets:(NSArray<NSString *> * _Nullable)facets score:(NSString * _Nullable)score display:(NSArray<NSString *> * _Nullable)display displayVariants:(NSArray<NSString *> * _Nullable)displayVariants meta:(NSDictionary<NSString *, SSDKKotlinx_serialization_jsonJsonElement *> * _Nullable)meta __attribute__((swift_name("doCopy(query:filter:exclusion:size:language:start:facetSize:facets:score:display:displayVariants:meta:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSArray<NSString *> * _Nullable display __attribute__((swift_name("display")));
@property (readonly) NSArray<NSString *> * _Nullable displayVariants __attribute__((swift_name("displayVariants")));
@property (readonly) SSDKFilter * _Nullable exclusion __attribute__((swift_name("exclusion")));
@property (readonly) SSDKInt * _Nullable facetSize __attribute__((swift_name("facetSize")));
@property (readonly) NSArray<NSString *> * _Nullable facets __attribute__((swift_name("facets")));
@property (readonly) SSDKFilter * _Nullable filter __attribute__((swift_name("filter")));
@property (readonly) NSString * _Nullable language __attribute__((swift_name("language")));
@property (readonly) NSDictionary<NSString *, SSDKKotlinx_serialization_jsonJsonElement *> * _Nullable meta __attribute__((swift_name("meta")));
@property (readonly) NSArray<NSString *> * _Nullable query __attribute__((swift_name("query")));
@property (readonly) NSString * _Nullable score __attribute__((swift_name("score")));
@property (readonly) SSDKInt * _Nullable size __attribute__((swift_name("size")));
@property (readonly) SSDKInt * _Nullable start __attribute__((swift_name("start")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("PayloadTemplate.Companion")))
@interface SSDKPayloadTemplateCompanion : SSDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SSDKPayloadTemplateCompanion *shared __attribute__((swift_name("shared")));
- (id<SSDKKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SystemaCache")))
@interface SSDKSystemaCache : SSDKBase
- (instancetype)initWithValues:(SSDKMutableDictionary<NSString *, NSString *> *)values __attribute__((swift_name("init(values:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) SSDKSystemaCacheCompanion *companion __attribute__((swift_name("companion")));
- (SSDKMutableDictionary<NSString *, NSString *> *)component1 __attribute__((swift_name("component1()")));
- (SSDKSystemaCache *)doCopyValues:(SSDKMutableDictionary<NSString *, NSString *> *)values __attribute__((swift_name("doCopy(values:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property SSDKMutableDictionary<NSString *, NSString *> *values __attribute__((swift_name("values")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SystemaCache.Companion")))
@interface SSDKSystemaCacheCompanion : SSDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SSDKSystemaCacheCompanion *shared __attribute__((swift_name("shared")));
- (id<SSDKKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((swift_name("SystemaKVStore")))
@protocol SSDKSystemaKVStore
@required

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)deleteKey:(NSString *)key completionHandler:(void (^)(NSString * _Nullable_result, NSError * _Nullable))completionHandler __attribute__((swift_name("delete(key:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)lazyWriteKey:(NSString *)key value:(NSString *)value completionHandler:(void (^)(SSDKKotlinUnit * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("lazyWrite(key:value:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)readKey:(NSString *)key completionHandler:(void (^)(NSString * _Nullable_result, NSError * _Nullable))completionHandler __attribute__((swift_name("read(key:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)writeKey:(NSString *)key value:(NSString *)value completionHandler:(void (^)(SSDKKotlinUnit * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("write(key:value:completionHandler:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SystemaAIClient")))
@interface SSDKSystemaAIClient : SSDKBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)initializeClientID:(NSString *)clientID apiKey:(NSString *)apiKey environment:(SSDKEnvironmentType *)environment logLevel:(SSDKSystemaLogLevel *)logLevel proxyUrls:(NSDictionary<SSDKEndpointType *, NSString *> *)proxyUrls meta:(NSDictionary<NSString *, id> *)meta completionHandler:(void (^)(id<SSDKSystemaAI> _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("initialize(clientID:apiKey:environment:logLevel:proxyUrls:meta:completionHandler:)")));
@end;

__attribute__((swift_name("SystemaIosStorage")))
@interface SSDKSystemaIosStorage : SSDKBase <SSDKSystemaKVStore>
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@property (class, readonly, getter=companion) SSDKSystemaIosStorageCompanion *companion __attribute__((swift_name("companion")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)deleteKey:(NSString *)key completionHandler:(void (^)(NSString * _Nullable_result, NSError * _Nullable))completionHandler __attribute__((swift_name("delete(key:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)lazyWriteKey:(NSString *)key value:(NSString *)value completionHandler:(void (^)(SSDKKotlinUnit * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("lazyWrite(key:value:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)readKey:(NSString *)key completionHandler:(void (^)(NSString * _Nullable_result, NSError * _Nullable))completionHandler __attribute__((swift_name("read(key:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)writeKey:(NSString *)key value:(NSString *)value completionHandler:(void (^)(SSDKKotlinUnit * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("write(key:value:completionHandler:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SystemaIosStorage.Companion")))
@interface SSDKSystemaIosStorageCompanion : SSDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SSDKSystemaIosStorageCompanion *shared __attribute__((swift_name("shared")));
@property (readonly) NSString * _Nullable TAG __attribute__((swift_name("TAG")));
@end;

__attribute__((swift_name("KotlinThrowable")))
@interface SSDKKotlinThrowable : SSDKBase
- (instancetype)initWithMessage:(NSString * _Nullable)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithCause:(SSDKKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(cause:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (instancetype)initWithMessage:(NSString * _Nullable)message cause:(SSDKKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(message:cause:)"))) __attribute__((objc_designated_initializer));
- (SSDKKotlinArray<NSString *> *)getStackTrace __attribute__((swift_name("getStackTrace()")));
- (void)printStackTrace __attribute__((swift_name("printStackTrace()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) SSDKKotlinThrowable * _Nullable cause __attribute__((swift_name("cause")));
@property (readonly) NSString * _Nullable message __attribute__((swift_name("message")));
- (NSError *)asError __attribute__((swift_name("asError()")));
@end;

__attribute__((swift_name("KotlinException")))
@interface SSDKKotlinException : SSDKKotlinThrowable
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (instancetype)initWithMessage:(NSString * _Nullable)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithMessage:(NSString * _Nullable)message cause:(SSDKKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(message:cause:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithCause:(SSDKKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(cause:)"))) __attribute__((objc_designated_initializer));
@end;

__attribute__((swift_name("KotlinRuntimeException")))
@interface SSDKKotlinRuntimeException : SSDKKotlinException
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (instancetype)initWithMessage:(NSString * _Nullable)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithMessage:(NSString * _Nullable)message cause:(SSDKKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(message:cause:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithCause:(SSDKKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(cause:)"))) __attribute__((objc_designated_initializer));
@end;

__attribute__((swift_name("SystemaRuntimeException")))
@interface SSDKSystemaRuntimeException : SSDKKotlinRuntimeException
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (instancetype)initWithMessage:(NSString * _Nullable)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (instancetype)initWithMessage:(NSString * _Nullable)message cause:(SSDKKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(message:cause:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (instancetype)initWithCause:(SSDKKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(cause:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DataNotFoundException")))
@interface SSDKDataNotFoundException : SSDKSystemaRuntimeException
- (instancetype)initWithEx:(SSDKKotlinThrowable *)ex __attribute__((swift_name("init(ex:)"))) __attribute__((objc_designated_initializer));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SystemaApiException")))
@interface SSDKSystemaApiException : SSDKSystemaRuntimeException
- (instancetype)initWithEx:(SSDKKotlinThrowable *)ex __attribute__((swift_name("init(ex:)"))) __attribute__((objc_designated_initializer));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SystemaListenerException")))
@interface SSDKSystemaListenerException : SSDKSystemaRuntimeException
- (instancetype)initWithEx:(SSDKKotlinThrowable *)ex __attribute__((swift_name("init(ex:)"))) __attribute__((objc_designated_initializer));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("RequestOptions")))
@interface SSDKRequestOptions : SSDKBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (void)headerKey:(NSString *)key value:(id _Nullable)value __attribute__((swift_name("header(key:value:)")));
- (void)parameterKey:(NSString *)key value:(id _Nullable)value __attribute__((swift_name("parameter(key:value:)")));
@property NSDictionary<NSString *, SSDKKotlinx_serialization_jsonJsonElement *> * _Nullable body __attribute__((swift_name("body")));
@property (readonly) SSDKMutableDictionary<NSString *, id> *headers __attribute__((swift_name("headers")));
@property (readonly) SSDKMutableDictionary<NSString *, id> *urlParameters __attribute__((swift_name("urlParameters")));
@end;

@interface SSDKSystemaDevice (Extensions)
- (NSString *)toUserAgentMaxLen:(int32_t)maxLen __attribute__((swift_name("toUserAgent(maxLen:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("BasicConstructorKt")))
@interface SSDKBasicConstructorKt : SSDKBase
+ (SSDKSystemaAPIKey *)toAPIKey:(NSString *)receiver __attribute__((swift_name("toAPIKey(_:)")));
+ (SSDKSystemaClientID *)toClientID:(NSString *)receiver __attribute__((swift_name("toClientID(_:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TrackEventDateKt")))
@interface SSDKTrackEventDateKt : SSDKBase
@property (class, readonly) SSDKTrackEventDate *CurTrackEventDate __attribute__((swift_name("CurTrackEventDate")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ItemContainerKt")))
@interface SSDKItemContainerKt : SSDKBase
+ (NSDictionary<NSString *, NSString *> *)prepContainerItemRecId:(NSString *)recId __attribute__((swift_name("prepContainerItem(recId:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ConfigurationKt")))
@interface SSDKConfigurationKt : SSDKBase
+ (id<SSDKConfiguration>)ConfigurationCredentials:(id<SSDKCredentials>)credentials logLevel:(SSDKSystemaLogLevel *)logLevel hosts:(NSDictionary<SSDKEndpointType *, SSDKKtor_httpUrl *> *)hosts engine:(id<SSDKKtor_client_coreHttpClientEngine> _Nullable)engine kvStore:(id<SSDKSystemaKVStore>)kvStore deviceManager:(id<SSDKSystemaDeviceManager>)deviceManager __attribute__((swift_name("Configuration(credentials:logLevel:hosts:engine:kvStore:deviceManager:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CredentialsKt")))
@interface SSDKCredentialsKt : SSDKBase
+ (id<SSDKCredentials>)CredentialsClientID:(SSDKSystemaClientID *)clientID apiKey:(SSDKSystemaAPIKey *)apiKey environment:(SSDKEnvironmentType *)environment proxyUrls:(NSDictionary<SSDKEndpointType *, SSDKKtor_httpUrl *> *)proxyUrls __attribute__((swift_name("Credentials(clientID:apiKey:environment:proxyUrls:)")));
@end;

__attribute__((swift_name("KotlinIllegalStateException")))
@interface SSDKKotlinIllegalStateException : SSDKKotlinRuntimeException
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (instancetype)initWithMessage:(NSString * _Nullable)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithMessage:(NSString * _Nullable)message cause:(SSDKKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(message:cause:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithCause:(SSDKKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(cause:)"))) __attribute__((objc_designated_initializer));
@end;

__attribute__((swift_name("KotlinCancellationException")))
@interface SSDKKotlinCancellationException : SSDKKotlinIllegalStateException
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (instancetype)initWithMessage:(NSString * _Nullable)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithMessage:(NSString * _Nullable)message cause:(SSDKKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(message:cause:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithCause:(SSDKKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(cause:)"))) __attribute__((objc_designated_initializer));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinUnit")))
@interface SSDKKotlinUnit : SSDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)unit __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SSDKKotlinUnit *shared __attribute__((swift_name("shared")));
- (NSString *)description __attribute__((swift_name("description()")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreCoroutineScope")))
@protocol SSDKKotlinx_coroutines_coreCoroutineScope
@required
@property (readonly) id<SSDKKotlinCoroutineContext> coroutineContext __attribute__((swift_name("coroutineContext")));
@end;

__attribute__((swift_name("Ktor_ioCloseable")))
@protocol SSDKKtor_ioCloseable
@required
- (void)close __attribute__((swift_name("close()")));
@end;

__attribute__((swift_name("Ktor_client_coreHttpClientEngine")))
@protocol SSDKKtor_client_coreHttpClientEngine <SSDKKotlinx_coroutines_coreCoroutineScope, SSDKKtor_ioCloseable>
@required

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)executeData:(SSDKKtor_client_coreHttpRequestData *)data completionHandler:(void (^)(SSDKKtor_client_coreHttpResponseData * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("execute(data:completionHandler:)")));
- (void)installClient:(SSDKKtor_client_coreHttpClient *)client __attribute__((swift_name("install(client:)")));
@property (readonly) SSDKKtor_client_coreHttpClientEngineConfig *config __attribute__((swift_name("config")));
@property (readonly) SSDKKotlinx_coroutines_coreCoroutineDispatcher *dispatcher __attribute__((swift_name("dispatcher")));
@property (readonly) NSSet<id<SSDKKtor_client_coreHttpClientEngineCapability>> *supportedCapabilities __attribute__((swift_name("supportedCapabilities")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_httpUrl")))
@interface SSDKKtor_httpUrl : SSDKBase
@property (class, readonly, getter=companion) SSDKKtor_httpUrlCompanion *companion __attribute__((swift_name("companion")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *encodedFragment __attribute__((swift_name("encodedFragment")));
@property (readonly) NSString * _Nullable encodedPassword __attribute__((swift_name("encodedPassword")));
@property (readonly) NSString *encodedPath __attribute__((swift_name("encodedPath")));
@property (readonly) NSString *encodedPathAndQuery __attribute__((swift_name("encodedPathAndQuery")));
@property (readonly) NSString *encodedQuery __attribute__((swift_name("encodedQuery")));
@property (readonly) NSString * _Nullable encodedUser __attribute__((swift_name("encodedUser")));
@property (readonly) NSString *fragment __attribute__((swift_name("fragment")));
@property (readonly) NSString *host __attribute__((swift_name("host")));
@property (readonly) id<SSDKKtor_httpParameters> parameters __attribute__((swift_name("parameters")));
@property (readonly) NSString * _Nullable password __attribute__((swift_name("password")));
@property (readonly) NSArray<NSString *> *pathSegments __attribute__((swift_name("pathSegments")));
@property (readonly) int32_t port __attribute__((swift_name("port")));
@property (readonly) SSDKKtor_httpURLProtocol *protocol __attribute__((swift_name("protocol")));
@property (readonly) int32_t specifiedPort __attribute__((swift_name("specifiedPort")));
@property (readonly) BOOL trailingQuery __attribute__((swift_name("trailingQuery")));
@property (readonly) NSString * _Nullable user __attribute__((swift_name("user")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_coreHttpClient")))
@interface SSDKKtor_client_coreHttpClient : SSDKBase <SSDKKotlinx_coroutines_coreCoroutineScope, SSDKKtor_ioCloseable>
- (instancetype)initWithEngine:(id<SSDKKtor_client_coreHttpClientEngine>)engine userConfig:(SSDKKtor_client_coreHttpClientConfig<SSDKKtor_client_coreHttpClientEngineConfig *> *)userConfig __attribute__((swift_name("init(engine:userConfig:)"))) __attribute__((objc_designated_initializer));
- (void)close __attribute__((swift_name("close()")));
- (SSDKKtor_client_coreHttpClient *)configBlock:(void (^)(SSDKKtor_client_coreHttpClientConfig<id> *))block __attribute__((swift_name("config(block:)")));
- (BOOL)isSupportedCapability:(id<SSDKKtor_client_coreHttpClientEngineCapability>)capability __attribute__((swift_name("isSupported(capability:)")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) id<SSDKKtor_utilsAttributes> attributes __attribute__((swift_name("attributes")));
@property (readonly) id<SSDKKotlinCoroutineContext> coroutineContext __attribute__((swift_name("coroutineContext")));
@property (readonly) id<SSDKKtor_client_coreHttpClientEngine> engine __attribute__((swift_name("engine")));
@property (readonly) SSDKKtor_client_coreHttpClientEngineConfig *engineConfig __attribute__((swift_name("engineConfig")));
@property (readonly) SSDKKtor_eventsEvents *monitor __attribute__((swift_name("monitor")));
@property (readonly) SSDKKtor_client_coreHttpReceivePipeline *receivePipeline __attribute__((swift_name("receivePipeline")));
@property (readonly) SSDKKtor_client_coreHttpRequestPipeline *requestPipeline __attribute__((swift_name("requestPipeline")));
@property (readonly) SSDKKtor_client_coreHttpResponsePipeline *responsePipeline __attribute__((swift_name("responsePipeline")));
@property (readonly) SSDKKtor_client_coreHttpSendPipeline *sendPipeline __attribute__((swift_name("sendPipeline")));
@end;

__attribute__((swift_name("Ktor_httpHttpMessage")))
@protocol SSDKKtor_httpHttpMessage
@required
@property (readonly) id<SSDKKtor_httpHeaders> headers __attribute__((swift_name("headers")));
@end;

__attribute__((swift_name("Ktor_client_coreHttpResponse")))
@interface SSDKKtor_client_coreHttpResponse : SSDKBase <SSDKKtor_httpHttpMessage, SSDKKotlinx_coroutines_coreCoroutineScope>
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) SSDKKtor_client_coreHttpClientCall *call __attribute__((swift_name("call")));
@property (readonly) id<SSDKKtor_ioByteReadChannel> content __attribute__((swift_name("content")));
@property (readonly) SSDKKtor_utilsGMTDate *requestTime __attribute__((swift_name("requestTime")));
@property (readonly) SSDKKtor_utilsGMTDate *responseTime __attribute__((swift_name("responseTime")));
@property (readonly) SSDKKtor_httpHttpStatusCode *status __attribute__((swift_name("status")));
@property (readonly) SSDKKtor_httpHttpProtocolVersion *version __attribute__((swift_name("version")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinEnumCompanion")))
@interface SSDKKotlinEnumCompanion : SSDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SSDKKotlinEnumCompanion *shared __attribute__((swift_name("shared")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinArray")))
@interface SSDKKotlinArray<T> : SSDKBase
+ (instancetype)arrayWithSize:(int32_t)size init:(T _Nullable (^)(SSDKInt *))init __attribute__((swift_name("init(size:init:)")));
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (T _Nullable)getIndex:(int32_t)index __attribute__((swift_name("get(index:)")));
- (id<SSDKKotlinIterator>)iterator __attribute__((swift_name("iterator()")));
- (void)setIndex:(int32_t)index value:(T _Nullable)value __attribute__((swift_name("set(index:value:)")));
@property (readonly) int32_t size __attribute__((swift_name("size")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Kotlinx_datetimeInstant")))
@interface SSDKKotlinx_datetimeInstant : SSDKBase <SSDKKotlinComparable>
@property (class, readonly, getter=companion) SSDKKotlinx_datetimeInstantCompanion *companion __attribute__((swift_name("companion")));
- (int32_t)compareToOther:(SSDKKotlinx_datetimeInstant *)other __attribute__((swift_name("compareTo(other:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (SSDKKotlinx_datetimeInstant *)minusDuration:(int64_t)duration __attribute__((swift_name("minus(duration:)")));
- (int64_t)minusOther:(SSDKKotlinx_datetimeInstant *)other __attribute__((swift_name("minus(other:)")));
- (SSDKKotlinx_datetimeInstant *)plusDuration:(int64_t)duration __attribute__((swift_name("plus(duration:)")));
- (int64_t)toEpochMilliseconds __attribute__((swift_name("toEpochMilliseconds()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int64_t epochSeconds __attribute__((swift_name("epochSeconds")));
@property (readonly) int32_t nanosecondsOfSecond __attribute__((swift_name("nanosecondsOfSecond")));
@end;

__attribute__((swift_name("Kotlinx_serialization_coreEncoder")))
@protocol SSDKKotlinx_serialization_coreEncoder
@required
- (id<SSDKKotlinx_serialization_coreCompositeEncoder>)beginCollectionDescriptor:(id<SSDKKotlinx_serialization_coreSerialDescriptor>)descriptor collectionSize:(int32_t)collectionSize __attribute__((swift_name("beginCollection(descriptor:collectionSize:)")));
- (id<SSDKKotlinx_serialization_coreCompositeEncoder>)beginStructureDescriptor:(id<SSDKKotlinx_serialization_coreSerialDescriptor>)descriptor __attribute__((swift_name("beginStructure(descriptor:)")));
- (void)encodeBooleanValue:(BOOL)value __attribute__((swift_name("encodeBoolean(value:)")));
- (void)encodeByteValue:(int8_t)value __attribute__((swift_name("encodeByte(value:)")));
- (void)encodeCharValue:(unichar)value __attribute__((swift_name("encodeChar(value:)")));
- (void)encodeDoubleValue:(double)value __attribute__((swift_name("encodeDouble(value:)")));
- (void)encodeEnumEnumDescriptor:(id<SSDKKotlinx_serialization_coreSerialDescriptor>)enumDescriptor index:(int32_t)index __attribute__((swift_name("encodeEnum(enumDescriptor:index:)")));
- (void)encodeFloatValue:(float)value __attribute__((swift_name("encodeFloat(value:)")));
- (id<SSDKKotlinx_serialization_coreEncoder>)encodeInlineInlineDescriptor:(id<SSDKKotlinx_serialization_coreSerialDescriptor>)inlineDescriptor __attribute__((swift_name("encodeInline(inlineDescriptor:)")));
- (void)encodeIntValue:(int32_t)value __attribute__((swift_name("encodeInt(value:)")));
- (void)encodeLongValue:(int64_t)value __attribute__((swift_name("encodeLong(value:)")));
- (void)encodeNotNullMark __attribute__((swift_name("encodeNotNullMark()")));
- (void)encodeNull __attribute__((swift_name("encodeNull()")));
- (void)encodeNullableSerializableValueSerializer:(id<SSDKKotlinx_serialization_coreSerializationStrategy>)serializer value:(id _Nullable)value __attribute__((swift_name("encodeNullableSerializableValue(serializer:value:)")));
- (void)encodeSerializableValueSerializer:(id<SSDKKotlinx_serialization_coreSerializationStrategy>)serializer value:(id _Nullable)value __attribute__((swift_name("encodeSerializableValue(serializer:value:)")));
- (void)encodeShortValue:(int16_t)value __attribute__((swift_name("encodeShort(value:)")));
- (void)encodeStringValue:(NSString *)value __attribute__((swift_name("encodeString(value:)")));
@property (readonly) SSDKKotlinx_serialization_coreSerializersModule *serializersModule __attribute__((swift_name("serializersModule")));
@end;

__attribute__((swift_name("Kotlinx_serialization_coreSerialDescriptor")))
@protocol SSDKKotlinx_serialization_coreSerialDescriptor
@required
- (NSArray<id<SSDKKotlinAnnotation>> *)getElementAnnotationsIndex:(int32_t)index __attribute__((swift_name("getElementAnnotations(index:)")));
- (id<SSDKKotlinx_serialization_coreSerialDescriptor>)getElementDescriptorIndex:(int32_t)index __attribute__((swift_name("getElementDescriptor(index:)")));
- (int32_t)getElementIndexName:(NSString *)name __attribute__((swift_name("getElementIndex(name:)")));
- (NSString *)getElementNameIndex:(int32_t)index __attribute__((swift_name("getElementName(index:)")));
- (BOOL)isElementOptionalIndex:(int32_t)index __attribute__((swift_name("isElementOptional(index:)")));
@property (readonly) NSArray<id<SSDKKotlinAnnotation>> *annotations __attribute__((swift_name("annotations")));
@property (readonly) int32_t elementsCount __attribute__((swift_name("elementsCount")));
@property (readonly) BOOL isInline __attribute__((swift_name("isInline")));
@property (readonly) BOOL isNullable __attribute__((swift_name("isNullable")));
@property (readonly) SSDKKotlinx_serialization_coreSerialKind *kind __attribute__((swift_name("kind")));
@property (readonly) NSString *serialName __attribute__((swift_name("serialName")));
@end;

__attribute__((swift_name("Kotlinx_serialization_coreDecoder")))
@protocol SSDKKotlinx_serialization_coreDecoder
@required
- (id<SSDKKotlinx_serialization_coreCompositeDecoder>)beginStructureDescriptor:(id<SSDKKotlinx_serialization_coreSerialDescriptor>)descriptor __attribute__((swift_name("beginStructure(descriptor:)")));
- (BOOL)decodeBoolean __attribute__((swift_name("decodeBoolean()")));
- (int8_t)decodeByte __attribute__((swift_name("decodeByte()")));
- (unichar)decodeChar __attribute__((swift_name("decodeChar()")));
- (double)decodeDouble __attribute__((swift_name("decodeDouble()")));
- (int32_t)decodeEnumEnumDescriptor:(id<SSDKKotlinx_serialization_coreSerialDescriptor>)enumDescriptor __attribute__((swift_name("decodeEnum(enumDescriptor:)")));
- (float)decodeFloat __attribute__((swift_name("decodeFloat()")));
- (id<SSDKKotlinx_serialization_coreDecoder>)decodeInlineInlineDescriptor:(id<SSDKKotlinx_serialization_coreSerialDescriptor>)inlineDescriptor __attribute__((swift_name("decodeInline(inlineDescriptor:)")));
- (int32_t)decodeInt __attribute__((swift_name("decodeInt()")));
- (int64_t)decodeLong __attribute__((swift_name("decodeLong()")));
- (BOOL)decodeNotNullMark __attribute__((swift_name("decodeNotNullMark()")));
- (SSDKKotlinNothing * _Nullable)decodeNull __attribute__((swift_name("decodeNull()")));
- (id _Nullable)decodeNullableSerializableValueDeserializer:(id<SSDKKotlinx_serialization_coreDeserializationStrategy>)deserializer __attribute__((swift_name("decodeNullableSerializableValue(deserializer:)")));
- (id _Nullable)decodeSerializableValueDeserializer:(id<SSDKKotlinx_serialization_coreDeserializationStrategy>)deserializer __attribute__((swift_name("decodeSerializableValue(deserializer:)")));
- (int16_t)decodeShort __attribute__((swift_name("decodeShort()")));
- (NSString *)decodeString __attribute__((swift_name("decodeString()")));
@property (readonly) SSDKKotlinx_serialization_coreSerializersModule *serializersModule __attribute__((swift_name("serializersModule")));
@end;

__attribute__((swift_name("Kotlinx_serialization_jsonJsonElement")))
@interface SSDKKotlinx_serialization_jsonJsonElement : SSDKBase
@property (class, readonly, getter=companion) SSDKKotlinx_serialization_jsonJsonElementCompanion *companion __attribute__((swift_name("companion")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_coreHttpRequestData")))
@interface SSDKKtor_client_coreHttpRequestData : SSDKBase
- (instancetype)initWithUrl:(SSDKKtor_httpUrl *)url method:(SSDKKtor_httpHttpMethod *)method headers:(id<SSDKKtor_httpHeaders>)headers body:(SSDKKtor_httpOutgoingContent *)body executionContext:(id<SSDKKotlinx_coroutines_coreJob>)executionContext attributes:(id<SSDKKtor_utilsAttributes>)attributes __attribute__((swift_name("init(url:method:headers:body:executionContext:attributes:)"))) __attribute__((objc_designated_initializer));
- (id _Nullable)getCapabilityOrNullKey:(id<SSDKKtor_client_coreHttpClientEngineCapability>)key __attribute__((swift_name("getCapabilityOrNull(key:)")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) id<SSDKKtor_utilsAttributes> attributes __attribute__((swift_name("attributes")));
@property (readonly) SSDKKtor_httpOutgoingContent *body __attribute__((swift_name("body")));
@property (readonly) id<SSDKKotlinx_coroutines_coreJob> executionContext __attribute__((swift_name("executionContext")));
@property (readonly) id<SSDKKtor_httpHeaders> headers __attribute__((swift_name("headers")));
@property (readonly) SSDKKtor_httpHttpMethod *method __attribute__((swift_name("method")));
@property (readonly) SSDKKtor_httpUrl *url __attribute__((swift_name("url")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_coreHttpResponseData")))
@interface SSDKKtor_client_coreHttpResponseData : SSDKBase
- (instancetype)initWithStatusCode:(SSDKKtor_httpHttpStatusCode *)statusCode requestTime:(SSDKKtor_utilsGMTDate *)requestTime headers:(id<SSDKKtor_httpHeaders>)headers version:(SSDKKtor_httpHttpProtocolVersion *)version body:(id)body callContext:(id<SSDKKotlinCoroutineContext>)callContext __attribute__((swift_name("init(statusCode:requestTime:headers:version:body:callContext:)"))) __attribute__((objc_designated_initializer));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) id body __attribute__((swift_name("body")));
@property (readonly) id<SSDKKotlinCoroutineContext> callContext __attribute__((swift_name("callContext")));
@property (readonly) id<SSDKKtor_httpHeaders> headers __attribute__((swift_name("headers")));
@property (readonly) SSDKKtor_utilsGMTDate *requestTime __attribute__((swift_name("requestTime")));
@property (readonly) SSDKKtor_utilsGMTDate *responseTime __attribute__((swift_name("responseTime")));
@property (readonly) SSDKKtor_httpHttpStatusCode *statusCode __attribute__((swift_name("statusCode")));
@property (readonly) SSDKKtor_httpHttpProtocolVersion *version __attribute__((swift_name("version")));
@end;

__attribute__((swift_name("Ktor_client_coreHttpClientEngineConfig")))
@interface SSDKKtor_client_coreHttpClientEngineConfig : SSDKBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@property BOOL pipelining __attribute__((swift_name("pipelining")));
@property SSDKKtor_client_coreProxyConfig * _Nullable proxy __attribute__((swift_name("proxy")));
@property int32_t threadsCount __attribute__((swift_name("threadsCount")));
@end;

__attribute__((swift_name("KotlinCoroutineContext")))
@protocol SSDKKotlinCoroutineContext
@required
- (id _Nullable)foldInitial:(id _Nullable)initial operation:(id _Nullable (^)(id _Nullable, id<SSDKKotlinCoroutineContextElement>))operation __attribute__((swift_name("fold(initial:operation:)")));
- (id<SSDKKotlinCoroutineContextElement> _Nullable)getKey:(id<SSDKKotlinCoroutineContextKey>)key __attribute__((swift_name("get(key:)")));
- (id<SSDKKotlinCoroutineContext>)minusKeyKey:(id<SSDKKotlinCoroutineContextKey>)key __attribute__((swift_name("minusKey(key:)")));
- (id<SSDKKotlinCoroutineContext>)plusContext:(id<SSDKKotlinCoroutineContext>)context __attribute__((swift_name("plus(context:)")));
@end;

__attribute__((swift_name("KotlinCoroutineContextElement")))
@protocol SSDKKotlinCoroutineContextElement <SSDKKotlinCoroutineContext>
@required
@property (readonly) id<SSDKKotlinCoroutineContextKey> key __attribute__((swift_name("key")));
@end;

__attribute__((swift_name("KotlinAbstractCoroutineContextElement")))
@interface SSDKKotlinAbstractCoroutineContextElement : SSDKBase <SSDKKotlinCoroutineContextElement>
- (instancetype)initWithKey:(id<SSDKKotlinCoroutineContextKey>)key __attribute__((swift_name("init(key:)"))) __attribute__((objc_designated_initializer));
@property (readonly) id<SSDKKotlinCoroutineContextKey> key __attribute__((swift_name("key")));
@end;

__attribute__((swift_name("KotlinContinuationInterceptor")))
@protocol SSDKKotlinContinuationInterceptor <SSDKKotlinCoroutineContextElement>
@required
- (id<SSDKKotlinContinuation>)interceptContinuationContinuation:(id<SSDKKotlinContinuation>)continuation __attribute__((swift_name("interceptContinuation(continuation:)")));
- (void)releaseInterceptedContinuationContinuation:(id<SSDKKotlinContinuation>)continuation __attribute__((swift_name("releaseInterceptedContinuation(continuation:)")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreCoroutineDispatcher")))
@interface SSDKKotlinx_coroutines_coreCoroutineDispatcher : SSDKKotlinAbstractCoroutineContextElement <SSDKKotlinContinuationInterceptor>
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (instancetype)initWithKey:(id<SSDKKotlinCoroutineContextKey>)key __attribute__((swift_name("init(key:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly, getter=companion) SSDKKotlinx_coroutines_coreCoroutineDispatcherKey *companion __attribute__((swift_name("companion")));
- (void)dispatchContext:(id<SSDKKotlinCoroutineContext>)context block:(id<SSDKKotlinx_coroutines_coreRunnable>)block __attribute__((swift_name("dispatch(context:block:)")));
- (void)dispatchYieldContext:(id<SSDKKotlinCoroutineContext>)context block:(id<SSDKKotlinx_coroutines_coreRunnable>)block __attribute__((swift_name("dispatchYield(context:block:)")));
- (id<SSDKKotlinContinuation>)interceptContinuationContinuation:(id<SSDKKotlinContinuation>)continuation __attribute__((swift_name("interceptContinuation(continuation:)")));
- (BOOL)isDispatchNeededContext:(id<SSDKKotlinCoroutineContext>)context __attribute__((swift_name("isDispatchNeeded(context:)")));
- (SSDKKotlinx_coroutines_coreCoroutineDispatcher *)limitedParallelismParallelism:(int32_t)parallelism __attribute__((swift_name("limitedParallelism(parallelism:)")));
- (SSDKKotlinx_coroutines_coreCoroutineDispatcher *)plusOther:(SSDKKotlinx_coroutines_coreCoroutineDispatcher *)other __attribute__((swift_name("plus(other:)"))) __attribute__((unavailable("Operator '+' on two CoroutineDispatcher objects is meaningless. CoroutineDispatcher is a coroutine context element and `+` is a set-sum operator for coroutine contexts. The dispatcher to the right of `+` just replaces the dispatcher to the left.")));
- (void)releaseInterceptedContinuationContinuation:(id<SSDKKotlinContinuation>)continuation __attribute__((swift_name("releaseInterceptedContinuation(continuation:)")));
- (NSString *)description __attribute__((swift_name("description()")));
@end;

__attribute__((swift_name("Ktor_client_coreHttpClientEngineCapability")))
@protocol SSDKKtor_client_coreHttpClientEngineCapability
@required
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_httpUrl.Companion")))
@interface SSDKKtor_httpUrlCompanion : SSDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SSDKKtor_httpUrlCompanion *shared __attribute__((swift_name("shared")));
@end;

__attribute__((swift_name("Ktor_utilsStringValues")))
@protocol SSDKKtor_utilsStringValues
@required
- (BOOL)containsName:(NSString *)name __attribute__((swift_name("contains(name:)")));
- (BOOL)containsName:(NSString *)name value:(NSString *)value __attribute__((swift_name("contains(name:value:)")));
- (NSSet<id<SSDKKotlinMapEntry>> *)entries __attribute__((swift_name("entries()")));
- (void)forEachBody:(void (^)(NSString *, NSArray<NSString *> *))body __attribute__((swift_name("forEach(body:)")));
- (NSString * _Nullable)getName:(NSString *)name __attribute__((swift_name("get(name:)")));
- (NSArray<NSString *> * _Nullable)getAllName:(NSString *)name __attribute__((swift_name("getAll(name:)")));
- (BOOL)isEmpty __attribute__((swift_name("isEmpty()")));
- (NSSet<NSString *> *)names __attribute__((swift_name("names()")));
@property (readonly) BOOL caseInsensitiveName __attribute__((swift_name("caseInsensitiveName")));
@end;

__attribute__((swift_name("Ktor_httpParameters")))
@protocol SSDKKtor_httpParameters <SSDKKtor_utilsStringValues>
@required
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_httpURLProtocol")))
@interface SSDKKtor_httpURLProtocol : SSDKBase
- (instancetype)initWithName:(NSString *)name defaultPort:(int32_t)defaultPort __attribute__((swift_name("init(name:defaultPort:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) SSDKKtor_httpURLProtocolCompanion *companion __attribute__((swift_name("companion")));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (int32_t)component2 __attribute__((swift_name("component2()")));
- (SSDKKtor_httpURLProtocol *)doCopyName:(NSString *)name defaultPort:(int32_t)defaultPort __attribute__((swift_name("doCopy(name:defaultPort:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int32_t defaultPort __attribute__((swift_name("defaultPort")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_coreHttpClientConfig")))
@interface SSDKKtor_client_coreHttpClientConfig<T> : SSDKBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (SSDKKtor_client_coreHttpClientConfig<T> *)clone __attribute__((swift_name("clone()")));
- (void)engineBlock:(void (^)(T))block __attribute__((swift_name("engine(block:)")));
- (void)installClient:(SSDKKtor_client_coreHttpClient *)client __attribute__((swift_name("install(client:)")));
- (void)installPlugin:(id<SSDKKtor_client_coreHttpClientPlugin>)plugin configure:(void (^)(id))configure __attribute__((swift_name("install(plugin:configure:)")));
- (void)installKey:(NSString *)key block:(void (^)(SSDKKtor_client_coreHttpClient *))block __attribute__((swift_name("install(key:block:)")));
- (void)plusAssignOther:(SSDKKtor_client_coreHttpClientConfig<T> *)other __attribute__((swift_name("plusAssign(other:)")));
@property BOOL developmentMode __attribute__((swift_name("developmentMode")));
@property BOOL expectSuccess __attribute__((swift_name("expectSuccess")));
@property BOOL followRedirects __attribute__((swift_name("followRedirects")));
@property BOOL useDefaultTransformers __attribute__((swift_name("useDefaultTransformers")));
@end;

__attribute__((swift_name("Ktor_utilsAttributes")))
@protocol SSDKKtor_utilsAttributes
@required
- (id)computeIfAbsentKey:(SSDKKtor_utilsAttributeKey<id> *)key block:(id (^)(void))block __attribute__((swift_name("computeIfAbsent(key:block:)")));
- (BOOL)containsKey:(SSDKKtor_utilsAttributeKey<id> *)key __attribute__((swift_name("contains(key:)")));
- (id)getKey_:(SSDKKtor_utilsAttributeKey<id> *)key __attribute__((swift_name("get(key_:)")));
- (id _Nullable)getOrNullKey:(SSDKKtor_utilsAttributeKey<id> *)key __attribute__((swift_name("getOrNull(key:)")));
- (void)putKey:(SSDKKtor_utilsAttributeKey<id> *)key value:(id)value __attribute__((swift_name("put(key:value:)")));
- (void)removeKey:(SSDKKtor_utilsAttributeKey<id> *)key __attribute__((swift_name("remove(key:)")));
- (id)takeKey:(SSDKKtor_utilsAttributeKey<id> *)key __attribute__((swift_name("take(key:)")));
- (id _Nullable)takeOrNullKey:(SSDKKtor_utilsAttributeKey<id> *)key __attribute__((swift_name("takeOrNull(key:)")));
@property (readonly) NSArray<SSDKKtor_utilsAttributeKey<id> *> *allKeys __attribute__((swift_name("allKeys")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_eventsEvents")))
@interface SSDKKtor_eventsEvents : SSDKBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (void)raiseDefinition:(SSDKKtor_eventsEventDefinition<id> *)definition value:(id _Nullable)value __attribute__((swift_name("raise(definition:value:)")));
- (id<SSDKKotlinx_coroutines_coreDisposableHandle>)subscribeDefinition:(SSDKKtor_eventsEventDefinition<id> *)definition handler:(void (^)(id _Nullable))handler __attribute__((swift_name("subscribe(definition:handler:)")));
- (void)unsubscribeDefinition:(SSDKKtor_eventsEventDefinition<id> *)definition handler:(void (^)(id _Nullable))handler __attribute__((swift_name("unsubscribe(definition:handler:)")));
@end;

__attribute__((swift_name("Ktor_utilsPipeline")))
@interface SSDKKtor_utilsPipeline<TSubject, TContext> : SSDKBase
- (instancetype)initWithPhase:(SSDKKtor_utilsPipelinePhase *)phase interceptors:(NSArray<id<SSDKKotlinSuspendFunction2>> *)interceptors __attribute__((swift_name("init(phase:interceptors:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithPhases:(SSDKKotlinArray<SSDKKtor_utilsPipelinePhase *> *)phases __attribute__((swift_name("init(phases:)"))) __attribute__((objc_designated_initializer));
- (void)addPhasePhase:(SSDKKtor_utilsPipelinePhase *)phase __attribute__((swift_name("addPhase(phase:)")));
- (void)afterIntercepted __attribute__((swift_name("afterIntercepted()")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)executeContext:(TContext)context subject:(TSubject)subject completionHandler:(void (^)(TSubject _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("execute(context:subject:completionHandler:)")));
- (void)insertPhaseAfterReference:(SSDKKtor_utilsPipelinePhase *)reference phase:(SSDKKtor_utilsPipelinePhase *)phase __attribute__((swift_name("insertPhaseAfter(reference:phase:)")));
- (void)insertPhaseBeforeReference:(SSDKKtor_utilsPipelinePhase *)reference phase:(SSDKKtor_utilsPipelinePhase *)phase __attribute__((swift_name("insertPhaseBefore(reference:phase:)")));
- (void)interceptPhase:(SSDKKtor_utilsPipelinePhase *)phase block:(id<SSDKKotlinSuspendFunction2>)block __attribute__((swift_name("intercept(phase:block:)")));
- (NSArray<id<SSDKKotlinSuspendFunction2>> *)interceptorsForPhasePhase:(SSDKKtor_utilsPipelinePhase *)phase __attribute__((swift_name("interceptorsForPhase(phase:)")));
- (void)mergeFrom:(SSDKKtor_utilsPipeline<TSubject, TContext> *)from __attribute__((swift_name("merge(from:)")));
- (void)mergePhasesFrom:(SSDKKtor_utilsPipeline<TSubject, TContext> *)from __attribute__((swift_name("mergePhases(from:)")));
- (void)resetFromFrom:(SSDKKtor_utilsPipeline<TSubject, TContext> *)from __attribute__((swift_name("resetFrom(from:)")));
@property (readonly) id<SSDKKtor_utilsAttributes> attributes __attribute__((swift_name("attributes")));
@property (readonly) BOOL developmentMode __attribute__((swift_name("developmentMode")));
@property (readonly, getter=isEmpty_) BOOL isEmpty __attribute__((swift_name("isEmpty")));
@property (readonly) NSArray<SSDKKtor_utilsPipelinePhase *> *items __attribute__((swift_name("items")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_coreHttpReceivePipeline")))
@interface SSDKKtor_client_coreHttpReceivePipeline : SSDKKtor_utilsPipeline<SSDKKtor_client_coreHttpResponse *, SSDKKotlinUnit *>
- (instancetype)initWithDevelopmentMode:(BOOL)developmentMode __attribute__((swift_name("init(developmentMode:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithPhase:(SSDKKtor_utilsPipelinePhase *)phase interceptors:(NSArray<id<SSDKKotlinSuspendFunction2>> *)interceptors __attribute__((swift_name("init(phase:interceptors:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (instancetype)initWithPhases:(SSDKKotlinArray<SSDKKtor_utilsPipelinePhase *> *)phases __attribute__((swift_name("init(phases:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly, getter=companion) SSDKKtor_client_coreHttpReceivePipelinePhases *companion __attribute__((swift_name("companion")));
@property (readonly) BOOL developmentMode __attribute__((swift_name("developmentMode")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_coreHttpRequestPipeline")))
@interface SSDKKtor_client_coreHttpRequestPipeline : SSDKKtor_utilsPipeline<id, SSDKKtor_client_coreHttpRequestBuilder *>
- (instancetype)initWithDevelopmentMode:(BOOL)developmentMode __attribute__((swift_name("init(developmentMode:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithPhase:(SSDKKtor_utilsPipelinePhase *)phase interceptors:(NSArray<id<SSDKKotlinSuspendFunction2>> *)interceptors __attribute__((swift_name("init(phase:interceptors:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (instancetype)initWithPhases:(SSDKKotlinArray<SSDKKtor_utilsPipelinePhase *> *)phases __attribute__((swift_name("init(phases:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly, getter=companion) SSDKKtor_client_coreHttpRequestPipelinePhases *companion __attribute__((swift_name("companion")));
@property (readonly) BOOL developmentMode __attribute__((swift_name("developmentMode")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_coreHttpResponsePipeline")))
@interface SSDKKtor_client_coreHttpResponsePipeline : SSDKKtor_utilsPipeline<SSDKKtor_client_coreHttpResponseContainer *, SSDKKtor_client_coreHttpClientCall *>
- (instancetype)initWithDevelopmentMode:(BOOL)developmentMode __attribute__((swift_name("init(developmentMode:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithPhase:(SSDKKtor_utilsPipelinePhase *)phase interceptors:(NSArray<id<SSDKKotlinSuspendFunction2>> *)interceptors __attribute__((swift_name("init(phase:interceptors:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (instancetype)initWithPhases:(SSDKKotlinArray<SSDKKtor_utilsPipelinePhase *> *)phases __attribute__((swift_name("init(phases:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly, getter=companion) SSDKKtor_client_coreHttpResponsePipelinePhases *companion __attribute__((swift_name("companion")));
@property (readonly) BOOL developmentMode __attribute__((swift_name("developmentMode")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_coreHttpSendPipeline")))
@interface SSDKKtor_client_coreHttpSendPipeline : SSDKKtor_utilsPipeline<id, SSDKKtor_client_coreHttpRequestBuilder *>
- (instancetype)initWithDevelopmentMode:(BOOL)developmentMode __attribute__((swift_name("init(developmentMode:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithPhase:(SSDKKtor_utilsPipelinePhase *)phase interceptors:(NSArray<id<SSDKKotlinSuspendFunction2>> *)interceptors __attribute__((swift_name("init(phase:interceptors:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (instancetype)initWithPhases:(SSDKKotlinArray<SSDKKtor_utilsPipelinePhase *> *)phases __attribute__((swift_name("init(phases:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly, getter=companion) SSDKKtor_client_coreHttpSendPipelinePhases *companion __attribute__((swift_name("companion")));
@property (readonly) BOOL developmentMode __attribute__((swift_name("developmentMode")));
@end;

__attribute__((swift_name("Ktor_httpHeaders")))
@protocol SSDKKtor_httpHeaders <SSDKKtor_utilsStringValues>
@required
@end;

__attribute__((swift_name("Ktor_client_coreHttpClientCall")))
@interface SSDKKtor_client_coreHttpClientCall : SSDKBase <SSDKKotlinx_coroutines_coreCoroutineScope>
- (instancetype)initWithClient:(SSDKKtor_client_coreHttpClient *)client requestData:(SSDKKtor_client_coreHttpRequestData *)requestData responseData:(SSDKKtor_client_coreHttpResponseData *)responseData __attribute__((swift_name("init(client:requestData:responseData:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithClient:(SSDKKtor_client_coreHttpClient *)client __attribute__((swift_name("init(client:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) SSDKKtor_client_coreHttpClientCallCompanion *companion __attribute__((swift_name("companion")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)bodyInfo:(SSDKKtor_utilsTypeInfo *)info completionHandler:(void (^)(id _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("body(info:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)getResponseContentWithCompletionHandler:(void (^)(id<SSDKKtor_ioByteReadChannel> _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("getResponseContent(completionHandler:)")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) BOOL allowDoubleReceive __attribute__((swift_name("allowDoubleReceive")));
@property (readonly) id<SSDKKtor_utilsAttributes> attributes __attribute__((swift_name("attributes")));
@property (readonly) SSDKKtor_client_coreHttpClient *client __attribute__((swift_name("client")));
@property (readonly) id<SSDKKotlinCoroutineContext> coroutineContext __attribute__((swift_name("coroutineContext")));
@property id<SSDKKtor_client_coreHttpRequest> request __attribute__((swift_name("request")));
@property SSDKKtor_client_coreHttpResponse *response __attribute__((swift_name("response")));
@end;

__attribute__((swift_name("Ktor_ioByteReadChannel")))
@protocol SSDKKtor_ioByteReadChannel
@required

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)awaitContentWithCompletionHandler:(void (^)(SSDKKotlinUnit * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("awaitContent(completionHandler:)")));
- (BOOL)cancelCause:(SSDKKotlinThrowable * _Nullable)cause __attribute__((swift_name("cancel(cause:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)discardMax:(int64_t)max completionHandler:(void (^)(SSDKLong * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("discard(max:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)peekToDestination:(SSDKKtor_ioMemory *)destination destinationOffset:(int64_t)destinationOffset offset:(int64_t)offset min:(int64_t)min max:(int64_t)max completionHandler:(void (^)(SSDKLong * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("peekTo(destination:destinationOffset:offset:min:max:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)readAvailableDst:(SSDKKtor_ioChunkBuffer *)dst completionHandler:(void (^)(SSDKInt * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("readAvailable(dst:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)readAvailableDst:(SSDKKotlinByteArray *)dst offset:(int32_t)offset length:(int32_t)length completionHandler:(void (^)(SSDKInt * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("readAvailable(dst:offset:length:completionHandler:)")));
- (int32_t)readAvailableMin:(int32_t)min block:(void (^)(SSDKKtor_ioBuffer *))block __attribute__((swift_name("readAvailable(min:block:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)readAvailableDst:(void *)dst offset:(int32_t)offset length:(int32_t)length completionHandler_:(void (^)(SSDKInt * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("readAvailable(dst:offset:length:completionHandler_:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)readAvailableDst:(void *)dst offset:(int64_t)offset length:(int64_t)length completionHandler__:(void (^)(SSDKInt * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("readAvailable(dst:offset:length:completionHandler__:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)readBooleanWithCompletionHandler:(void (^)(SSDKBoolean * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("readBoolean(completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)readByteWithCompletionHandler:(void (^)(SSDKByte * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("readByte(completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)readDoubleWithCompletionHandler:(void (^)(SSDKDouble * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("readDouble(completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)readFloatWithCompletionHandler:(void (^)(SSDKFloat * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("readFloat(completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)readFullyDst:(SSDKKtor_ioChunkBuffer *)dst n:(int32_t)n completionHandler:(void (^)(SSDKKotlinUnit * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("readFully(dst:n:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)readFullyDst:(SSDKKotlinByteArray *)dst offset:(int32_t)offset length:(int32_t)length completionHandler:(void (^)(SSDKKotlinUnit * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("readFully(dst:offset:length:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)readFullyDst:(void *)dst offset:(int32_t)offset length:(int32_t)length completionHandler_:(void (^)(SSDKKotlinUnit * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("readFully(dst:offset:length:completionHandler_:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)readFullyDst:(void *)dst offset:(int64_t)offset length:(int64_t)length completionHandler__:(void (^)(SSDKKotlinUnit * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("readFully(dst:offset:length:completionHandler__:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)readIntWithCompletionHandler:(void (^)(SSDKInt * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("readInt(completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)readLongWithCompletionHandler:(void (^)(SSDKLong * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("readLong(completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)readPacketSize:(int32_t)size completionHandler:(void (^)(SSDKKtor_ioByteReadPacket * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("readPacket(size:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)readRemainingLimit:(int64_t)limit completionHandler:(void (^)(SSDKKtor_ioByteReadPacket * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("readRemaining(limit:completionHandler:)")));
- (void)readSessionConsumer:(void (^)(id<SSDKKtor_ioReadSession>))consumer __attribute__((swift_name("readSession(consumer:)"))) __attribute__((deprecated("Use read { } instead.")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)readShortWithCompletionHandler:(void (^)(SSDKShort * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("readShort(completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)readSuspendableSessionConsumer:(id<SSDKKotlinSuspendFunction1>)consumer completionHandler:(void (^)(SSDKKotlinUnit * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("readSuspendableSession(consumer:completionHandler:)"))) __attribute__((deprecated("Use read { } instead.")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)readUTF8LineLimit:(int32_t)limit completionHandler:(void (^)(NSString * _Nullable_result, NSError * _Nullable))completionHandler __attribute__((swift_name("readUTF8Line(limit:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)readUTF8LineToOut:(id<SSDKKotlinAppendable>)out limit:(int32_t)limit completionHandler:(void (^)(SSDKBoolean * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("readUTF8LineTo(out:limit:completionHandler:)")));
@property (readonly) int32_t availableForRead __attribute__((swift_name("availableForRead")));
@property (readonly) SSDKKotlinThrowable * _Nullable closedCause __attribute__((swift_name("closedCause")));
@property (readonly) BOOL isClosedForRead __attribute__((swift_name("isClosedForRead")));
@property (readonly) BOOL isClosedForWrite __attribute__((swift_name("isClosedForWrite")));
@property (readonly) int64_t totalBytesRead __attribute__((swift_name("totalBytesRead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_utilsGMTDate")))
@interface SSDKKtor_utilsGMTDate : SSDKBase <SSDKKotlinComparable>
@property (class, readonly, getter=companion) SSDKKtor_utilsGMTDateCompanion *companion __attribute__((swift_name("companion")));
- (int32_t)compareToOther:(SSDKKtor_utilsGMTDate *)other __attribute__((swift_name("compareTo(other:)")));
- (int32_t)component1 __attribute__((swift_name("component1()")));
- (int32_t)component2 __attribute__((swift_name("component2()")));
- (int32_t)component3 __attribute__((swift_name("component3()")));
- (SSDKKtor_utilsWeekDay *)component4 __attribute__((swift_name("component4()")));
- (int32_t)component5 __attribute__((swift_name("component5()")));
- (int32_t)component6 __attribute__((swift_name("component6()")));
- (SSDKKtor_utilsMonth *)component7 __attribute__((swift_name("component7()")));
- (int32_t)component8 __attribute__((swift_name("component8()")));
- (int64_t)component9 __attribute__((swift_name("component9()")));
- (SSDKKtor_utilsGMTDate *)doCopySeconds:(int32_t)seconds minutes:(int32_t)minutes hours:(int32_t)hours dayOfWeek:(SSDKKtor_utilsWeekDay *)dayOfWeek dayOfMonth:(int32_t)dayOfMonth dayOfYear:(int32_t)dayOfYear month:(SSDKKtor_utilsMonth *)month year:(int32_t)year timestamp:(int64_t)timestamp __attribute__((swift_name("doCopy(seconds:minutes:hours:dayOfWeek:dayOfMonth:dayOfYear:month:year:timestamp:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int32_t dayOfMonth __attribute__((swift_name("dayOfMonth")));
@property (readonly) SSDKKtor_utilsWeekDay *dayOfWeek __attribute__((swift_name("dayOfWeek")));
@property (readonly) int32_t dayOfYear __attribute__((swift_name("dayOfYear")));
@property (readonly) int32_t hours __attribute__((swift_name("hours")));
@property (readonly) int32_t minutes __attribute__((swift_name("minutes")));
@property (readonly) SSDKKtor_utilsMonth *month __attribute__((swift_name("month")));
@property (readonly) int32_t seconds __attribute__((swift_name("seconds")));
@property (readonly) int64_t timestamp __attribute__((swift_name("timestamp")));
@property (readonly) int32_t year __attribute__((swift_name("year")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_httpHttpStatusCode")))
@interface SSDKKtor_httpHttpStatusCode : SSDKBase
- (instancetype)initWithValue:(int32_t)value description:(NSString *)description __attribute__((swift_name("init(value:description:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) SSDKKtor_httpHttpStatusCodeCompanion *companion __attribute__((swift_name("companion")));
- (int32_t)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (SSDKKtor_httpHttpStatusCode *)doCopyValue:(int32_t)value description:(NSString *)description __attribute__((swift_name("doCopy(value:description:)")));
- (SSDKKtor_httpHttpStatusCode *)descriptionValue:(NSString *)value __attribute__((swift_name("description(value:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *description_ __attribute__((swift_name("description_")));
@property (readonly) int32_t value __attribute__((swift_name("value")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_httpHttpProtocolVersion")))
@interface SSDKKtor_httpHttpProtocolVersion : SSDKBase
- (instancetype)initWithName:(NSString *)name major:(int32_t)major minor:(int32_t)minor __attribute__((swift_name("init(name:major:minor:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) SSDKKtor_httpHttpProtocolVersionCompanion *companion __attribute__((swift_name("companion")));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (int32_t)component2 __attribute__((swift_name("component2()")));
- (int32_t)component3 __attribute__((swift_name("component3()")));
- (SSDKKtor_httpHttpProtocolVersion *)doCopyName:(NSString *)name major:(int32_t)major minor:(int32_t)minor __attribute__((swift_name("doCopy(name:major:minor:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int32_t major __attribute__((swift_name("major")));
@property (readonly) int32_t minor __attribute__((swift_name("minor")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@end;

__attribute__((swift_name("KotlinIterator")))
@protocol SSDKKotlinIterator
@required
- (BOOL)hasNext __attribute__((swift_name("hasNext()")));
- (id _Nullable)next __attribute__((swift_name("next()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Kotlinx_datetimeInstant.Companion")))
@interface SSDKKotlinx_datetimeInstantCompanion : SSDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SSDKKotlinx_datetimeInstantCompanion *shared __attribute__((swift_name("shared")));
- (SSDKKotlinx_datetimeInstant *)fromEpochMillisecondsEpochMilliseconds:(int64_t)epochMilliseconds __attribute__((swift_name("fromEpochMilliseconds(epochMilliseconds:)")));
- (SSDKKotlinx_datetimeInstant *)fromEpochSecondsEpochSeconds:(int64_t)epochSeconds nanosecondAdjustment:(int32_t)nanosecondAdjustment __attribute__((swift_name("fromEpochSeconds(epochSeconds:nanosecondAdjustment:)")));
- (SSDKKotlinx_datetimeInstant *)fromEpochSecondsEpochSeconds:(int64_t)epochSeconds nanosecondAdjustment_:(int64_t)nanosecondAdjustment __attribute__((swift_name("fromEpochSeconds(epochSeconds:nanosecondAdjustment_:)")));
- (SSDKKotlinx_datetimeInstant *)now __attribute__((swift_name("now()"))) __attribute__((unavailable("Use Clock.System.now() instead")));
- (SSDKKotlinx_datetimeInstant *)parseIsoString:(NSString *)isoString __attribute__((swift_name("parse(isoString:)")));
- (id<SSDKKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@property (readonly) SSDKKotlinx_datetimeInstant *DISTANT_FUTURE __attribute__((swift_name("DISTANT_FUTURE")));
@property (readonly) SSDKKotlinx_datetimeInstant *DISTANT_PAST __attribute__((swift_name("DISTANT_PAST")));
@end;

__attribute__((swift_name("Kotlinx_serialization_coreCompositeEncoder")))
@protocol SSDKKotlinx_serialization_coreCompositeEncoder
@required
- (void)encodeBooleanElementDescriptor:(id<SSDKKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(BOOL)value __attribute__((swift_name("encodeBooleanElement(descriptor:index:value:)")));
- (void)encodeByteElementDescriptor:(id<SSDKKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(int8_t)value __attribute__((swift_name("encodeByteElement(descriptor:index:value:)")));
- (void)encodeCharElementDescriptor:(id<SSDKKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(unichar)value __attribute__((swift_name("encodeCharElement(descriptor:index:value:)")));
- (void)encodeDoubleElementDescriptor:(id<SSDKKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(double)value __attribute__((swift_name("encodeDoubleElement(descriptor:index:value:)")));
- (void)encodeFloatElementDescriptor:(id<SSDKKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(float)value __attribute__((swift_name("encodeFloatElement(descriptor:index:value:)")));
- (id<SSDKKotlinx_serialization_coreEncoder>)encodeInlineElementDescriptor:(id<SSDKKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("encodeInlineElement(descriptor:index:)")));
- (void)encodeIntElementDescriptor:(id<SSDKKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(int32_t)value __attribute__((swift_name("encodeIntElement(descriptor:index:value:)")));
- (void)encodeLongElementDescriptor:(id<SSDKKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(int64_t)value __attribute__((swift_name("encodeLongElement(descriptor:index:value:)")));
- (void)encodeNullableSerializableElementDescriptor:(id<SSDKKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index serializer:(id<SSDKKotlinx_serialization_coreSerializationStrategy>)serializer value:(id _Nullable)value __attribute__((swift_name("encodeNullableSerializableElement(descriptor:index:serializer:value:)")));
- (void)encodeSerializableElementDescriptor:(id<SSDKKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index serializer:(id<SSDKKotlinx_serialization_coreSerializationStrategy>)serializer value:(id _Nullable)value __attribute__((swift_name("encodeSerializableElement(descriptor:index:serializer:value:)")));
- (void)encodeShortElementDescriptor:(id<SSDKKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(int16_t)value __attribute__((swift_name("encodeShortElement(descriptor:index:value:)")));
- (void)encodeStringElementDescriptor:(id<SSDKKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(NSString *)value __attribute__((swift_name("encodeStringElement(descriptor:index:value:)")));
- (void)endStructureDescriptor:(id<SSDKKotlinx_serialization_coreSerialDescriptor>)descriptor __attribute__((swift_name("endStructure(descriptor:)")));
- (BOOL)shouldEncodeElementDefaultDescriptor:(id<SSDKKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("shouldEncodeElementDefault(descriptor:index:)")));
@property (readonly) SSDKKotlinx_serialization_coreSerializersModule *serializersModule __attribute__((swift_name("serializersModule")));
@end;

__attribute__((swift_name("Kotlinx_serialization_coreSerializersModule")))
@interface SSDKKotlinx_serialization_coreSerializersModule : SSDKBase
- (void)dumpToCollector:(id<SSDKKotlinx_serialization_coreSerializersModuleCollector>)collector __attribute__((swift_name("dumpTo(collector:)")));
- (id<SSDKKotlinx_serialization_coreKSerializer> _Nullable)getContextualKClass:(id<SSDKKotlinKClass>)kClass typeArgumentsSerializers:(NSArray<id<SSDKKotlinx_serialization_coreKSerializer>> *)typeArgumentsSerializers __attribute__((swift_name("getContextual(kClass:typeArgumentsSerializers:)")));
- (id<SSDKKotlinx_serialization_coreSerializationStrategy> _Nullable)getPolymorphicBaseClass:(id<SSDKKotlinKClass>)baseClass value:(id)value __attribute__((swift_name("getPolymorphic(baseClass:value:)")));
- (id<SSDKKotlinx_serialization_coreDeserializationStrategy> _Nullable)getPolymorphicBaseClass:(id<SSDKKotlinKClass>)baseClass serializedClassName:(NSString * _Nullable)serializedClassName __attribute__((swift_name("getPolymorphic(baseClass:serializedClassName:)")));
@end;

__attribute__((swift_name("KotlinAnnotation")))
@protocol SSDKKotlinAnnotation
@required
@end;

__attribute__((swift_name("Kotlinx_serialization_coreSerialKind")))
@interface SSDKKotlinx_serialization_coreSerialKind : SSDKBase
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@end;

__attribute__((swift_name("Kotlinx_serialization_coreCompositeDecoder")))
@protocol SSDKKotlinx_serialization_coreCompositeDecoder
@required
- (BOOL)decodeBooleanElementDescriptor:(id<SSDKKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeBooleanElement(descriptor:index:)")));
- (int8_t)decodeByteElementDescriptor:(id<SSDKKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeByteElement(descriptor:index:)")));
- (unichar)decodeCharElementDescriptor:(id<SSDKKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeCharElement(descriptor:index:)")));
- (int32_t)decodeCollectionSizeDescriptor:(id<SSDKKotlinx_serialization_coreSerialDescriptor>)descriptor __attribute__((swift_name("decodeCollectionSize(descriptor:)")));
- (double)decodeDoubleElementDescriptor:(id<SSDKKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeDoubleElement(descriptor:index:)")));
- (int32_t)decodeElementIndexDescriptor:(id<SSDKKotlinx_serialization_coreSerialDescriptor>)descriptor __attribute__((swift_name("decodeElementIndex(descriptor:)")));
- (float)decodeFloatElementDescriptor:(id<SSDKKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeFloatElement(descriptor:index:)")));
- (id<SSDKKotlinx_serialization_coreDecoder>)decodeInlineElementDescriptor:(id<SSDKKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeInlineElement(descriptor:index:)")));
- (int32_t)decodeIntElementDescriptor:(id<SSDKKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeIntElement(descriptor:index:)")));
- (int64_t)decodeLongElementDescriptor:(id<SSDKKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeLongElement(descriptor:index:)")));
- (id _Nullable)decodeNullableSerializableElementDescriptor:(id<SSDKKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index deserializer:(id<SSDKKotlinx_serialization_coreDeserializationStrategy>)deserializer previousValue:(id _Nullable)previousValue __attribute__((swift_name("decodeNullableSerializableElement(descriptor:index:deserializer:previousValue:)")));
- (BOOL)decodeSequentially __attribute__((swift_name("decodeSequentially()")));
- (id _Nullable)decodeSerializableElementDescriptor:(id<SSDKKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index deserializer:(id<SSDKKotlinx_serialization_coreDeserializationStrategy>)deserializer previousValue:(id _Nullable)previousValue __attribute__((swift_name("decodeSerializableElement(descriptor:index:deserializer:previousValue:)")));
- (int16_t)decodeShortElementDescriptor:(id<SSDKKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeShortElement(descriptor:index:)")));
- (NSString *)decodeStringElementDescriptor:(id<SSDKKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeStringElement(descriptor:index:)")));
- (void)endStructureDescriptor:(id<SSDKKotlinx_serialization_coreSerialDescriptor>)descriptor __attribute__((swift_name("endStructure(descriptor:)")));
@property (readonly) SSDKKotlinx_serialization_coreSerializersModule *serializersModule __attribute__((swift_name("serializersModule")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinNothing")))
@interface SSDKKotlinNothing : SSDKBase
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Kotlinx_serialization_jsonJsonElement.Companion")))
@interface SSDKKotlinx_serialization_jsonJsonElementCompanion : SSDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SSDKKotlinx_serialization_jsonJsonElementCompanion *shared __attribute__((swift_name("shared")));
- (id<SSDKKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_httpHttpMethod")))
@interface SSDKKtor_httpHttpMethod : SSDKBase
- (instancetype)initWithValue:(NSString *)value __attribute__((swift_name("init(value:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) SSDKKtor_httpHttpMethodCompanion *companion __attribute__((swift_name("companion")));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (SSDKKtor_httpHttpMethod *)doCopyValue:(NSString *)value __attribute__((swift_name("doCopy(value:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *value __attribute__((swift_name("value")));
@end;

__attribute__((swift_name("Ktor_httpOutgoingContent")))
@interface SSDKKtor_httpOutgoingContent : SSDKBase
- (id _Nullable)getPropertyKey:(SSDKKtor_utilsAttributeKey<id> *)key __attribute__((swift_name("getProperty(key:)")));
- (void)setPropertyKey:(SSDKKtor_utilsAttributeKey<id> *)key value:(id _Nullable)value __attribute__((swift_name("setProperty(key:value:)")));
- (id<SSDKKtor_httpHeaders> _Nullable)trailers __attribute__((swift_name("trailers()")));
@property (readonly) SSDKLong * _Nullable contentLength __attribute__((swift_name("contentLength")));
@property (readonly) SSDKKtor_httpContentType * _Nullable contentType __attribute__((swift_name("contentType")));
@property (readonly) id<SSDKKtor_httpHeaders> headers __attribute__((swift_name("headers")));
@property (readonly) SSDKKtor_httpHttpStatusCode * _Nullable status __attribute__((swift_name("status")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreJob")))
@protocol SSDKKotlinx_coroutines_coreJob <SSDKKotlinCoroutineContextElement>
@required
- (id<SSDKKotlinx_coroutines_coreChildHandle>)attachChildChild:(id<SSDKKotlinx_coroutines_coreChildJob>)child __attribute__((swift_name("attachChild(child:)")));
- (void)cancelCause_:(SSDKKotlinCancellationException * _Nullable)cause __attribute__((swift_name("cancel(cause_:)")));
- (SSDKKotlinCancellationException *)getCancellationException __attribute__((swift_name("getCancellationException()")));
- (id<SSDKKotlinx_coroutines_coreDisposableHandle>)invokeOnCompletionOnCancelling:(BOOL)onCancelling invokeImmediately:(BOOL)invokeImmediately handler:(void (^)(SSDKKotlinThrowable * _Nullable))handler __attribute__((swift_name("invokeOnCompletion(onCancelling:invokeImmediately:handler:)")));
- (id<SSDKKotlinx_coroutines_coreDisposableHandle>)invokeOnCompletionHandler:(void (^)(SSDKKotlinThrowable * _Nullable))handler __attribute__((swift_name("invokeOnCompletion(handler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)joinWithCompletionHandler:(void (^)(SSDKKotlinUnit * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("join(completionHandler:)")));
- (id<SSDKKotlinx_coroutines_coreJob>)plusOther_:(id<SSDKKotlinx_coroutines_coreJob>)other __attribute__((swift_name("plus(other_:)"))) __attribute__((unavailable("Operator '+' on two Job objects is meaningless. Job is a coroutine context element and `+` is a set-sum operator for coroutine contexts. The job to the right of `+` just replaces the job the left of `+`.")));
- (BOOL)start __attribute__((swift_name("start()")));
@property (readonly) id<SSDKKotlinSequence> children __attribute__((swift_name("children")));
@property (readonly) BOOL isActive __attribute__((swift_name("isActive")));
@property (readonly) BOOL isCancelled __attribute__((swift_name("isCancelled")));
@property (readonly) BOOL isCompleted __attribute__((swift_name("isCompleted")));
@property (readonly) id<SSDKKotlinx_coroutines_coreSelectClause0> onJoin __attribute__((swift_name("onJoin")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_coreProxyConfig")))
@interface SSDKKtor_client_coreProxyConfig : SSDKBase
- (instancetype)initWithUrl:(SSDKKtor_httpUrl *)url __attribute__((swift_name("init(url:)"))) __attribute__((objc_designated_initializer));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) SSDKKtor_httpUrl *url __attribute__((swift_name("url")));
@end;

__attribute__((swift_name("KotlinCoroutineContextKey")))
@protocol SSDKKotlinCoroutineContextKey
@required
@end;

__attribute__((swift_name("KotlinContinuation")))
@protocol SSDKKotlinContinuation
@required
- (void)resumeWithResult:(id _Nullable)result __attribute__((swift_name("resumeWith(result:)")));
@property (readonly) id<SSDKKotlinCoroutineContext> context __attribute__((swift_name("context")));
@end;

__attribute__((swift_name("KotlinAbstractCoroutineContextKey")))
@interface SSDKKotlinAbstractCoroutineContextKey<B, E> : SSDKBase <SSDKKotlinCoroutineContextKey>
- (instancetype)initWithBaseKey:(id<SSDKKotlinCoroutineContextKey>)baseKey safeCast:(E _Nullable (^)(id<SSDKKotlinCoroutineContextElement>))safeCast __attribute__((swift_name("init(baseKey:safeCast:)"))) __attribute__((objc_designated_initializer));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Kotlinx_coroutines_coreCoroutineDispatcher.Key")))
@interface SSDKKotlinx_coroutines_coreCoroutineDispatcherKey : SSDKKotlinAbstractCoroutineContextKey<id<SSDKKotlinContinuationInterceptor>, SSDKKotlinx_coroutines_coreCoroutineDispatcher *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithBaseKey:(id<SSDKKotlinCoroutineContextKey>)baseKey safeCast:(id<SSDKKotlinCoroutineContextElement> _Nullable (^)(id<SSDKKotlinCoroutineContextElement>))safeCast __attribute__((swift_name("init(baseKey:safeCast:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)key __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SSDKKotlinx_coroutines_coreCoroutineDispatcherKey *shared __attribute__((swift_name("shared")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreRunnable")))
@protocol SSDKKotlinx_coroutines_coreRunnable
@required
- (void)run __attribute__((swift_name("run()")));
@end;

__attribute__((swift_name("KotlinMapEntry")))
@protocol SSDKKotlinMapEntry
@required
@property (readonly) id _Nullable key __attribute__((swift_name("key")));
@property (readonly) id _Nullable value __attribute__((swift_name("value")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_httpURLProtocol.Companion")))
@interface SSDKKtor_httpURLProtocolCompanion : SSDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SSDKKtor_httpURLProtocolCompanion *shared __attribute__((swift_name("shared")));
- (SSDKKtor_httpURLProtocol *)createOrDefaultName:(NSString *)name __attribute__((swift_name("createOrDefault(name:)")));
@property (readonly) SSDKKtor_httpURLProtocol *HTTP __attribute__((swift_name("HTTP")));
@property (readonly) SSDKKtor_httpURLProtocol *HTTPS __attribute__((swift_name("HTTPS")));
@property (readonly) SSDKKtor_httpURLProtocol *SOCKS __attribute__((swift_name("SOCKS")));
@property (readonly) SSDKKtor_httpURLProtocol *WS __attribute__((swift_name("WS")));
@property (readonly) SSDKKtor_httpURLProtocol *WSS __attribute__((swift_name("WSS")));
@property (readonly) NSDictionary<NSString *, SSDKKtor_httpURLProtocol *> *byName __attribute__((swift_name("byName")));
@end;

__attribute__((swift_name("Ktor_client_coreHttpClientPlugin")))
@protocol SSDKKtor_client_coreHttpClientPlugin
@required
- (void)installPlugin:(id)plugin scope:(SSDKKtor_client_coreHttpClient *)scope __attribute__((swift_name("install(plugin:scope:)")));
- (id)prepareBlock:(void (^)(id))block __attribute__((swift_name("prepare(block:)")));
@property (readonly) SSDKKtor_utilsAttributeKey<id> *key __attribute__((swift_name("key")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_utilsAttributeKey")))
@interface SSDKKtor_utilsAttributeKey<T> : SSDKBase
- (instancetype)initWithName:(NSString *)name __attribute__((swift_name("init(name:)"))) __attribute__((objc_designated_initializer));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@end;

__attribute__((swift_name("Ktor_eventsEventDefinition")))
@interface SSDKKtor_eventsEventDefinition<T> : SSDKBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreDisposableHandle")))
@protocol SSDKKotlinx_coroutines_coreDisposableHandle
@required
- (void)dispose __attribute__((swift_name("dispose()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_utilsPipelinePhase")))
@interface SSDKKtor_utilsPipelinePhase : SSDKBase
- (instancetype)initWithName:(NSString *)name __attribute__((swift_name("init(name:)"))) __attribute__((objc_designated_initializer));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@end;

__attribute__((swift_name("KotlinFunction")))
@protocol SSDKKotlinFunction
@required
@end;

__attribute__((swift_name("KotlinSuspendFunction2")))
@protocol SSDKKotlinSuspendFunction2 <SSDKKotlinFunction>
@required

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)invokeP1:(id _Nullable)p1 p2:(id _Nullable)p2 completionHandler:(void (^)(id _Nullable_result, NSError * _Nullable))completionHandler __attribute__((swift_name("invoke(p1:p2:completionHandler:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_coreHttpReceivePipeline.Phases")))
@interface SSDKKtor_client_coreHttpReceivePipelinePhases : SSDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)phases __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SSDKKtor_client_coreHttpReceivePipelinePhases *shared __attribute__((swift_name("shared")));
@property (readonly) SSDKKtor_utilsPipelinePhase *After __attribute__((swift_name("After")));
@property (readonly) SSDKKtor_utilsPipelinePhase *Before __attribute__((swift_name("Before")));
@property (readonly) SSDKKtor_utilsPipelinePhase *State __attribute__((swift_name("State")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_coreHttpRequestPipeline.Phases")))
@interface SSDKKtor_client_coreHttpRequestPipelinePhases : SSDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)phases __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SSDKKtor_client_coreHttpRequestPipelinePhases *shared __attribute__((swift_name("shared")));
@property (readonly) SSDKKtor_utilsPipelinePhase *Before __attribute__((swift_name("Before")));
@property (readonly) SSDKKtor_utilsPipelinePhase *Render __attribute__((swift_name("Render")));
@property (readonly) SSDKKtor_utilsPipelinePhase *Send __attribute__((swift_name("Send")));
@property (readonly) SSDKKtor_utilsPipelinePhase *State __attribute__((swift_name("State")));
@property (readonly) SSDKKtor_utilsPipelinePhase *Transform __attribute__((swift_name("Transform")));
@end;

__attribute__((swift_name("Ktor_httpHttpMessageBuilder")))
@protocol SSDKKtor_httpHttpMessageBuilder
@required
@property (readonly) SSDKKtor_httpHeadersBuilder *headers __attribute__((swift_name("headers")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_coreHttpRequestBuilder")))
@interface SSDKKtor_client_coreHttpRequestBuilder : SSDKBase <SSDKKtor_httpHttpMessageBuilder>
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@property (class, readonly, getter=companion) SSDKKtor_client_coreHttpRequestBuilderCompanion *companion __attribute__((swift_name("companion")));
- (SSDKKtor_client_coreHttpRequestData *)build __attribute__((swift_name("build()")));
- (id _Nullable)getCapabilityOrNullKey:(id<SSDKKtor_client_coreHttpClientEngineCapability>)key __attribute__((swift_name("getCapabilityOrNull(key:)")));
- (void)setAttributesBlock:(void (^)(id<SSDKKtor_utilsAttributes>))block __attribute__((swift_name("setAttributes(block:)")));
- (void)setCapabilityKey:(id<SSDKKtor_client_coreHttpClientEngineCapability>)key capability:(id)capability __attribute__((swift_name("setCapability(key:capability:)")));
- (SSDKKtor_client_coreHttpRequestBuilder *)takeFromBuilder:(SSDKKtor_client_coreHttpRequestBuilder *)builder __attribute__((swift_name("takeFrom(builder:)")));
- (SSDKKtor_client_coreHttpRequestBuilder *)takeFromWithExecutionContextBuilder:(SSDKKtor_client_coreHttpRequestBuilder *)builder __attribute__((swift_name("takeFromWithExecutionContext(builder:)")));
- (void)urlBlock:(void (^)(SSDKKtor_httpURLBuilder *, SSDKKtor_httpURLBuilder *))block __attribute__((swift_name("url(block:)")));
@property (readonly) id<SSDKKtor_utilsAttributes> attributes __attribute__((swift_name("attributes")));
@property id body __attribute__((swift_name("body")));
@property SSDKKtor_utilsTypeInfo * _Nullable bodyType __attribute__((swift_name("bodyType")));
@property (readonly) id<SSDKKotlinx_coroutines_coreJob> executionContext __attribute__((swift_name("executionContext")));
@property (readonly) SSDKKtor_httpHeadersBuilder *headers __attribute__((swift_name("headers")));
@property SSDKKtor_httpHttpMethod *method __attribute__((swift_name("method")));
@property (readonly) SSDKKtor_httpURLBuilder *url __attribute__((swift_name("url")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_coreHttpResponsePipeline.Phases")))
@interface SSDKKtor_client_coreHttpResponsePipelinePhases : SSDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)phases __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SSDKKtor_client_coreHttpResponsePipelinePhases *shared __attribute__((swift_name("shared")));
@property (readonly) SSDKKtor_utilsPipelinePhase *After __attribute__((swift_name("After")));
@property (readonly) SSDKKtor_utilsPipelinePhase *Parse __attribute__((swift_name("Parse")));
@property (readonly) SSDKKtor_utilsPipelinePhase *Receive __attribute__((swift_name("Receive")));
@property (readonly) SSDKKtor_utilsPipelinePhase *State __attribute__((swift_name("State")));
@property (readonly) SSDKKtor_utilsPipelinePhase *Transform __attribute__((swift_name("Transform")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_coreHttpResponseContainer")))
@interface SSDKKtor_client_coreHttpResponseContainer : SSDKBase
- (instancetype)initWithExpectedType:(SSDKKtor_utilsTypeInfo *)expectedType response:(id)response __attribute__((swift_name("init(expectedType:response:)"))) __attribute__((objc_designated_initializer));
- (SSDKKtor_utilsTypeInfo *)component1 __attribute__((swift_name("component1()")));
- (id)component2 __attribute__((swift_name("component2()")));
- (SSDKKtor_client_coreHttpResponseContainer *)doCopyExpectedType:(SSDKKtor_utilsTypeInfo *)expectedType response:(id)response __attribute__((swift_name("doCopy(expectedType:response:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) SSDKKtor_utilsTypeInfo *expectedType __attribute__((swift_name("expectedType")));
@property (readonly) id response __attribute__((swift_name("response")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_coreHttpSendPipeline.Phases")))
@interface SSDKKtor_client_coreHttpSendPipelinePhases : SSDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)phases __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SSDKKtor_client_coreHttpSendPipelinePhases *shared __attribute__((swift_name("shared")));
@property (readonly) SSDKKtor_utilsPipelinePhase *Before __attribute__((swift_name("Before")));
@property (readonly) SSDKKtor_utilsPipelinePhase *Engine __attribute__((swift_name("Engine")));
@property (readonly) SSDKKtor_utilsPipelinePhase *Monitoring __attribute__((swift_name("Monitoring")));
@property (readonly) SSDKKtor_utilsPipelinePhase *Receive __attribute__((swift_name("Receive")));
@property (readonly) SSDKKtor_utilsPipelinePhase *State __attribute__((swift_name("State")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_coreHttpClientCall.Companion")))
@interface SSDKKtor_client_coreHttpClientCallCompanion : SSDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SSDKKtor_client_coreHttpClientCallCompanion *shared __attribute__((swift_name("shared")));
@property (readonly) SSDKKtor_utilsAttributeKey<id> *CustomResponse __attribute__((swift_name("CustomResponse"))) __attribute__((unavailable("This is going to be removed. Please file a ticket with clarification why and what for do you need it.")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_utilsTypeInfo")))
@interface SSDKKtor_utilsTypeInfo : SSDKBase
- (instancetype)initWithType:(id<SSDKKotlinKClass>)type reifiedType:(id<SSDKKotlinKType>)reifiedType kotlinType:(id<SSDKKotlinKType> _Nullable)kotlinType __attribute__((swift_name("init(type:reifiedType:kotlinType:)"))) __attribute__((objc_designated_initializer));
- (id<SSDKKotlinKClass>)component1 __attribute__((swift_name("component1()")));
- (id<SSDKKotlinKType>)component2 __attribute__((swift_name("component2()")));
- (id<SSDKKotlinKType> _Nullable)component3 __attribute__((swift_name("component3()")));
- (SSDKKtor_utilsTypeInfo *)doCopyType:(id<SSDKKotlinKClass>)type reifiedType:(id<SSDKKotlinKType>)reifiedType kotlinType:(id<SSDKKotlinKType> _Nullable)kotlinType __attribute__((swift_name("doCopy(type:reifiedType:kotlinType:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) id<SSDKKotlinKType> _Nullable kotlinType __attribute__((swift_name("kotlinType")));
@property (readonly) id<SSDKKotlinKType> reifiedType __attribute__((swift_name("reifiedType")));
@property (readonly) id<SSDKKotlinKClass> type __attribute__((swift_name("type")));
@end;

__attribute__((swift_name("Ktor_client_coreHttpRequest")))
@protocol SSDKKtor_client_coreHttpRequest <SSDKKtor_httpHttpMessage, SSDKKotlinx_coroutines_coreCoroutineScope>
@required
@property (readonly) id<SSDKKtor_utilsAttributes> attributes __attribute__((swift_name("attributes")));
@property (readonly) SSDKKtor_client_coreHttpClientCall *call __attribute__((swift_name("call")));
@property (readonly) SSDKKtor_httpOutgoingContent *content __attribute__((swift_name("content")));
@property (readonly) SSDKKtor_httpHttpMethod *method __attribute__((swift_name("method")));
@property (readonly) SSDKKtor_httpUrl *url __attribute__((swift_name("url")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_ioMemory")))
@interface SSDKKtor_ioMemory : SSDKBase
- (instancetype)initWithPointer:(void *)pointer size:(int64_t)size __attribute__((swift_name("init(pointer:size:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) SSDKKtor_ioMemoryCompanion *companion __attribute__((swift_name("companion")));
- (void)doCopyToDestination:(SSDKKtor_ioMemory *)destination offset:(int32_t)offset length:(int32_t)length destinationOffset:(int32_t)destinationOffset __attribute__((swift_name("doCopyTo(destination:offset:length:destinationOffset:)")));
- (void)doCopyToDestination:(SSDKKtor_ioMemory *)destination offset:(int64_t)offset length:(int64_t)length destinationOffset_:(int64_t)destinationOffset __attribute__((swift_name("doCopyTo(destination:offset:length:destinationOffset_:)")));
- (int8_t)loadAtIndex:(int32_t)index __attribute__((swift_name("loadAt(index:)")));
- (int8_t)loadAtIndex_:(int64_t)index __attribute__((swift_name("loadAt(index_:)")));
- (SSDKKtor_ioMemory *)sliceOffset:(int32_t)offset length:(int32_t)length __attribute__((swift_name("slice(offset:length:)")));
- (SSDKKtor_ioMemory *)sliceOffset:(int64_t)offset length_:(int64_t)length __attribute__((swift_name("slice(offset:length_:)")));
- (void)storeAtIndex:(int32_t)index value:(int8_t)value __attribute__((swift_name("storeAt(index:value:)")));
- (void)storeAtIndex:(int64_t)index value_:(int8_t)value __attribute__((swift_name("storeAt(index:value_:)")));
@property (readonly) void *pointer __attribute__((swift_name("pointer")));
@property (readonly) int64_t size __attribute__((swift_name("size")));
@property (readonly) int32_t size32 __attribute__((swift_name("size32")));
@end;

__attribute__((swift_name("Ktor_ioBuffer")))
@interface SSDKKtor_ioBuffer : SSDKBase
- (instancetype)initWithMemory:(SSDKKtor_ioMemory *)memory __attribute__((swift_name("init(memory:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) SSDKKtor_ioBufferCompanion *companion __attribute__((swift_name("companion")));
- (void)commitWrittenCount:(int32_t)count __attribute__((swift_name("commitWritten(count:)")));
- (void)discardExactCount:(int32_t)count __attribute__((swift_name("discardExact(count:)")));
- (SSDKKtor_ioBuffer *)duplicate __attribute__((swift_name("duplicate()")));
- (void)duplicateToCopy:(SSDKKtor_ioBuffer *)copy __attribute__((swift_name("duplicateTo(copy:)")));
- (int8_t)readByte __attribute__((swift_name("readByte()")));
- (void)reserveEndGapEndGap:(int32_t)endGap __attribute__((swift_name("reserveEndGap(endGap:)")));
- (void)reserveStartGapStartGap:(int32_t)startGap __attribute__((swift_name("reserveStartGap(startGap:)")));
- (void)reset __attribute__((swift_name("reset()")));
- (void)resetForRead __attribute__((swift_name("resetForRead()")));
- (void)resetForWrite __attribute__((swift_name("resetForWrite()")));
- (void)resetForWriteLimit:(int32_t)limit __attribute__((swift_name("resetForWrite(limit:)")));
- (void)rewindCount:(int32_t)count __attribute__((swift_name("rewind(count:)")));
- (NSString *)description __attribute__((swift_name("description()")));
- (int32_t)tryPeekByte __attribute__((swift_name("tryPeekByte()")));
- (int32_t)tryReadByte __attribute__((swift_name("tryReadByte()")));
- (void)writeByteValue:(int8_t)value __attribute__((swift_name("writeByte(value:)")));
@property (readonly) int32_t capacity __attribute__((swift_name("capacity")));
@property (readonly) int32_t endGap __attribute__((swift_name("endGap")));
@property (readonly) int32_t limit __attribute__((swift_name("limit")));
@property (readonly) SSDKKtor_ioMemory *memory __attribute__((swift_name("memory")));
@property (readonly) int32_t readPosition __attribute__((swift_name("readPosition")));
@property (readonly) int32_t readRemaining __attribute__((swift_name("readRemaining")));
@property (readonly) int32_t startGap __attribute__((swift_name("startGap")));
@property (readonly) int32_t writePosition __attribute__((swift_name("writePosition")));
@property (readonly) int32_t writeRemaining __attribute__((swift_name("writeRemaining")));
@end;

__attribute__((swift_name("Ktor_ioChunkBuffer")))
@interface SSDKKtor_ioChunkBuffer : SSDKKtor_ioBuffer
- (instancetype)initWithMemory:(SSDKKtor_ioMemory *)memory origin:(SSDKKtor_ioChunkBuffer * _Nullable)origin parentPool:(id<SSDKKtor_ioObjectPool> _Nullable)parentPool __attribute__((swift_name("init(memory:origin:parentPool:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithMemory:(SSDKKtor_ioMemory *)memory __attribute__((swift_name("init(memory:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly, getter=companion) SSDKKtor_ioChunkBufferCompanion *companion __attribute__((swift_name("companion")));
- (SSDKKtor_ioChunkBuffer * _Nullable)cleanNext __attribute__((swift_name("cleanNext()")));
- (SSDKKtor_ioChunkBuffer *)duplicate __attribute__((swift_name("duplicate()")));
- (void)releasePool:(id<SSDKKtor_ioObjectPool>)pool __attribute__((swift_name("release(pool:)")));
- (void)reset __attribute__((swift_name("reset()")));
@property (getter=next_) SSDKKtor_ioChunkBuffer * _Nullable next __attribute__((swift_name("next")));
@property (readonly) SSDKKtor_ioChunkBuffer * _Nullable origin __attribute__((swift_name("origin")));
@property (readonly) int32_t referenceCount __attribute__((swift_name("referenceCount")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinByteArray")))
@interface SSDKKotlinByteArray : SSDKBase
+ (instancetype)arrayWithSize:(int32_t)size __attribute__((swift_name("init(size:)")));
+ (instancetype)arrayWithSize:(int32_t)size init:(SSDKByte *(^)(SSDKInt *))init __attribute__((swift_name("init(size:init:)")));
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (int8_t)getIndex:(int32_t)index __attribute__((swift_name("get(index:)")));
- (SSDKKotlinByteIterator *)iterator __attribute__((swift_name("iterator()")));
- (void)setIndex:(int32_t)index value:(int8_t)value __attribute__((swift_name("set(index:value:)")));
@property (readonly) int32_t size __attribute__((swift_name("size")));
@end;

__attribute__((swift_name("Ktor_ioInput")))
@interface SSDKKtor_ioInput : SSDKBase <SSDKKtor_ioCloseable>
- (instancetype)initWithHead:(SSDKKtor_ioChunkBuffer *)head remaining:(int64_t)remaining pool:(id<SSDKKtor_ioObjectPool>)pool __attribute__((swift_name("init(head:remaining:pool:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) SSDKKtor_ioInputCompanion *companion __attribute__((swift_name("companion")));
- (BOOL)canRead __attribute__((swift_name("canRead()")));
- (void)close __attribute__((swift_name("close()")));
- (void)closeSource __attribute__((swift_name("closeSource()")));
- (int32_t)discardN:(int32_t)n __attribute__((swift_name("discard(n:)")));
- (int64_t)discardN_:(int64_t)n __attribute__((swift_name("discard(n_:)")));
- (void)discardExactN:(int32_t)n __attribute__((swift_name("discardExact(n:)")));
- (SSDKKtor_ioChunkBuffer * _Nullable)fill __attribute__((swift_name("fill()")));
- (int32_t)fillDestination:(SSDKKtor_ioMemory *)destination offset:(int32_t)offset length:(int32_t)length __attribute__((swift_name("fill(destination:offset:length:)")));
- (BOOL)hasBytesN:(int32_t)n __attribute__((swift_name("hasBytes(n:)")));
- (void)markNoMoreChunksAvailable __attribute__((swift_name("markNoMoreChunksAvailable()")));
- (int64_t)peekToDestination:(SSDKKtor_ioMemory *)destination destinationOffset:(int64_t)destinationOffset offset:(int64_t)offset min:(int64_t)min max:(int64_t)max __attribute__((swift_name("peekTo(destination:destinationOffset:offset:min:max:)")));
- (int32_t)peekToBuffer:(SSDKKtor_ioChunkBuffer *)buffer __attribute__((swift_name("peekTo(buffer:)")));
- (int8_t)readByte __attribute__((swift_name("readByte()")));
- (NSString *)readTextMin:(int32_t)min max:(int32_t)max __attribute__((swift_name("readText(min:max:)")));
- (int32_t)readTextOut:(id<SSDKKotlinAppendable>)out min:(int32_t)min max:(int32_t)max __attribute__((swift_name("readText(out:min:max:)")));
- (NSString *)readTextExactExactCharacters:(int32_t)exactCharacters __attribute__((swift_name("readTextExact(exactCharacters:)")));
- (void)readTextExactOut:(id<SSDKKotlinAppendable>)out exactCharacters:(int32_t)exactCharacters __attribute__((swift_name("readTextExact(out:exactCharacters:)")));
- (void)release_ __attribute__((swift_name("release()")));
- (int32_t)tryPeek __attribute__((swift_name("tryPeek()")));
@property (readonly) BOOL endOfInput __attribute__((swift_name("endOfInput")));
@property (readonly) id<SSDKKtor_ioObjectPool> pool __attribute__((swift_name("pool")));
@property (readonly) int64_t remaining __attribute__((swift_name("remaining")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_ioByteReadPacket")))
@interface SSDKKtor_ioByteReadPacket : SSDKKtor_ioInput
- (instancetype)initWithHead:(SSDKKtor_ioChunkBuffer *)head pool:(id<SSDKKtor_ioObjectPool>)pool __attribute__((swift_name("init(head:pool:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithHead:(SSDKKtor_ioChunkBuffer *)head remaining:(int64_t)remaining pool:(id<SSDKKtor_ioObjectPool>)pool __attribute__((swift_name("init(head:remaining:pool:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly, getter=companion) SSDKKtor_ioByteReadPacketCompanion *companion __attribute__((swift_name("companion")));
- (void)closeSource __attribute__((swift_name("closeSource()")));
- (SSDKKtor_ioByteReadPacket *)doCopy __attribute__((swift_name("doCopy()")));
- (SSDKKtor_ioChunkBuffer * _Nullable)fill __attribute__((swift_name("fill()")));
- (int32_t)fillDestination:(SSDKKtor_ioMemory *)destination offset:(int32_t)offset length:(int32_t)length __attribute__((swift_name("fill(destination:offset:length:)")));
- (NSString *)description __attribute__((swift_name("description()")));
@end;

__attribute__((swift_name("Ktor_ioReadSession")))
@protocol SSDKKtor_ioReadSession
@required
- (int32_t)discardN:(int32_t)n __attribute__((swift_name("discard(n:)")));
- (SSDKKtor_ioChunkBuffer * _Nullable)requestAtLeast:(int32_t)atLeast __attribute__((swift_name("request(atLeast:)")));
@property (readonly) int32_t availableForRead __attribute__((swift_name("availableForRead")));
@end;

__attribute__((swift_name("KotlinSuspendFunction1")))
@protocol SSDKKotlinSuspendFunction1 <SSDKKotlinFunction>
@required

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)invokeP1:(id _Nullable)p1 completionHandler:(void (^)(id _Nullable_result, NSError * _Nullable))completionHandler __attribute__((swift_name("invoke(p1:completionHandler:)")));
@end;

__attribute__((swift_name("KotlinAppendable")))
@protocol SSDKKotlinAppendable
@required
- (id<SSDKKotlinAppendable>)appendValue:(unichar)value __attribute__((swift_name("append(value:)")));
- (id<SSDKKotlinAppendable>)appendValue_:(id _Nullable)value __attribute__((swift_name("append(value_:)")));
- (id<SSDKKotlinAppendable>)appendValue:(id _Nullable)value startIndex:(int32_t)startIndex endIndex:(int32_t)endIndex __attribute__((swift_name("append(value:startIndex:endIndex:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_utilsGMTDate.Companion")))
@interface SSDKKtor_utilsGMTDateCompanion : SSDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SSDKKtor_utilsGMTDateCompanion *shared __attribute__((swift_name("shared")));
@property (readonly) SSDKKtor_utilsGMTDate *START __attribute__((swift_name("START")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_utilsWeekDay")))
@interface SSDKKtor_utilsWeekDay : SSDKKotlinEnum<SSDKKtor_utilsWeekDay *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly, getter=companion) SSDKKtor_utilsWeekDayCompanion *companion __attribute__((swift_name("companion")));
@property (class, readonly) SSDKKtor_utilsWeekDay *monday __attribute__((swift_name("monday")));
@property (class, readonly) SSDKKtor_utilsWeekDay *tuesday __attribute__((swift_name("tuesday")));
@property (class, readonly) SSDKKtor_utilsWeekDay *wednesday __attribute__((swift_name("wednesday")));
@property (class, readonly) SSDKKtor_utilsWeekDay *thursday __attribute__((swift_name("thursday")));
@property (class, readonly) SSDKKtor_utilsWeekDay *friday __attribute__((swift_name("friday")));
@property (class, readonly) SSDKKtor_utilsWeekDay *saturday __attribute__((swift_name("saturday")));
@property (class, readonly) SSDKKtor_utilsWeekDay *sunday __attribute__((swift_name("sunday")));
+ (SSDKKotlinArray<SSDKKtor_utilsWeekDay *> *)values __attribute__((swift_name("values()")));
@property (readonly) NSString *value __attribute__((swift_name("value")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_utilsMonth")))
@interface SSDKKtor_utilsMonth : SSDKKotlinEnum<SSDKKtor_utilsMonth *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly, getter=companion) SSDKKtor_utilsMonthCompanion *companion __attribute__((swift_name("companion")));
@property (class, readonly) SSDKKtor_utilsMonth *january __attribute__((swift_name("january")));
@property (class, readonly) SSDKKtor_utilsMonth *february __attribute__((swift_name("february")));
@property (class, readonly) SSDKKtor_utilsMonth *march __attribute__((swift_name("march")));
@property (class, readonly) SSDKKtor_utilsMonth *april __attribute__((swift_name("april")));
@property (class, readonly) SSDKKtor_utilsMonth *may __attribute__((swift_name("may")));
@property (class, readonly) SSDKKtor_utilsMonth *june __attribute__((swift_name("june")));
@property (class, readonly) SSDKKtor_utilsMonth *july __attribute__((swift_name("july")));
@property (class, readonly) SSDKKtor_utilsMonth *august __attribute__((swift_name("august")));
@property (class, readonly) SSDKKtor_utilsMonth *september __attribute__((swift_name("september")));
@property (class, readonly) SSDKKtor_utilsMonth *october __attribute__((swift_name("october")));
@property (class, readonly) SSDKKtor_utilsMonth *november __attribute__((swift_name("november")));
@property (class, readonly) SSDKKtor_utilsMonth *december __attribute__((swift_name("december")));
+ (SSDKKotlinArray<SSDKKtor_utilsMonth *> *)values __attribute__((swift_name("values()")));
@property (readonly) NSString *value __attribute__((swift_name("value")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_httpHttpStatusCode.Companion")))
@interface SSDKKtor_httpHttpStatusCodeCompanion : SSDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SSDKKtor_httpHttpStatusCodeCompanion *shared __attribute__((swift_name("shared")));
- (SSDKKtor_httpHttpStatusCode *)fromValueValue:(int32_t)value __attribute__((swift_name("fromValue(value:)")));
@property (readonly) SSDKKtor_httpHttpStatusCode *Accepted __attribute__((swift_name("Accepted")));
@property (readonly) SSDKKtor_httpHttpStatusCode *BadGateway __attribute__((swift_name("BadGateway")));
@property (readonly) SSDKKtor_httpHttpStatusCode *BadRequest __attribute__((swift_name("BadRequest")));
@property (readonly) SSDKKtor_httpHttpStatusCode *Conflict __attribute__((swift_name("Conflict")));
@property (readonly) SSDKKtor_httpHttpStatusCode *Continue __attribute__((swift_name("Continue")));
@property (readonly) SSDKKtor_httpHttpStatusCode *Created __attribute__((swift_name("Created")));
@property (readonly) SSDKKtor_httpHttpStatusCode *ExpectationFailed __attribute__((swift_name("ExpectationFailed")));
@property (readonly) SSDKKtor_httpHttpStatusCode *FailedDependency __attribute__((swift_name("FailedDependency")));
@property (readonly) SSDKKtor_httpHttpStatusCode *Forbidden __attribute__((swift_name("Forbidden")));
@property (readonly) SSDKKtor_httpHttpStatusCode *Found __attribute__((swift_name("Found")));
@property (readonly) SSDKKtor_httpHttpStatusCode *GatewayTimeout __attribute__((swift_name("GatewayTimeout")));
@property (readonly) SSDKKtor_httpHttpStatusCode *Gone __attribute__((swift_name("Gone")));
@property (readonly) SSDKKtor_httpHttpStatusCode *InsufficientStorage __attribute__((swift_name("InsufficientStorage")));
@property (readonly) SSDKKtor_httpHttpStatusCode *InternalServerError __attribute__((swift_name("InternalServerError")));
@property (readonly) SSDKKtor_httpHttpStatusCode *LengthRequired __attribute__((swift_name("LengthRequired")));
@property (readonly) SSDKKtor_httpHttpStatusCode *Locked __attribute__((swift_name("Locked")));
@property (readonly) SSDKKtor_httpHttpStatusCode *MethodNotAllowed __attribute__((swift_name("MethodNotAllowed")));
@property (readonly) SSDKKtor_httpHttpStatusCode *MovedPermanently __attribute__((swift_name("MovedPermanently")));
@property (readonly) SSDKKtor_httpHttpStatusCode *MultiStatus __attribute__((swift_name("MultiStatus")));
@property (readonly) SSDKKtor_httpHttpStatusCode *MultipleChoices __attribute__((swift_name("MultipleChoices")));
@property (readonly) SSDKKtor_httpHttpStatusCode *NoContent __attribute__((swift_name("NoContent")));
@property (readonly) SSDKKtor_httpHttpStatusCode *NonAuthoritativeInformation __attribute__((swift_name("NonAuthoritativeInformation")));
@property (readonly) SSDKKtor_httpHttpStatusCode *NotAcceptable __attribute__((swift_name("NotAcceptable")));
@property (readonly) SSDKKtor_httpHttpStatusCode *NotFound __attribute__((swift_name("NotFound")));
@property (readonly) SSDKKtor_httpHttpStatusCode *NotImplemented __attribute__((swift_name("NotImplemented")));
@property (readonly) SSDKKtor_httpHttpStatusCode *NotModified __attribute__((swift_name("NotModified")));
@property (readonly) SSDKKtor_httpHttpStatusCode *OK __attribute__((swift_name("OK")));
@property (readonly) SSDKKtor_httpHttpStatusCode *PartialContent __attribute__((swift_name("PartialContent")));
@property (readonly) SSDKKtor_httpHttpStatusCode *PayloadTooLarge __attribute__((swift_name("PayloadTooLarge")));
@property (readonly) SSDKKtor_httpHttpStatusCode *PaymentRequired __attribute__((swift_name("PaymentRequired")));
@property (readonly) SSDKKtor_httpHttpStatusCode *PermanentRedirect __attribute__((swift_name("PermanentRedirect")));
@property (readonly) SSDKKtor_httpHttpStatusCode *PreconditionFailed __attribute__((swift_name("PreconditionFailed")));
@property (readonly) SSDKKtor_httpHttpStatusCode *Processing __attribute__((swift_name("Processing")));
@property (readonly) SSDKKtor_httpHttpStatusCode *ProxyAuthenticationRequired __attribute__((swift_name("ProxyAuthenticationRequired")));
@property (readonly) SSDKKtor_httpHttpStatusCode *RequestHeaderFieldTooLarge __attribute__((swift_name("RequestHeaderFieldTooLarge")));
@property (readonly) SSDKKtor_httpHttpStatusCode *RequestTimeout __attribute__((swift_name("RequestTimeout")));
@property (readonly) SSDKKtor_httpHttpStatusCode *RequestURITooLong __attribute__((swift_name("RequestURITooLong")));
@property (readonly) SSDKKtor_httpHttpStatusCode *RequestedRangeNotSatisfiable __attribute__((swift_name("RequestedRangeNotSatisfiable")));
@property (readonly) SSDKKtor_httpHttpStatusCode *ResetContent __attribute__((swift_name("ResetContent")));
@property (readonly) SSDKKtor_httpHttpStatusCode *SeeOther __attribute__((swift_name("SeeOther")));
@property (readonly) SSDKKtor_httpHttpStatusCode *ServiceUnavailable __attribute__((swift_name("ServiceUnavailable")));
@property (readonly) SSDKKtor_httpHttpStatusCode *SwitchProxy __attribute__((swift_name("SwitchProxy")));
@property (readonly) SSDKKtor_httpHttpStatusCode *SwitchingProtocols __attribute__((swift_name("SwitchingProtocols")));
@property (readonly) SSDKKtor_httpHttpStatusCode *TemporaryRedirect __attribute__((swift_name("TemporaryRedirect")));
@property (readonly) SSDKKtor_httpHttpStatusCode *TooManyRequests __attribute__((swift_name("TooManyRequests")));
@property (readonly) SSDKKtor_httpHttpStatusCode *Unauthorized __attribute__((swift_name("Unauthorized")));
@property (readonly) SSDKKtor_httpHttpStatusCode *UnprocessableEntity __attribute__((swift_name("UnprocessableEntity")));
@property (readonly) SSDKKtor_httpHttpStatusCode *UnsupportedMediaType __attribute__((swift_name("UnsupportedMediaType")));
@property (readonly) SSDKKtor_httpHttpStatusCode *UpgradeRequired __attribute__((swift_name("UpgradeRequired")));
@property (readonly) SSDKKtor_httpHttpStatusCode *UseProxy __attribute__((swift_name("UseProxy")));
@property (readonly) SSDKKtor_httpHttpStatusCode *VariantAlsoNegotiates __attribute__((swift_name("VariantAlsoNegotiates")));
@property (readonly) SSDKKtor_httpHttpStatusCode *VersionNotSupported __attribute__((swift_name("VersionNotSupported")));
@property (readonly) NSArray<SSDKKtor_httpHttpStatusCode *> *allStatusCodes __attribute__((swift_name("allStatusCodes")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_httpHttpProtocolVersion.Companion")))
@interface SSDKKtor_httpHttpProtocolVersionCompanion : SSDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SSDKKtor_httpHttpProtocolVersionCompanion *shared __attribute__((swift_name("shared")));
- (SSDKKtor_httpHttpProtocolVersion *)fromValueName:(NSString *)name major:(int32_t)major minor:(int32_t)minor __attribute__((swift_name("fromValue(name:major:minor:)")));
- (SSDKKtor_httpHttpProtocolVersion *)parseValue:(id)value __attribute__((swift_name("parse(value:)")));
@property (readonly) SSDKKtor_httpHttpProtocolVersion *HTTP_1_0 __attribute__((swift_name("HTTP_1_0")));
@property (readonly) SSDKKtor_httpHttpProtocolVersion *HTTP_1_1 __attribute__((swift_name("HTTP_1_1")));
@property (readonly) SSDKKtor_httpHttpProtocolVersion *HTTP_2_0 __attribute__((swift_name("HTTP_2_0")));
@property (readonly) SSDKKtor_httpHttpProtocolVersion *QUIC __attribute__((swift_name("QUIC")));
@property (readonly) SSDKKtor_httpHttpProtocolVersion *SPDY_3 __attribute__((swift_name("SPDY_3")));
@end;

__attribute__((swift_name("Kotlinx_serialization_coreSerializersModuleCollector")))
@protocol SSDKKotlinx_serialization_coreSerializersModuleCollector
@required
- (void)contextualKClass:(id<SSDKKotlinKClass>)kClass provider:(id<SSDKKotlinx_serialization_coreKSerializer> (^)(NSArray<id<SSDKKotlinx_serialization_coreKSerializer>> *))provider __attribute__((swift_name("contextual(kClass:provider:)")));
- (void)contextualKClass:(id<SSDKKotlinKClass>)kClass serializer:(id<SSDKKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("contextual(kClass:serializer:)")));
- (void)polymorphicBaseClass:(id<SSDKKotlinKClass>)baseClass actualClass:(id<SSDKKotlinKClass>)actualClass actualSerializer:(id<SSDKKotlinx_serialization_coreKSerializer>)actualSerializer __attribute__((swift_name("polymorphic(baseClass:actualClass:actualSerializer:)")));
- (void)polymorphicDefaultBaseClass:(id<SSDKKotlinKClass>)baseClass defaultDeserializerProvider:(id<SSDKKotlinx_serialization_coreDeserializationStrategy> _Nullable (^)(NSString * _Nullable))defaultDeserializerProvider __attribute__((swift_name("polymorphicDefault(baseClass:defaultDeserializerProvider:)")));
- (void)polymorphicDefaultDeserializerBaseClass:(id<SSDKKotlinKClass>)baseClass defaultDeserializerProvider:(id<SSDKKotlinx_serialization_coreDeserializationStrategy> _Nullable (^)(NSString * _Nullable))defaultDeserializerProvider __attribute__((swift_name("polymorphicDefaultDeserializer(baseClass:defaultDeserializerProvider:)")));
- (void)polymorphicDefaultSerializerBaseClass:(id<SSDKKotlinKClass>)baseClass defaultSerializerProvider:(id<SSDKKotlinx_serialization_coreSerializationStrategy> _Nullable (^)(id))defaultSerializerProvider __attribute__((swift_name("polymorphicDefaultSerializer(baseClass:defaultSerializerProvider:)")));
@end;

__attribute__((swift_name("KotlinKDeclarationContainer")))
@protocol SSDKKotlinKDeclarationContainer
@required
@end;

__attribute__((swift_name("KotlinKAnnotatedElement")))
@protocol SSDKKotlinKAnnotatedElement
@required
@end;

__attribute__((swift_name("KotlinKClassifier")))
@protocol SSDKKotlinKClassifier
@required
@end;

__attribute__((swift_name("KotlinKClass")))
@protocol SSDKKotlinKClass <SSDKKotlinKDeclarationContainer, SSDKKotlinKAnnotatedElement, SSDKKotlinKClassifier>
@required
- (BOOL)isInstanceValue:(id _Nullable)value __attribute__((swift_name("isInstance(value:)")));
@property (readonly) NSString * _Nullable qualifiedName __attribute__((swift_name("qualifiedName")));
@property (readonly) NSString * _Nullable simpleName __attribute__((swift_name("simpleName")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_httpHttpMethod.Companion")))
@interface SSDKKtor_httpHttpMethodCompanion : SSDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SSDKKtor_httpHttpMethodCompanion *shared __attribute__((swift_name("shared")));
- (SSDKKtor_httpHttpMethod *)parseMethod:(NSString *)method __attribute__((swift_name("parse(method:)")));
@property (readonly) NSArray<SSDKKtor_httpHttpMethod *> *DefaultMethods __attribute__((swift_name("DefaultMethods")));
@property (readonly) SSDKKtor_httpHttpMethod *Delete __attribute__((swift_name("Delete")));
@property (readonly) SSDKKtor_httpHttpMethod *Get __attribute__((swift_name("Get")));
@property (readonly) SSDKKtor_httpHttpMethod *Head __attribute__((swift_name("Head")));
@property (readonly) SSDKKtor_httpHttpMethod *Options __attribute__((swift_name("Options")));
@property (readonly) SSDKKtor_httpHttpMethod *Patch __attribute__((swift_name("Patch")));
@property (readonly) SSDKKtor_httpHttpMethod *Post __attribute__((swift_name("Post")));
@property (readonly) SSDKKtor_httpHttpMethod *Put __attribute__((swift_name("Put")));
@end;

__attribute__((swift_name("Ktor_httpHeaderValueWithParameters")))
@interface SSDKKtor_httpHeaderValueWithParameters : SSDKBase
- (instancetype)initWithContent:(NSString *)content parameters:(NSArray<SSDKKtor_httpHeaderValueParam *> *)parameters __attribute__((swift_name("init(content:parameters:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) SSDKKtor_httpHeaderValueWithParametersCompanion *companion __attribute__((swift_name("companion")));
- (NSString * _Nullable)parameterName:(NSString *)name __attribute__((swift_name("parameter(name:)")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *content __attribute__((swift_name("content")));
@property (readonly) NSArray<SSDKKtor_httpHeaderValueParam *> *parameters __attribute__((swift_name("parameters")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_httpContentType")))
@interface SSDKKtor_httpContentType : SSDKKtor_httpHeaderValueWithParameters
- (instancetype)initWithContentType:(NSString *)contentType contentSubtype:(NSString *)contentSubtype parameters:(NSArray<SSDKKtor_httpHeaderValueParam *> *)parameters __attribute__((swift_name("init(contentType:contentSubtype:parameters:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithContent:(NSString *)content parameters:(NSArray<SSDKKtor_httpHeaderValueParam *> *)parameters __attribute__((swift_name("init(content:parameters:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly, getter=companion) SSDKKtor_httpContentTypeCompanion *companion __attribute__((swift_name("companion")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (BOOL)matchPattern:(SSDKKtor_httpContentType *)pattern __attribute__((swift_name("match(pattern:)")));
- (BOOL)matchPattern_:(NSString *)pattern __attribute__((swift_name("match(pattern_:)")));
- (SSDKKtor_httpContentType *)withParameterName:(NSString *)name value:(NSString *)value __attribute__((swift_name("withParameter(name:value:)")));
- (SSDKKtor_httpContentType *)withoutParameters __attribute__((swift_name("withoutParameters()")));
@property (readonly) NSString *contentSubtype __attribute__((swift_name("contentSubtype")));
@property (readonly) NSString *contentType __attribute__((swift_name("contentType")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreChildHandle")))
@protocol SSDKKotlinx_coroutines_coreChildHandle <SSDKKotlinx_coroutines_coreDisposableHandle>
@required
- (BOOL)childCancelledCause:(SSDKKotlinThrowable *)cause __attribute__((swift_name("childCancelled(cause:)")));
@property (readonly) id<SSDKKotlinx_coroutines_coreJob> _Nullable parent __attribute__((swift_name("parent")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreChildJob")))
@protocol SSDKKotlinx_coroutines_coreChildJob <SSDKKotlinx_coroutines_coreJob>
@required
- (void)parentCancelledParentJob:(id<SSDKKotlinx_coroutines_coreParentJob>)parentJob __attribute__((swift_name("parentCancelled(parentJob:)")));
@end;

__attribute__((swift_name("KotlinSequence")))
@protocol SSDKKotlinSequence
@required
- (id<SSDKKotlinIterator>)iterator __attribute__((swift_name("iterator()")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreSelectClause0")))
@protocol SSDKKotlinx_coroutines_coreSelectClause0
@required
- (void)registerSelectClause0Select:(id<SSDKKotlinx_coroutines_coreSelectInstance>)select block:(id<SSDKKotlinSuspendFunction0>)block __attribute__((swift_name("registerSelectClause0(select:block:)")));
@end;

__attribute__((swift_name("Ktor_utilsStringValuesBuilder")))
@protocol SSDKKtor_utilsStringValuesBuilder
@required
- (void)appendName:(NSString *)name value:(NSString *)value __attribute__((swift_name("append(name:value:)")));
- (void)appendAllStringValues:(id<SSDKKtor_utilsStringValues>)stringValues __attribute__((swift_name("appendAll(stringValues:)")));
- (void)appendAllName:(NSString *)name values:(id)values __attribute__((swift_name("appendAll(name:values:)")));
- (void)appendMissingStringValues:(id<SSDKKtor_utilsStringValues>)stringValues __attribute__((swift_name("appendMissing(stringValues:)")));
- (void)appendMissingName:(NSString *)name values:(id)values __attribute__((swift_name("appendMissing(name:values:)")));
- (id<SSDKKtor_utilsStringValues>)build __attribute__((swift_name("build()")));
- (void)clear __attribute__((swift_name("clear()")));
- (BOOL)containsName:(NSString *)name __attribute__((swift_name("contains(name:)")));
- (BOOL)containsName:(NSString *)name value:(NSString *)value __attribute__((swift_name("contains(name:value:)")));
- (NSSet<id<SSDKKotlinMapEntry>> *)entries __attribute__((swift_name("entries()")));
- (NSString * _Nullable)getName:(NSString *)name __attribute__((swift_name("get(name:)")));
- (NSArray<NSString *> * _Nullable)getAllName:(NSString *)name __attribute__((swift_name("getAll(name:)")));
- (BOOL)isEmpty __attribute__((swift_name("isEmpty()")));
- (NSSet<NSString *> *)names __attribute__((swift_name("names()")));
- (void)removeName:(NSString *)name __attribute__((swift_name("remove(name:)")));
- (BOOL)removeName:(NSString *)name value:(NSString *)value __attribute__((swift_name("remove(name:value:)")));
- (void)removeKeysWithNoEntries __attribute__((swift_name("removeKeysWithNoEntries()")));
- (void)setName:(NSString *)name value:(NSString *)value __attribute__((swift_name("set(name:value:)")));
@property (readonly) BOOL caseInsensitiveName __attribute__((swift_name("caseInsensitiveName")));
@end;

__attribute__((swift_name("Ktor_utilsStringValuesBuilderImpl")))
@interface SSDKKtor_utilsStringValuesBuilderImpl : SSDKBase <SSDKKtor_utilsStringValuesBuilder>
- (instancetype)initWithCaseInsensitiveName:(BOOL)caseInsensitiveName size:(int32_t)size __attribute__((swift_name("init(caseInsensitiveName:size:)"))) __attribute__((objc_designated_initializer));
- (void)appendName:(NSString *)name value:(NSString *)value __attribute__((swift_name("append(name:value:)")));
- (void)appendAllStringValues:(id<SSDKKtor_utilsStringValues>)stringValues __attribute__((swift_name("appendAll(stringValues:)")));
- (void)appendAllName:(NSString *)name values:(id)values __attribute__((swift_name("appendAll(name:values:)")));
- (void)appendMissingStringValues:(id<SSDKKtor_utilsStringValues>)stringValues __attribute__((swift_name("appendMissing(stringValues:)")));
- (void)appendMissingName:(NSString *)name values:(id)values __attribute__((swift_name("appendMissing(name:values:)")));
- (id<SSDKKtor_utilsStringValues>)build __attribute__((swift_name("build()")));
- (void)clear __attribute__((swift_name("clear()")));
- (BOOL)containsName:(NSString *)name __attribute__((swift_name("contains(name:)")));
- (BOOL)containsName:(NSString *)name value:(NSString *)value __attribute__((swift_name("contains(name:value:)")));
- (NSSet<id<SSDKKotlinMapEntry>> *)entries __attribute__((swift_name("entries()")));
- (NSString * _Nullable)getName:(NSString *)name __attribute__((swift_name("get(name:)")));
- (NSArray<NSString *> * _Nullable)getAllName:(NSString *)name __attribute__((swift_name("getAll(name:)")));
- (BOOL)isEmpty __attribute__((swift_name("isEmpty()")));
- (NSSet<NSString *> *)names __attribute__((swift_name("names()")));
- (void)removeName:(NSString *)name __attribute__((swift_name("remove(name:)")));
- (BOOL)removeName:(NSString *)name value:(NSString *)value __attribute__((swift_name("remove(name:value:)")));
- (void)removeKeysWithNoEntries __attribute__((swift_name("removeKeysWithNoEntries()")));
- (void)setName:(NSString *)name value:(NSString *)value __attribute__((swift_name("set(name:value:)")));
- (void)validateNameName:(NSString *)name __attribute__((swift_name("validateName(name:)")));
- (void)validateValueValue:(NSString *)value __attribute__((swift_name("validateValue(value:)")));
@property (readonly) BOOL caseInsensitiveName __attribute__((swift_name("caseInsensitiveName")));
@property (readonly) SSDKMutableDictionary<NSString *, NSMutableArray<NSString *> *> *values __attribute__((swift_name("values")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_httpHeadersBuilder")))
@interface SSDKKtor_httpHeadersBuilder : SSDKKtor_utilsStringValuesBuilderImpl
- (instancetype)initWithSize:(int32_t)size __attribute__((swift_name("init(size:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithCaseInsensitiveName:(BOOL)caseInsensitiveName size:(int32_t)size __attribute__((swift_name("init(caseInsensitiveName:size:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (id<SSDKKtor_httpHeaders>)build __attribute__((swift_name("build()")));
- (void)validateNameName:(NSString *)name __attribute__((swift_name("validateName(name:)")));
- (void)validateValueValue:(NSString *)value __attribute__((swift_name("validateValue(value:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_coreHttpRequestBuilder.Companion")))
@interface SSDKKtor_client_coreHttpRequestBuilderCompanion : SSDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SSDKKtor_client_coreHttpRequestBuilderCompanion *shared __attribute__((swift_name("shared")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_httpURLBuilder")))
@interface SSDKKtor_httpURLBuilder : SSDKBase
- (instancetype)initWithProtocol:(SSDKKtor_httpURLProtocol *)protocol host:(NSString *)host port:(int32_t)port user:(NSString * _Nullable)user password:(NSString * _Nullable)password pathSegments:(NSArray<NSString *> *)pathSegments parameters:(id<SSDKKtor_httpParameters>)parameters fragment:(NSString *)fragment trailingQuery:(BOOL)trailingQuery __attribute__((swift_name("init(protocol:host:port:user:password:pathSegments:parameters:fragment:trailingQuery:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) SSDKKtor_httpURLBuilderCompanion *companion __attribute__((swift_name("companion")));
- (SSDKKtor_httpUrl *)build __attribute__((swift_name("build()")));
- (NSString *)buildString __attribute__((swift_name("buildString()")));
@property NSString *encodedFragment __attribute__((swift_name("encodedFragment")));
@property id<SSDKKtor_httpParametersBuilder> encodedParameters __attribute__((swift_name("encodedParameters")));
@property NSString * _Nullable encodedPassword __attribute__((swift_name("encodedPassword")));
@property NSArray<NSString *> *encodedPathSegments __attribute__((swift_name("encodedPathSegments")));
@property NSString * _Nullable encodedUser __attribute__((swift_name("encodedUser")));
@property NSString *fragment __attribute__((swift_name("fragment")));
@property NSString *host __attribute__((swift_name("host")));
@property (readonly) id<SSDKKtor_httpParametersBuilder> parameters __attribute__((swift_name("parameters")));
@property NSString * _Nullable password __attribute__((swift_name("password")));
@property NSArray<NSString *> *pathSegments __attribute__((swift_name("pathSegments")));
@property int32_t port __attribute__((swift_name("port")));
@property SSDKKtor_httpURLProtocol *protocol __attribute__((swift_name("protocol")));
@property BOOL trailingQuery __attribute__((swift_name("trailingQuery")));
@property NSString * _Nullable user __attribute__((swift_name("user")));
@end;

__attribute__((swift_name("KotlinKType")))
@protocol SSDKKotlinKType
@required
@property (readonly) NSArray<SSDKKotlinKTypeProjection *> *arguments __attribute__((swift_name("arguments")));
@property (readonly) id<SSDKKotlinKClassifier> _Nullable classifier __attribute__((swift_name("classifier")));
@property (readonly) BOOL isMarkedNullable __attribute__((swift_name("isMarkedNullable")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_ioMemory.Companion")))
@interface SSDKKtor_ioMemoryCompanion : SSDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SSDKKtor_ioMemoryCompanion *shared __attribute__((swift_name("shared")));
@property (readonly) SSDKKtor_ioMemory *Empty __attribute__((swift_name("Empty")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_ioBuffer.Companion")))
@interface SSDKKtor_ioBufferCompanion : SSDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SSDKKtor_ioBufferCompanion *shared __attribute__((swift_name("shared")));
@property (readonly) SSDKKtor_ioBuffer *Empty __attribute__((swift_name("Empty")));
@property (readonly) int32_t ReservedSize __attribute__((swift_name("ReservedSize")));
@end;

__attribute__((swift_name("Ktor_ioObjectPool")))
@protocol SSDKKtor_ioObjectPool <SSDKKtor_ioCloseable>
@required
- (id)borrow __attribute__((swift_name("borrow()")));
- (void)dispose __attribute__((swift_name("dispose()")));
- (void)recycleInstance:(id)instance __attribute__((swift_name("recycle(instance:)")));
@property (readonly) int32_t capacity __attribute__((swift_name("capacity")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_ioChunkBuffer.Companion")))
@interface SSDKKtor_ioChunkBufferCompanion : SSDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SSDKKtor_ioChunkBufferCompanion *shared __attribute__((swift_name("shared")));
@property (readonly) SSDKKtor_ioChunkBuffer *Empty __attribute__((swift_name("Empty")));
@property (readonly) id<SSDKKtor_ioObjectPool> EmptyPool __attribute__((swift_name("EmptyPool")));
@property (readonly) id<SSDKKtor_ioObjectPool> Pool __attribute__((swift_name("Pool")));
@end;

__attribute__((swift_name("KotlinByteIterator")))
@interface SSDKKotlinByteIterator : SSDKBase <SSDKKotlinIterator>
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (SSDKByte *)next __attribute__((swift_name("next()")));
- (int8_t)nextByte __attribute__((swift_name("nextByte()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_ioInput.Companion")))
@interface SSDKKtor_ioInputCompanion : SSDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SSDKKtor_ioInputCompanion *shared __attribute__((swift_name("shared")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_ioByteReadPacket.Companion")))
@interface SSDKKtor_ioByteReadPacketCompanion : SSDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SSDKKtor_ioByteReadPacketCompanion *shared __attribute__((swift_name("shared")));
@property (readonly) SSDKKtor_ioByteReadPacket *Empty __attribute__((swift_name("Empty")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_utilsWeekDay.Companion")))
@interface SSDKKtor_utilsWeekDayCompanion : SSDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SSDKKtor_utilsWeekDayCompanion *shared __attribute__((swift_name("shared")));
- (SSDKKtor_utilsWeekDay *)fromOrdinal:(int32_t)ordinal __attribute__((swift_name("from(ordinal:)")));
- (SSDKKtor_utilsWeekDay *)fromValue:(NSString *)value __attribute__((swift_name("from(value:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_utilsMonth.Companion")))
@interface SSDKKtor_utilsMonthCompanion : SSDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SSDKKtor_utilsMonthCompanion *shared __attribute__((swift_name("shared")));
- (SSDKKtor_utilsMonth *)fromOrdinal:(int32_t)ordinal __attribute__((swift_name("from(ordinal:)")));
- (SSDKKtor_utilsMonth *)fromValue:(NSString *)value __attribute__((swift_name("from(value:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_httpHeaderValueParam")))
@interface SSDKKtor_httpHeaderValueParam : SSDKBase
- (instancetype)initWithName:(NSString *)name value:(NSString *)value __attribute__((swift_name("init(name:value:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (SSDKKtor_httpHeaderValueParam *)doCopyName:(NSString *)name value:(NSString *)value __attribute__((swift_name("doCopy(name:value:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@property (readonly) NSString *value __attribute__((swift_name("value")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_httpHeaderValueWithParameters.Companion")))
@interface SSDKKtor_httpHeaderValueWithParametersCompanion : SSDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SSDKKtor_httpHeaderValueWithParametersCompanion *shared __attribute__((swift_name("shared")));
- (id _Nullable)parseValue:(NSString *)value init:(id _Nullable (^)(NSString *, NSArray<SSDKKtor_httpHeaderValueParam *> *))init __attribute__((swift_name("parse(value:init:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_httpContentType.Companion")))
@interface SSDKKtor_httpContentTypeCompanion : SSDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SSDKKtor_httpContentTypeCompanion *shared __attribute__((swift_name("shared")));
- (SSDKKtor_httpContentType *)parseValue:(NSString *)value __attribute__((swift_name("parse(value:)")));
@property (readonly) SSDKKtor_httpContentType *Any __attribute__((swift_name("Any")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreParentJob")))
@protocol SSDKKotlinx_coroutines_coreParentJob <SSDKKotlinx_coroutines_coreJob>
@required
- (SSDKKotlinCancellationException *)getChildJobCancellationCause __attribute__((swift_name("getChildJobCancellationCause()")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreSelectInstance")))
@protocol SSDKKotlinx_coroutines_coreSelectInstance
@required
- (void)disposeOnSelectHandle:(id<SSDKKotlinx_coroutines_coreDisposableHandle>)handle __attribute__((swift_name("disposeOnSelect(handle:)")));
- (id _Nullable)performAtomicTrySelectDesc:(SSDKKotlinx_coroutines_coreAtomicDesc *)desc __attribute__((swift_name("performAtomicTrySelect(desc:)")));
- (void)resumeSelectWithExceptionException:(SSDKKotlinThrowable *)exception __attribute__((swift_name("resumeSelectWithException(exception:)")));
- (BOOL)trySelect __attribute__((swift_name("trySelect()")));
- (id _Nullable)trySelectOtherOtherOp:(SSDKKotlinx_coroutines_coreLockFreeLinkedListNodePrepareOp * _Nullable)otherOp __attribute__((swift_name("trySelectOther(otherOp:)")));
@property (readonly) id<SSDKKotlinContinuation> completion __attribute__((swift_name("completion")));
@property (readonly) BOOL isSelected __attribute__((swift_name("isSelected")));
@end;

__attribute__((swift_name("KotlinSuspendFunction0")))
@protocol SSDKKotlinSuspendFunction0 <SSDKKotlinFunction>
@required

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)invokeWithCompletionHandler:(void (^)(id _Nullable_result, NSError * _Nullable))completionHandler __attribute__((swift_name("invoke(completionHandler:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_httpURLBuilder.Companion")))
@interface SSDKKtor_httpURLBuilderCompanion : SSDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SSDKKtor_httpURLBuilderCompanion *shared __attribute__((swift_name("shared")));
@end;

__attribute__((swift_name("Ktor_httpParametersBuilder")))
@protocol SSDKKtor_httpParametersBuilder <SSDKKtor_utilsStringValuesBuilder>
@required
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinKTypeProjection")))
@interface SSDKKotlinKTypeProjection : SSDKBase
- (instancetype)initWithVariance:(SSDKKotlinKVariance * _Nullable)variance type:(id<SSDKKotlinKType> _Nullable)type __attribute__((swift_name("init(variance:type:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) SSDKKotlinKTypeProjectionCompanion *companion __attribute__((swift_name("companion")));
- (SSDKKotlinKVariance * _Nullable)component1 __attribute__((swift_name("component1()")));
- (id<SSDKKotlinKType> _Nullable)component2 __attribute__((swift_name("component2()")));
- (SSDKKotlinKTypeProjection *)doCopyVariance:(SSDKKotlinKVariance * _Nullable)variance type:(id<SSDKKotlinKType> _Nullable)type __attribute__((swift_name("doCopy(variance:type:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) id<SSDKKotlinKType> _Nullable type __attribute__((swift_name("type")));
@property (readonly) SSDKKotlinKVariance * _Nullable variance __attribute__((swift_name("variance")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreAtomicDesc")))
@interface SSDKKotlinx_coroutines_coreAtomicDesc : SSDKBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (void)completeOp:(SSDKKotlinx_coroutines_coreAtomicOp<id> *)op failure:(id _Nullable)failure __attribute__((swift_name("complete(op:failure:)")));
- (id _Nullable)prepareOp:(SSDKKotlinx_coroutines_coreAtomicOp<id> *)op __attribute__((swift_name("prepare(op:)")));
@property SSDKKotlinx_coroutines_coreAtomicOp<id> *atomicOp __attribute__((swift_name("atomicOp")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreOpDescriptor")))
@interface SSDKKotlinx_coroutines_coreOpDescriptor : SSDKBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (BOOL)isEarlierThanThat:(SSDKKotlinx_coroutines_coreOpDescriptor *)that __attribute__((swift_name("isEarlierThan(that:)")));
- (id _Nullable)performAffected:(id _Nullable)affected __attribute__((swift_name("perform(affected:)")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) SSDKKotlinx_coroutines_coreAtomicOp<id> * _Nullable atomicOp __attribute__((swift_name("atomicOp")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Kotlinx_coroutines_coreLockFreeLinkedListNode.PrepareOp")))
@interface SSDKKotlinx_coroutines_coreLockFreeLinkedListNodePrepareOp : SSDKKotlinx_coroutines_coreOpDescriptor
- (instancetype)initWithAffected:(SSDKKotlinx_coroutines_coreLockFreeLinkedListNode *)affected next:(SSDKKotlinx_coroutines_coreLockFreeLinkedListNode *)next desc:(SSDKKotlinx_coroutines_coreLockFreeLinkedListNodeAbstractAtomicDesc *)desc __attribute__((swift_name("init(affected:next:desc:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (void)finishPrepare __attribute__((swift_name("finishPrepare()")));
- (id _Nullable)performAffected:(id _Nullable)affected __attribute__((swift_name("perform(affected:)")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) SSDKKotlinx_coroutines_coreLockFreeLinkedListNode *affected __attribute__((swift_name("affected")));
@property (readonly) SSDKKotlinx_coroutines_coreAtomicOp<id> *atomicOp __attribute__((swift_name("atomicOp")));
@property (readonly) SSDKKotlinx_coroutines_coreLockFreeLinkedListNodeAbstractAtomicDesc *desc __attribute__((swift_name("desc")));
@property (readonly) SSDKKotlinx_coroutines_coreLockFreeLinkedListNode *next __attribute__((swift_name("next")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinKVariance")))
@interface SSDKKotlinKVariance : SSDKKotlinEnum<SSDKKotlinKVariance *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) SSDKKotlinKVariance *invariant __attribute__((swift_name("invariant")));
@property (class, readonly) SSDKKotlinKVariance *in __attribute__((swift_name("in")));
@property (class, readonly) SSDKKotlinKVariance *out __attribute__((swift_name("out")));
+ (SSDKKotlinArray<SSDKKotlinKVariance *> *)values __attribute__((swift_name("values()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinKTypeProjection.Companion")))
@interface SSDKKotlinKTypeProjectionCompanion : SSDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SSDKKotlinKTypeProjectionCompanion *shared __attribute__((swift_name("shared")));
- (SSDKKotlinKTypeProjection *)contravariantType:(id<SSDKKotlinKType>)type __attribute__((swift_name("contravariant(type:)")));
- (SSDKKotlinKTypeProjection *)covariantType:(id<SSDKKotlinKType>)type __attribute__((swift_name("covariant(type:)")));
- (SSDKKotlinKTypeProjection *)invariantType:(id<SSDKKotlinKType>)type __attribute__((swift_name("invariant(type:)")));
@property (readonly) SSDKKotlinKTypeProjection *STAR __attribute__((swift_name("STAR")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreAtomicOp")))
@interface SSDKKotlinx_coroutines_coreAtomicOp<__contravariant T> : SSDKKotlinx_coroutines_coreOpDescriptor
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (void)completeAffected:(T _Nullable)affected failure:(id _Nullable)failure __attribute__((swift_name("complete(affected:failure:)")));
- (id _Nullable)decideDecision:(id _Nullable)decision __attribute__((swift_name("decide(decision:)")));
- (id _Nullable)performAffected:(id _Nullable)affected __attribute__((swift_name("perform(affected:)")));
- (id _Nullable)prepareAffected:(T _Nullable)affected __attribute__((swift_name("prepare(affected:)")));
@property (readonly) SSDKKotlinx_coroutines_coreAtomicOp<id> *atomicOp __attribute__((swift_name("atomicOp")));
@property (readonly) id _Nullable consensus __attribute__((swift_name("consensus")));
@property (readonly) BOOL isDecided __attribute__((swift_name("isDecided")));
@property (readonly) int64_t opSequence __attribute__((swift_name("opSequence")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreLockFreeLinkedListNode")))
@interface SSDKKotlinx_coroutines_coreLockFreeLinkedListNode : SSDKBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (void)addLastNode:(SSDKKotlinx_coroutines_coreLockFreeLinkedListNode *)node __attribute__((swift_name("addLast(node:)")));
- (BOOL)addLastIfNode:(SSDKKotlinx_coroutines_coreLockFreeLinkedListNode *)node condition:(SSDKBoolean *(^)(void))condition __attribute__((swift_name("addLastIf(node:condition:)")));
- (BOOL)addLastIfPrevNode:(SSDKKotlinx_coroutines_coreLockFreeLinkedListNode *)node predicate:(SSDKBoolean *(^)(SSDKKotlinx_coroutines_coreLockFreeLinkedListNode *))predicate __attribute__((swift_name("addLastIfPrev(node:predicate:)")));
- (BOOL)addLastIfPrevAndIfNode:(SSDKKotlinx_coroutines_coreLockFreeLinkedListNode *)node predicate:(SSDKBoolean *(^)(SSDKKotlinx_coroutines_coreLockFreeLinkedListNode *))predicate condition:(SSDKBoolean *(^)(void))condition __attribute__((swift_name("addLastIfPrevAndIf(node:predicate:condition:)")));
- (BOOL)addOneIfEmptyNode:(SSDKKotlinx_coroutines_coreLockFreeLinkedListNode *)node __attribute__((swift_name("addOneIfEmpty(node:)")));
- (SSDKKotlinx_coroutines_coreLockFreeLinkedListNodeAddLastDesc<SSDKKotlinx_coroutines_coreLockFreeLinkedListNode *> *)describeAddLastNode:(SSDKKotlinx_coroutines_coreLockFreeLinkedListNode *)node __attribute__((swift_name("describeAddLast(node:)")));
- (SSDKKotlinx_coroutines_coreLockFreeLinkedListNodeRemoveFirstDesc<SSDKKotlinx_coroutines_coreLockFreeLinkedListNode *> *)describeRemoveFirst __attribute__((swift_name("describeRemoveFirst()")));
- (void)helpRemove __attribute__((swift_name("helpRemove()")));
- (SSDKKotlinx_coroutines_coreLockFreeLinkedListNode * _Nullable)nextIfRemoved __attribute__((swift_name("nextIfRemoved()")));
- (BOOL)remove __attribute__((swift_name("remove()")));
- (id _Nullable)removeFirstIfIsInstanceOfOrPeekIfPredicate:(SSDKBoolean *(^)(id _Nullable))predicate __attribute__((swift_name("removeFirstIfIsInstanceOfOrPeekIf(predicate:)")));
- (SSDKKotlinx_coroutines_coreLockFreeLinkedListNode * _Nullable)removeFirstOrNull __attribute__((swift_name("removeFirstOrNull()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) BOOL isRemoved __attribute__((swift_name("isRemoved")));
@property (readonly, getter=next_) id next __attribute__((swift_name("next")));
@property (readonly) SSDKKotlinx_coroutines_coreLockFreeLinkedListNode *nextNode __attribute__((swift_name("nextNode")));
@property (readonly) SSDKKotlinx_coroutines_coreLockFreeLinkedListNode *prevNode __attribute__((swift_name("prevNode")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreLockFreeLinkedListNode.AbstractAtomicDesc")))
@interface SSDKKotlinx_coroutines_coreLockFreeLinkedListNodeAbstractAtomicDesc : SSDKKotlinx_coroutines_coreAtomicDesc
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (void)completeOp:(SSDKKotlinx_coroutines_coreAtomicOp<id> *)op failure:(id _Nullable)failure __attribute__((swift_name("complete(op:failure:)")));
- (id _Nullable)failureAffected:(SSDKKotlinx_coroutines_coreLockFreeLinkedListNode *)affected __attribute__((swift_name("failure(affected:)")));
- (void)finishOnSuccessAffected:(SSDKKotlinx_coroutines_coreLockFreeLinkedListNode *)affected next:(SSDKKotlinx_coroutines_coreLockFreeLinkedListNode *)next __attribute__((swift_name("finishOnSuccess(affected:next:)")));
- (void)finishPreparePrepareOp:(SSDKKotlinx_coroutines_coreLockFreeLinkedListNodePrepareOp *)prepareOp __attribute__((swift_name("finishPrepare(prepareOp:)")));
- (id _Nullable)onPreparePrepareOp:(SSDKKotlinx_coroutines_coreLockFreeLinkedListNodePrepareOp *)prepareOp __attribute__((swift_name("onPrepare(prepareOp:)")));
- (void)onRemovedAffected:(SSDKKotlinx_coroutines_coreLockFreeLinkedListNode *)affected __attribute__((swift_name("onRemoved(affected:)")));
- (id _Nullable)prepareOp:(SSDKKotlinx_coroutines_coreAtomicOp<id> *)op __attribute__((swift_name("prepare(op:)")));
- (BOOL)retryAffected:(SSDKKotlinx_coroutines_coreLockFreeLinkedListNode *)affected next:(id)next __attribute__((swift_name("retry(affected:next:)")));
- (SSDKKotlinx_coroutines_coreLockFreeLinkedListNode * _Nullable)takeAffectedNodeOp:(SSDKKotlinx_coroutines_coreOpDescriptor *)op __attribute__((swift_name("takeAffectedNode(op:)")));
- (id)updatedNextAffected:(SSDKKotlinx_coroutines_coreLockFreeLinkedListNode *)affected next:(SSDKKotlinx_coroutines_coreLockFreeLinkedListNode *)next __attribute__((swift_name("updatedNext(affected:next:)")));
@property (readonly) SSDKKotlinx_coroutines_coreLockFreeLinkedListNode * _Nullable affectedNode __attribute__((swift_name("affectedNode")));
@property (readonly) SSDKKotlinx_coroutines_coreLockFreeLinkedListNode * _Nullable originalNext __attribute__((swift_name("originalNext")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreLockFreeLinkedListNodeAddLastDesc")))
@interface SSDKKotlinx_coroutines_coreLockFreeLinkedListNodeAddLastDesc<T> : SSDKKotlinx_coroutines_coreLockFreeLinkedListNodeAbstractAtomicDesc
- (instancetype)initWithQueue:(SSDKKotlinx_coroutines_coreLockFreeLinkedListNode *)queue node:(T)node __attribute__((swift_name("init(queue:node:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (void)finishOnSuccessAffected:(SSDKKotlinx_coroutines_coreLockFreeLinkedListNode *)affected next:(SSDKKotlinx_coroutines_coreLockFreeLinkedListNode *)next __attribute__((swift_name("finishOnSuccess(affected:next:)")));
- (void)finishPreparePrepareOp:(SSDKKotlinx_coroutines_coreLockFreeLinkedListNodePrepareOp *)prepareOp __attribute__((swift_name("finishPrepare(prepareOp:)")));
- (BOOL)retryAffected:(SSDKKotlinx_coroutines_coreLockFreeLinkedListNode *)affected next:(id)next __attribute__((swift_name("retry(affected:next:)")));
- (SSDKKotlinx_coroutines_coreLockFreeLinkedListNode * _Nullable)takeAffectedNodeOp:(SSDKKotlinx_coroutines_coreOpDescriptor *)op __attribute__((swift_name("takeAffectedNode(op:)")));
- (id)updatedNextAffected:(SSDKKotlinx_coroutines_coreLockFreeLinkedListNode *)affected next:(SSDKKotlinx_coroutines_coreLockFreeLinkedListNode *)next __attribute__((swift_name("updatedNext(affected:next:)")));
@property (readonly) SSDKKotlinx_coroutines_coreLockFreeLinkedListNode * _Nullable affectedNode __attribute__((swift_name("affectedNode")));
@property (readonly) T node __attribute__((swift_name("node")));
@property (readonly) SSDKKotlinx_coroutines_coreLockFreeLinkedListNode *originalNext __attribute__((swift_name("originalNext")));
@property (readonly) SSDKKotlinx_coroutines_coreLockFreeLinkedListNode *queue __attribute__((swift_name("queue")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreLockFreeLinkedListNodeRemoveFirstDesc")))
@interface SSDKKotlinx_coroutines_coreLockFreeLinkedListNodeRemoveFirstDesc<T> : SSDKKotlinx_coroutines_coreLockFreeLinkedListNodeAbstractAtomicDesc
- (instancetype)initWithQueue:(SSDKKotlinx_coroutines_coreLockFreeLinkedListNode *)queue __attribute__((swift_name("init(queue:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (id _Nullable)failureAffected:(SSDKKotlinx_coroutines_coreLockFreeLinkedListNode *)affected __attribute__((swift_name("failure(affected:)")));
- (void)finishOnSuccessAffected:(SSDKKotlinx_coroutines_coreLockFreeLinkedListNode *)affected next:(SSDKKotlinx_coroutines_coreLockFreeLinkedListNode *)next __attribute__((swift_name("finishOnSuccess(affected:next:)")));
- (void)finishPreparePrepareOp:(SSDKKotlinx_coroutines_coreLockFreeLinkedListNodePrepareOp *)prepareOp __attribute__((swift_name("finishPrepare(prepareOp:)")));
- (BOOL)retryAffected:(SSDKKotlinx_coroutines_coreLockFreeLinkedListNode *)affected next:(id)next __attribute__((swift_name("retry(affected:next:)")));
- (SSDKKotlinx_coroutines_coreLockFreeLinkedListNode * _Nullable)takeAffectedNodeOp:(SSDKKotlinx_coroutines_coreOpDescriptor *)op __attribute__((swift_name("takeAffectedNode(op:)")));
- (id)updatedNextAffected:(SSDKKotlinx_coroutines_coreLockFreeLinkedListNode *)affected next:(SSDKKotlinx_coroutines_coreLockFreeLinkedListNode *)next __attribute__((swift_name("updatedNext(affected:next:)")));
@property (readonly) SSDKKotlinx_coroutines_coreLockFreeLinkedListNode * _Nullable affectedNode __attribute__((swift_name("affectedNode")));
@property (readonly) SSDKKotlinx_coroutines_coreLockFreeLinkedListNode * _Nullable originalNext __attribute__((swift_name("originalNext")));
@property (readonly) SSDKKotlinx_coroutines_coreLockFreeLinkedListNode *queue __attribute__((swift_name("queue")));
@property (readonly) T _Nullable result __attribute__((swift_name("result")));
@end;

#pragma pop_macro("_Nullable_result")
#pragma clang diagnostic pop
NS_ASSUME_NONNULL_END
