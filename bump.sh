#!/bin/sh
oldVersion=$(grep "version:" cz.yaml | awk '{print $2}')
newVersion=$(cz bump $1 --dry-run | grep tag | awk '{print $4}')

# modify version for POM property and SystemaConstants
for FILE in "SystemaSDK.podspec"; do
  echo "Changing ${oldVersion} -> ${newVersion}: ${FILE}"
  sed -i "s/${oldVersion}/${newVersion}/" $FILE
done

# bump version and generate changelog
cz bump --changelog $1
